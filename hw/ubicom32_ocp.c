/*
 * QEMU/UBICOM32 OCP block.
 *
 * Copyright (c) 2017, The Linux Foundation. All rights reserved.
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
/*
 *    pll
 *    mailbox interface
 *    timer block
 *    MMU block.
 *
 * We will make this 4k block look like a piece of memory on the IO bus. We will set up read and write interfaces
 * to this block.
 *
 * We will have 2 Qemu timers to represent the MPT and Sysval block. We need to extract the frequency from the PLL
 * Registers settings. This will be different for 8k.
 */

#include <arpa/inet.h>
#include <stdint.h>

#include "sysemu.h"
#include "boards.h"
#include "monitor.h"
#include "qemu-timer.h"
#include "qemu-char.h"
#include "qemu-log.h"
#include "cpu.h"
#include "ubicom32.h"

#define ARCHREFCOMPARE
#define USE_NONCACHEABLE_BIT

void irq_info(Monitor *mon);
void pic_info(Monitor *mon);

#ifdef ARCHREFCOMPARE
extern int archrefcompare_flag;
extern int archrefcompare_debug_flag;
extern int TLB_value_check_flag;
extern int flash_write_check_flag;

/* Current privilege level. (0 = supervisor mode, 1 = user mode - yes, this is intentional) */
static int privilege_level;

uint32_t ocp_memory[4096];
void archrefcompare_abort(void);
#endif

/*
 * ubicom32_io_timer
 */
struct ubicom32_io_timer {
    volatile uint32_t mptval;
    volatile uint32_t rtcom;
    volatile uint32_t tkey;
    volatile uint32_t wdcom;
    volatile uint32_t wdcfg;
    volatile uint32_t sysval;
    volatile uint32_t syscom[64 - 6];
    volatile uint32_t rsgcfg;
    volatile uint32_t trn;
} ubicom32_timer;

/*
 * ubicom32_mailbox
 */
struct ubicom32_mailbox {
    volatile uint32_t inmail;
    volatile uint32_t outmail;
    volatile uint32_t mail_stat;
};

#define ISD_MAILBOX_STATUS_IN_FULL (1 << 31)
#define ISD_MAILBOX_STATUS_IN_EMPTY (1 << 30)
#define ISD_MAILBOX_STATUS_OUT_FULL (1 << 29)
#define ISD_MAILBOX_STATUS_OUT_EMPTY (1 << 28)

/*
 * The serial data arriving from Qemu is stored in a 32 byte ring buffer. Write at head and read from tail.
 * The data will be taken from Qemu and placed into the ring buffer. Every time Ubicom reads the inmail location
 * we will give it data from the ring buffer tail. A read to the mail stat will have the IN_EMPTY bit clear if
 * there is data in the ring buffer. Write of data by UBICOM to the outmail register will be take and supplied
 * directly to Qemu. The out queue will be empty all the time.
 */
struct serial_in_data {
    uint32_t head;
    uint32_t tail;
    char data[32];
} qemu_in_data;

uint32_t ocp_general[0x40];

/*
 * OCP MMU block.
 */
uint32_t ocp_mmu[0x80];

/*
** Note: The values in the Jupiter Chip Specification section 9.1: MMU registers are BYTE indexes.
**       These are INT indexes (byte index / 4)
*/

#define CONFIG		0
#define PTEC_CFG0	1
#define PTEC_CFG1	2
#define PTEC_CFG2	3
#define MISSQW0		4
#define MISSQW1		5
#define RESTART		6
#define INSERTW0	7
#define INSERTW1	8
#define I_PURGE		9
#define D_PURGE		10
#define PTEC_ERR	11
#define TLB_IDX		12
#define ENTRYW0		13
#define ENTRYW21	14
#define STATSU		15
#define BUS_ST0		16	/* A 1 indicates a thread in blocked state */
#define BUS_ST1		17	/* 1 = data bus blocked, 0 = instruction bus blocked */
#define BUS_ST2		18	/* 1 = MMU blocked 0 = Blocked by some other reason */
#define BUS_ST3		19	/* 1 = Cache blocked 0 = Blocked by some other reason */

/*************************************
* Start ASID access macros
*************************************/

#define PGD(thread_num) 	(ocp_mmu[(0x100 + (thread_num << 4)) >> 2] >>  0)
#define PGD_INDEX(thread_num) 	((0x100 + (thread_num << 4)) >> 2)

#define ASID0(thread_num) 	((ocp_mmu[(0x104 + (thread_num << 4)) >> 2] >>  0) & 0x3ff)
#define ASID1(thread_num) 	((ocp_mmu[(0x104 + (thread_num << 4)) >> 2] >> 10) & 0x3ff)
#define ASID2(thread_num) 	((ocp_mmu[(0x104 + (thread_num << 4)) >> 2] >> 20) & 0x3ff)

#define CMP_0_EN(thread_num)	((ocp_mmu[(0x108 + (thread_num << 4)) >> 2] >> 24) & 1)
#define CMP_1_EN(thread_num)	((ocp_mmu[(0x108 + (thread_num << 4)) >> 2] >> 25) & 1)
#define CMP_2_EN(thread_num)	((ocp_mmu[(0x108 + (thread_num << 4)) >> 2] >> 26) & 1)

#define ASID_CMP0(thread_num)	((ocp_mmu[(0x108 + (thread_num << 4)) >> 2] >>  0) & 0xf)
#define ASID_CMP1(thread_num)	((ocp_mmu[(0x108 + (thread_num << 4)) >> 2] >>  8) & 0xf)
#define ASID_CMP2(thread_num)	((ocp_mmu[(0x108 + (thread_num << 4)) >> 2] >> 16) & 0xf)

#define ASID_MASK0(thread_num)	((ocp_mmu[(0x10c + (thread_num << 4)) >> 2] >>   0) & 0xf)
#define ASID_MASK1(thread_num)	((ocp_mmu[(0x10c + (thread_num << 4)) >> 2] >>   8) & 0xf)
#define ASID_MASK2(thread_num)	((ocp_mmu[(0x10c + (thread_num << 4)) >> 2] >>  16) & 0xf)

/*************************************
* End ASID access macros
*************************************/
/*************************************
* Start PTEC access macros
*************************************/

#define PTEC_ENABLED		(ocp_mmu[0] & (1 << 4))

#define ATAG_BASE		(ocp_mmu[0x8 >> 2] & (((1 << 25) - 1) << 7))
#define ENTRY_BASE		(ocp_mmu[0xc >> 2] & (((1 << 25) - 1) << 7))

/* This is the equivalent of "ENTRYO_BITS" in the manual, but renamed to be less confusing */

#define PTEC_ENTRIES_LOG2	((ocp_mmu[0x4 >> 2] >> 16) & ((1 << 5) - 1))

#define PTEC_ENTRIES_NUM 	(1 << PTEC_ENTRIES_LOG2)

#define EXTRACT_BITS_FROM_INT32(value, right_shift, bits_num)		(((unsigned int)value >> right_shift) & ((1 << bits_num) - 1))

#define PTEC_TAG0(atag_value)	EXTRACT_BITS_FROM_INT32(atag_value, 26,  6)
#define PTEC_TAG1(atag_value)	EXTRACT_BITS_FROM_INT32(atag_value, 10,  6)
#define PTEC_ASID0(atag_value)	EXTRACT_BITS_FROM_INT32(atag_value, 16, 10)
#define PTEC_ASID1(atag_value)	EXTRACT_BITS_FROM_INT32(atag_value,  0, 10)

/*************************************
* End PTEC access macros
*************************************/
/*************************************
* Start TLB access macros
*************************************/

#ifdef USE_NONCACHEABLE_BIT

/*
** The Jupiter hardware (and software) TLB value is in this format:
**
** |vpn|asid|ppn|r|w|x|s|v|
**
** where:
**
** field    description          # of  bits   bit positions
** --------------------------------------------------------
** vpn     virtual page number    19 bits       bits 53:35
** asid    address space ID       10 bits       bits 34:25
** ppn     physical page number   19 bits       bits 24:6
** nocache noncacheable bit        1 bit        bit  5  (unused by sidrefcompare)
** r       read permission         1 bit        bit  4
** w       write permission        1 bit        bit  3
** x       execute permission      1 bit        bit  2
** s       supervisor permission   1 bit        bit  1
** v       valid qualifier         1 bit        bit  0
**
** IMPORTANT NOTE IMPORTANT NOTE IMPORTANT NOTE IMPORTANT NOTE
**
** The "supervisor permission" bit is inverted!
**
** 0 = supervisor mode
** 1 = user mode
**
** IMPORTANT NOTE IMPORTANT NOTE IMPORTANT NOTE IMPORTANT NOTE
*/

/*
** Note: "tlb" is a long long * (pointer to a 64-bit value) macros should return int
** in case the code prints the value with %d or %x.
*/

/* The upper 19 bits of the virtual page of the TLB entry (19 bits) */
#define TLB_ENTRY_VPN(tlb_value)                ((int)(tlb_value >> 35) & 0x7ffff)

/* The ASID of the TLB entry (10 bits) */
#define TLB_ENTRY_ASID(tlb_value)               ((int)(tlb_value >> 25) & 0x3ff)

/* The physical page page number of the TLB entry (19 bits) */
#define TLB_ENTRY_PPN(tlb_value)                ((int)(tlb_value >> 6) & 0x7ffff)

/* The read permission bit for the TLB entry (1 bit) */
#define TLB_ENTRY_READ_BIT(tlb_value)           ((int)(tlb_value >> 4) & 1)

/* The write permission bit for the TLB entry (1 bit) */
#define TLB_ENTRY_WRITE_BIT(tlb_value)          ((int)(tlb_value >> 3) & 1)

/* The execute permission bit for the TLB entry (1 bit) */
#define TLB_ENTRY_EXECUTE_BIT(tlb_value)        ((int)(tlb_value >> 2) & 1)

/* The supervisor permission bit for the TLB entry (1 bit) */
/* 0 = supervisor mode, 1 = user mode - this is the way the hardware works! */
#define TLB_ENTRY_SUPERVISOR_BIT(tlb_value)     ((int)(tlb_value >> 1) & 1)

/* The valid bit for the TLB entry (1 bit) */
#define TLB_ENTRY_VALID_BIT(tlb_value)          ((int)(tlb_value >> 0) & 1)

#else

/* The upper 19 bits of the virtual page of the TLB entry (19 bits) */
#define TLB_ENTRY_VPN(tlb_value)                ((int)(tlb_value >> 34) & 0x7ffff)

/* The ASID of the TLB entry (10 bits) */
#define TLB_ENTRY_ASID(tlb_value)               ((int)(tlb_value >> 24) & 0x3ff)

/* The physical page page number of the TLB entry (20 bits) */
#define TLB_ENTRY_PPN(tlb_value)                ((int)(tlb_value >> 5) & 0x7ffff)

/* The read permission bit for the TLB entry (1 bit)*/
#define TLB_ENTRY_READ_BIT(tlb_value)           ((int)(tlb_value >> 4) & 1)

/* The write permission bit for the TLB entry (1 bit) */
#define TLB_ENTRY_WRITE_BIT(tlb_value)          ((int)(tlb_value >> 3) & 1)

/* The execute permission bit for the TLB entry (1 bit) */
#define TLB_ENTRY_EXECUTE_BIT(tlb_value)        ((int)(tlb_value >> 2) & 1)

/* The supervisor permission bit for the TLB entry (1 bit) */
/* 0 = supervisor mode, 1 = user mode - this is the way the hardware works! */
#define TLB_ENTRY_SUPERVISOR_BIT(tlb_value)     ((int)(tlb_value >> 1) & 1)

/* The valid bit for the TLB entry (1 bit) */
#define TLB_ENTRY_VALID_BIT(tlb_value)          ((int)(tlb_value >> 0) & 1)

#endif

/*************************************
* Start TLB access macros
*************************************/
/*************************************
* Start other MMU macros
*************************************/

/* Number of hardware TLB entries */
#define TLB_ENTRIES_NUM 64

/* MMU page size - 1 = 8kbytes, 3 = 16kbytes */
#define PAGE_MASK			(ocp_mmu[0] & 3)
#ifdef USE_NONCACHEABLE_BIT
#define RTAG_MASK			((ocp_mmu[4 >> 2] & ((1 << 7) - 1)) >> 1)
#else
#define RTAG_MASK			(ocp_mmu[4 >> 2] & ((1 << 7) - 1))
#endif

/*************************************
* End other MMU macros
*************************************/
/*************************************
* Start archrefcompare data structures
*************************************/

int check_ptec_flag = 1;
typedef uint64_t TLB_ENTRY;
TLB_ENTRY insn_tlb[TLB_ENTRIES_NUM];
TLB_ENTRY data_tlb[TLB_ENTRIES_NUM];

/*************************************
* End archrefcompare data structures
*************************************/

#define GEN_GET_CLK_PLL_NR(v) ((((v) >> 23) & 0x003f) + 1)
#define GEN_GET_CLK_PLL_NF(v) ((((v) >> 11) & 0x0fff) + 1)
#define GEN_GET_CLK_PLL_OD(v) ((((v) >> 8) & 0x7) + 1)

struct miss_queue_entry {
    uint32_t w0;		/* This word holds PGD, TYPE, SRC, TNUM */
    uint32_t w1;		/* This word holds VPN, MULTIPLE, SRC and ASID */
};

/*
 * For each thread there are 2 slots.
 */
struct per_thread_miss_queue_entries {
    struct miss_queue_entry slot[2];
};

/*
 * m_queue is the array that has the per thread miss queue slots.
 */
static struct miss_queue {
    struct per_thread_miss_queue_entries m_q_entries[12];
} m_queue;

/*
 * The miss queue is implemented as a 13 entry ring buffer. Write at head and read from tail.
 * When we have to generate a tlb miss we first go scribble over the entry/entries for a specific thread
 * in the m_queue data structure.
 * We then have to enter the thread index to the end of the m_t_ix_queue if the thread index is not
 * already in the queue. If thread 1 is already in the queue then we will just write over the information
 * in the m_queue but we will leave the entry in m_t_ix_queue alone.
 * For the ring buffer if head == tail then queue is empty. We will stop adding entries to the queue
 * if ((head +1 ) %13) == tail.
 */
static struct miss_thread_index_queue {
    uint32_t head;
    uint32_t tail;
    uint32_t miss_thread_index[13];
} m_t_ix_queue;

/*
 * Entries in this data structure get updated when writes happen to CONFIG and
 * PTEX_CFG0 - 2 registers.
 */
static struct mmu_config_state {
    uint32_t ptec_enabled;
    uint32_t vpn_bits;
    uint32_t extra_asid_bits;
    uint32_t ptec_entries_log2;
    uint32_t ptec_index_mask;
    uint32_t atag_base;
    uint32_t entry_base;
    uint32_t rtag_mask;
} ubicom32_mmu;

#define VPN_BITS(x) ((x) >> (32 - ubicom32_mmu.vpn_bits))
#define RESTORE_VPN_BITS(x) ((x) << (32 - ubicom32_mmu.vpn_bits))

static int select_ASID(uint32_t current_thread_num, uint32_t vaddr, uint32_t *asid)

{
    uint32_t upper_vaddr_bits, matches;

    matches = 0;

    upper_vaddr_bits = (vaddr >> 28) & 0x0f;

    if (CMP_0_EN(current_thread_num)) {
	if ((upper_vaddr_bits & ASID_MASK0(current_thread_num)) == ASID_CMP0(current_thread_num)) {
	    *asid = ASID0(current_thread_num);
	    matches++;
	}
    }

    if (CMP_1_EN(current_thread_num)) {
	if ((upper_vaddr_bits & ASID_MASK1(current_thread_num)) == ASID_CMP1(current_thread_num)) {
	    *asid = ASID1(current_thread_num);
	    matches++;
	}
    }

    if (CMP_2_EN(current_thread_num)) {
	if ((upper_vaddr_bits & ASID_MASK2(current_thread_num)) == ASID_CMP2(current_thread_num)) {
	    *asid = ASID2(current_thread_num);
	    matches++;
	}
    }

    return matches;
}

/*************************************
* Start MMU read buffering code
*************************************/

/* When the CPU starts large number of threads before the DDR controller has initialized, the CPU
   will generate a large number of consecutive MMU reads (empiricaly observed over 90).
   The maximum theoretical number of consecutive MMU reads is 32 * 2 * 3 = 192, and the next
   larger power of two is 256. */

#define MMU_READ_BUFFER_ENTRIES 256

typedef struct {

	int paddr, value;

} MMU_READ_BUFFER_ENTRY;

MMU_READ_BUFFER_ENTRY MMU_read_buffer[MMU_READ_BUFFER_ENTRIES];
int MMU_read_buffer_index;

/*
** These routines buffer the data when the "MMU read" occurs. This is required because the jupiterMMU code checks the TLB value
** on a TLB update, and the PTEC value could have been modified between the MMU Read and the TLB Update lines.
*/

void add_entry_to_ring_buffer(int paddr);
void add_entry_to_ring_buffer(int paddr)
{
	uint32_t data;
//	bus::status retval;

//	retval = downstream_bus->read(paddr, data);

//	if (retval != bus::ok) {
//		printf("Error: couldn't read paddr 0x%08x in jupiterMMU::add_entry_to_ring_buffer()\n");
//		archrefcompare_abort();
//	}

	cpu_physical_memory_read(paddr, (uint8_t *)&data, 4);

	data = htonl(data);

	MMU_read_buffer[MMU_read_buffer_index].paddr = paddr;
	MMU_read_buffer[MMU_read_buffer_index].value = (int)data;

	if (archrefcompare_debug_flag)
		printf("    MMU read (paddr: 0x%08x, value: 0x%08x) stored at index %d in ring buffer\n", paddr, (int)data, MMU_read_buffer_index);

	MMU_read_buffer_index = (MMU_read_buffer_index + 1) & (MMU_READ_BUFFER_ENTRIES - 1);
}

/*
** Given a paddr, this routine searches through the ring buffer of previously processed "MMU Read" lines, and returns the value which
** was previously read by the MMU from this paddr.
*/

int find_entry_in_ring_buffer(int paddr, int *value);
int find_entry_in_ring_buffer(int paddr, int *value)
{
	int i, temp_index;
//	bus::status retval;
	uint32_t data;

	temp_index = (MMU_read_buffer_index - 1) & (MMU_READ_BUFFER_ENTRIES - 1);

	for (i=0; i<MMU_READ_BUFFER_ENTRIES; i++) {

		if (MMU_read_buffer[temp_index].paddr == paddr) {

			*value = MMU_read_buffer[temp_index].value;

			cpu_physical_memory_read(paddr, (uint8_t *)&data, 4);

			data = htonl(data);

			if (*value != data) {
				printf("    Note: PTEC data (atag or entry) was modified between \"MMU Read\" and \"TLB Update\" lines\n");
				printf("          paddr: 0x%08x: value at MMU read: 0x%08x, value at TLB Update: 0x%08x\n", paddr, *value, (int)data);
			}

			if (archrefcompare_debug_flag)
				printf("        Previous MMU Read (0x%x = 0x%x) found in ring buffer at index %d\n", paddr, *value, temp_index);

			return 1;
		}

		temp_index = (temp_index - 1) & (MMU_READ_BUFFER_ENTRIES - 1);
	}

	if (archrefcompare_debug_flag)
		printf("        Note: PTEC address (0x%08x) was never read by hardware MMU (does not match previous %d MMU Reads in trace file)\n",
			paddr, MMU_READ_BUFFER_ENTRIES);

	return 0;
}

/*************************************
* End MMU read buffering code
*************************************/
/*************************************
* Start check PTEC check code
*************************************/

/*
** The PTEC checking code herein performs two primary functions:
**
** o Performs a sanity check on the order of MMU read accesses.
**   It ensures the MMU reads the atag, way0, and way1 arrays in the correct order.
**
** o It keeps track of the current PTEC index accesse by the MMU for later use by other routines.
**/

/* This is the equivalent of "ENTRY_BITS" in the manual, but renamed to be less confusing */

#define PTEC_ENTRIES_MASK       ((1 << PTEC_ENTRIES_LOG2) - 1)

typedef enum {

	EXPECTING_PTEC_ATAG_READ,
	EXPECTING_PTEC_ATAG_OR_WAY0_OR_WAY1_READ,
	EXPECTING_PTEC_WAY1_READ,
	EXPECTING_PTEC_WAY1_OR_ATAG_READ

} MMU_PTEC_STATE;

/* The current state of the PTEC-tracking state machine */
MMU_PTEC_STATE mmu_ptec_state;

/* The last MMU PTEC index accessed (deduced from MMU Read lines in mp.trace) */
int last_ptec_index;

/*
** Print the current state of the MMU state machine.
*/

const char *print_MMU_PTEC_state(MMU_PTEC_STATE mmu_ptec_state);
const char *print_MMU_PTEC_state(MMU_PTEC_STATE mmu_ptec_state)
{
	switch (mmu_ptec_state) {

		case EXPECTING_PTEC_ATAG_READ:

			return "PTEC atag read";
			break;

		case EXPECTING_PTEC_ATAG_OR_WAY0_OR_WAY1_READ:

			return "PTEC atag or entry way0 or entry way 1 read";
			break;

		case EXPECTING_PTEC_WAY1_READ:

			return "PTEC way 1 read";
			break;

		case EXPECTING_PTEC_WAY1_OR_ATAG_READ:

			return "PTEC way 1 or atag read";
			break;
	}

	return "UNKNOWN state";
}

/*
** When an "MMU Read" line is processed in archrefcompare, this function is called. It decodes the access
** (determines whether it's an atag, way 0, or way 1 access) and advances the state machine which tracks the MMU state.
**/

void snoop_MMU_read(int address);
void snoop_MMU_read(int address)
{
	int ptec_index, ptec_size;

	add_entry_to_ring_buffer(address);

	ptec_size = 1 << (PTEC_ENTRIES_LOG2 + 2);

	/* Check if the MMU read is an atag read */

	if ((address >= ATAG_BASE) && (address < (ATAG_BASE + ptec_size))) {

		ptec_index  = (address - ENTRY_BASE) >> 2;
		ptec_index &= PTEC_ENTRIES_MASK;
		last_ptec_index = ptec_index;

		if (archrefcompare_debug_flag)
			printf("    Note: MMU read is PTEC atag at index 0x%x\n", ptec_index);

		if ((mmu_ptec_state != EXPECTING_PTEC_ATAG_READ) && (mmu_ptec_state != EXPECTING_PTEC_ATAG_OR_WAY0_OR_WAY1_READ) &&
		    (mmu_ptec_state != EXPECTING_PTEC_WAY1_OR_ATAG_READ)) {
			printf("jupiterMMU: Unexpected PTEC atag read; was expecting a %s\n", print_MMU_PTEC_state(mmu_ptec_state));
			printf("Possible bug in either archrefcompare or trace file, or test may require PTEC checking disabled (-p option)\n");
			archrefcompare_abort();
		}

		mmu_ptec_state = EXPECTING_PTEC_ATAG_OR_WAY0_OR_WAY1_READ;
	}

	/* Check if the MMU read is an entry way 0 read */

	if ((address >= ENTRY_BASE) && (address < (ENTRY_BASE + ptec_size))) {
		ptec_index  = (address - ENTRY_BASE) >> 2;
		ptec_index &= PTEC_ENTRIES_MASK;

		if (archrefcompare_debug_flag)
			printf("    Note: MMU read is PTEC entry way 0 at index: 0x%x\n", ptec_index);

		if (mmu_ptec_state != EXPECTING_PTEC_ATAG_OR_WAY0_OR_WAY1_READ) {
			printf("jupiterMMU: Unexpected PTEC way 0 read; was expecting a %s\n", print_MMU_PTEC_state(mmu_ptec_state));
			printf("Possible bug in either archrefcompare or trace file, or test may require PTEC checking disabled (-p option)\n");
			archrefcompare_abort();
		}

		if (ptec_index != last_ptec_index) {
			printf("jupiterMMU: PTEC way 0 read index (0x%x) does not match previous PTEC atag read index (0x%x)\n", ptec_index, last_ptec_index);
			archrefcompare_abort();
		}

		mmu_ptec_state = EXPECTING_PTEC_WAY1_OR_ATAG_READ;
	}

	/* Check if the MMU read is an entry way 1 read */

	if ((address >= (ENTRY_BASE + ptec_size)) && (address < (ENTRY_BASE + (ptec_size * 2)))) {
		ptec_index  = (address - ENTRY_BASE) >> 2;
		ptec_index &= PTEC_ENTRIES_MASK;

		if (archrefcompare_debug_flag)
			printf("    Note: MMU read is PTEC entry way 1 at index: 0x%x\n", ptec_index);

		if ((mmu_ptec_state != EXPECTING_PTEC_ATAG_OR_WAY0_OR_WAY1_READ) && (mmu_ptec_state != EXPECTING_PTEC_WAY1_OR_ATAG_READ)) {
			printf("jupiterMMU: Unexpected PTEC way 1 read; was expecting a %s\n", print_MMU_PTEC_state(mmu_ptec_state));
			printf("Possible bug in either archrefcompare or trace file, or test may require PTEC checking disabled (-p option)\n");
			archrefcompare_abort();
		}

		if (ptec_index != last_ptec_index) {
			printf("jupiterMMU: PTEC way 1 read index (0x%x) does not match previous PTEC atag read index (0x%x)\n", ptec_index, last_ptec_index);
			archrefcompare_abort();
		}

		mmu_ptec_state = EXPECTING_PTEC_ATAG_READ;
	}
}

/*
** Given a way, ASID, and VPN, this routine reads the PTEC and returns the attributes of the PTEC entry.
*/

int get_PTEC_values(int way, int asid, int vpn, int *ptec_asid, int *ptec_vpn, int *vpn_mask, int *ptec_ppn,
				 int *ptec_read_bit, int *ptec_write_bit, int *ptec_execute_bit, int *ptec_supervisor_bit);
int get_PTEC_values(int way, int asid, int vpn, int *ptec_asid, int *ptec_vpn, int *vpn_mask, int *ptec_ppn,
				 int *ptec_read_bit, int *ptec_write_bit, int *ptec_execute_bit, int *ptec_supervisor_bit)

{
	int asid_shifts, page_shift, ptec_index, atag_paddr, entry_paddr, ptec_atag, ptec_entry;
	int vpn_atag_bits, vpn_entry_bits, vpn_index_bits, extra_page_shifts;

	if ((way < 0) || (way > 2)) {
		printf("internal error: get_PTEC_entry passed illegal way %d\n", way);
		abort();
	}

	if (archrefcompare_debug_flag)
		printf("Checking way %d...\n", way);

	/*
	** This section of code calculates the PTEC index from the ASID and VPN and checks it against the previous MMU accesses.
	** Sometimes this check fails because the TLB Update occurs late, and the MMU Read for the next TLB Update has already occured.
	** Therfore, we only print a note when this check fails instead of aborting.
	*/

	/* For 16k page mode, the VPN needs to be shifted right one because the LSB is always zero. */

	asid_shifts = (PTEC_ENTRIES_LOG2 > 10) ? (PTEC_ENTRIES_LOG2 - 10) : 0;
	page_shift = (PAGE_MASK == 3);

	ptec_index = ((asid << asid_shifts) ^ (vpn >> page_shift)) & PTEC_ENTRIES_MASK;

	if (archrefcompare_debug_flag)
		printf("    ptec_index:0x%x = ((asid:0x%x << asid_shifts:%d) ^ (vpn:0x%x >> page_shift:%d)) & ((1 << PTEC_ENTRIES_LOG2:%d) - 1)\n",
			ptec_index, asid, asid_shifts, vpn, page_shift, PTEC_ENTRIES_LOG2);

	if (ptec_index == last_ptec_index) {
		if (archrefcompare_debug_flag)
			printf("    Calculated PTEC index (0x%x) matches last empirically observed PTEC index (0x%x)\n", ptec_index, last_ptec_index);
	} else {
		printf("    Note: Calculated PTEC index (0x%x) does not match last empirically observed PTEC index (0x%x)\n", ptec_index, last_ptec_index);
		printf("          This may be caused by a delayed TLB Update and is probably harmless\n");
	}

	/*
	** This section of code reads the PTEC atag and entry bits for the specified way.
	*/

	atag_paddr = ATAG_BASE + (ptec_index << 2);

	if (archrefcompare_debug_flag)
		printf("    Reading PTEC ATAG from paddr 0x%08x (ATAG_BASE:0x%x, ptec_index:0x%x)\n", atag_paddr, ATAG_BASE, ptec_index);

	if (!find_entry_in_ring_buffer(atag_paddr, &ptec_atag))
		return 0;

	entry_paddr = ENTRY_BASE + (ptec_index << 2);

	if (way) {
		ptec_atag = EXTRACT_BITS_FROM_INT32(ptec_atag, 0, 16);
		entry_paddr += 1 << (PTEC_ENTRIES_LOG2 + 2);
	} else
		ptec_atag = EXTRACT_BITS_FROM_INT32(ptec_atag, 16, 16);

	if (archrefcompare_debug_flag)
		printf("    Reading PTEC way %d from paddr %08x (ENTRY_BASE:0x%x, ptec_index:0x%x)\n", way, entry_paddr, ENTRY_BASE, ptec_index);

	if (!find_entry_in_ring_buffer(entry_paddr, &ptec_entry))
		return 0;

	/*
	** This section of code actually extracts all the attributes from the previously-read PTEC values.
	*/

	*ptec_asid = EXTRACT_BITS_FROM_INT32(ptec_atag, 0, 10);

	/* Not all the VPN bits are compared due to the RTAG_MASK, so build a compare mask for convenience.
	   Compare all the index and tag bits, but only compare the rtag bits using the RTAG_MASK. */

	*vpn_mask = (RTAG_MASK << (PTEC_ENTRIES_LOG2 + 6)) | ((1 << (PTEC_ENTRIES_LOG2 + 6)) - 1);

	/* VPN from the PTEC: | RTAG_MASK bits from rtag | 6 bits from the ptec atag | PTEC_ENTRIES_LOG2 bits | */

	/* In 16k page mode, the VPN is shifted right one bit before building the tag and rtag (because the LSB is always zero).
	   Therefore, when we rebuild the VPN from the tag and rtag in 16k page mode, the VPN needs to be shifted left one bit. */

	extra_page_shifts = (PAGE_MASK == 3);

#ifdef USE_NONCACHEABLE_BIT
	vpn_entry_bits = ((ptec_entry >> 6) & RTAG_MASK) << (PTEC_ENTRIES_LOG2 + 6 + extra_page_shifts);        /* high bits from the ptec_entry */
#else
	vpn_entry_bits = ((ptec_entry >> 5) & RTAG_MASK) << (PTEC_ENTRIES_LOG2 + 6 + extra_page_shifts);        /* high bits from the ptec_entry */
#endif
	vpn_atag_bits  = ((ptec_atag >> 10) & ((1 << 6) - 1)) << (PTEC_ENTRIES_LOG2 + extra_page_shifts);	/* low bits from the ptec_atag */
	vpn_index_bits = ((ptec_index ^ (*ptec_asid << asid_shifts)) & PTEC_ENTRIES_MASK) << page_shift;

	/* Build the final VPN from the partial bits from the entry, atag, and PTEC index bits */

	*ptec_vpn = vpn_entry_bits | vpn_atag_bits | vpn_index_bits;

	if (archrefcompare_debug_flag) {

		printf("    vpn_mask:0x%x = (RTAG_MASK:0x%x << (PTEC_ENTRIES_LOG2:%d + 6)) | ((1 << (PTEC_ENTRIES_LOG2:%d + 6)) - 1)\n",
			*vpn_mask, RTAG_MASK, PTEC_ENTRIES_LOG2, PTEC_ENTRIES_LOG2);

		printf("    ptec_vpn:0x%x = vpn_entry_bits:0x%x | vpn_atag_bits:0x%x | vpn_index_bits:0x%x, calculated as:\n",
			*ptec_vpn, vpn_entry_bits, vpn_atag_bits, vpn_index_bits);

		printf("        extra_page_shifts:%d = (PAGE_MASK:%d == 3)\n", extra_page_shifts, PAGE_MASK);
#ifdef USE_NONCACHEABLE_BIT
		printf("        vpn_entry_bits:0x%x = ((ptec_entry:0x%x >> 6) & RTAG_MASK:0x%x) << (PTEC_ENTRIES_LOG2:%d + 6 + extra_page_shifts:%d)\n",
			vpn_entry_bits, ptec_entry, RTAG_MASK, PTEC_ENTRIES_LOG2, extra_page_shifts);
#else
		printf("        vpn_entry_bits:0x%x = ((ptec_entry:0x%x >> 5) & RTAG_MASK:0x%x) << (PTEC_ENTRIES_LOG2:%d + 6 + extra_page_shifts:%d)\n",
			vpn_entry_bits, ptec_entry, RTAG_MASK, PTEC_ENTRIES_LOG2, extra_page_shifts);
#endif
		printf("        vpn_atag_bits:0x%x = ((ptec_atag:0x%x >> 10) & ((1 << 6) - 1)) << (PTEC_ENTRIES_LOG2:0x%x + extra_page_shifts:%d)\n",
			vpn_atag_bits, ptec_atag, PTEC_ENTRIES_LOG2, extra_page_shifts);

		printf("        vpn_index_bits:0x%x = ((ptec_index:0x%x ^ (ptec_asid:0x%x << asid_shifts:%d)) & PTEC_ENTRIES_MASK:0x%x) << page_shift:%d\n",
			vpn_index_bits, ptec_index, *ptec_asid, asid_shifts, PTEC_ENTRIES_MASK, page_shift);
	}

	/* Extract all the other attributes from the PTEC. */

	*ptec_ppn = EXTRACT_BITS_FROM_INT32(ptec_entry, 12, 20) >> page_shift;

	*ptec_read_bit       = EXTRACT_BITS_FROM_INT32(ptec_entry, 4, 1);
	*ptec_write_bit      = EXTRACT_BITS_FROM_INT32(ptec_entry, 3, 1);
	*ptec_execute_bit    = EXTRACT_BITS_FROM_INT32(ptec_entry, 2, 1);
	*ptec_supervisor_bit = EXTRACT_BITS_FROM_INT32(ptec_entry, 1, 1);

	return 1;
}

/*
** When a value is loaded into a TLB entry, this routine is called to check the value against the PTEC.
*/

void Check_TLB_Value(TLB_ENTRY tlb_value);
void Check_TLB_Value(TLB_ENTRY tlb_value)
{
	int tlb_asid, tlb_vpn, tlb_ppn, tlb_read_bit, tlb_write_bit, tlb_execute_bit, tlb_supervisor_bit;
	int ptec_asid, ptec_vpn, vpn_mask, ptec_ppn, ptec_read_bit, ptec_write_bit, ptec_execute_bit, ptec_supervisor_bit;
	int match_way0, match_way1, read_flag;

	tlb_asid           = TLB_ENTRY_ASID(tlb_value);
	tlb_vpn            = TLB_ENTRY_VPN(tlb_value);
	tlb_ppn            = TLB_ENTRY_PPN(tlb_value);
	tlb_read_bit       = TLB_ENTRY_READ_BIT(tlb_value);
	tlb_write_bit      = TLB_ENTRY_WRITE_BIT(tlb_value);
	tlb_execute_bit    = TLB_ENTRY_EXECUTE_BIT(tlb_value);
	tlb_supervisor_bit = TLB_ENTRY_SUPERVISOR_BIT(tlb_value);

	match_way0 = match_way1 = 1;

	read_flag = get_PTEC_values(0, tlb_asid, tlb_vpn, &ptec_asid, &ptec_vpn, &vpn_mask, &ptec_ppn, &ptec_read_bit, &ptec_write_bit,
				    &ptec_execute_bit, &ptec_supervisor_bit);

	if (read_flag) {

		if ((ptec_asid != tlb_asid) || ((ptec_vpn & vpn_mask) != (tlb_vpn & vpn_mask)) || (ptec_ppn != tlb_ppn)) {

			match_way0 = 0;

			if (archrefcompare_debug_flag)
				printf("    TLB does not match PTEC way 0 translation: RTAG_MASK: 0x%x, ASID/VPN/PPN: TLB: 0x%x/0x%x/0x%x, way 0: 0x%x/0x%x/0x%x\n",
					RTAG_MASK, tlb_asid, tlb_vpn, tlb_ppn, ptec_asid, ptec_vpn, ptec_ppn);
		}

		if ((ptec_read_bit != tlb_read_bit) || (ptec_write_bit != tlb_write_bit) || (ptec_execute_bit != tlb_execute_bit) ||
		    (ptec_supervisor_bit != tlb_supervisor_bit)) {

			match_way0 = 0;

			if (archrefcompare_debug_flag)
				printf("    TLB does not match PTEC way 0 permissions: TLB: %d%d%d%d, PTEC way 0: %d%d%d%d\n",
					tlb_read_bit, tlb_write_bit, tlb_execute_bit, tlb_supervisor_bit,
					ptec_read_bit, ptec_write_bit, ptec_execute_bit, ptec_supervisor_bit);
		}

	} else {
		match_way0 = 0;

		if (archrefcompare_debug_flag)
			printf("    Way 0 check aborted; hardware MMU never read PTEC way 0\n");
	}

	if (archrefcompare_debug_flag && match_way0)
		printf("    TLB load matched PTEC way 0\n");

	read_flag = get_PTEC_values(1, tlb_asid, tlb_vpn, &ptec_asid, &ptec_vpn, &vpn_mask, &ptec_ppn, &ptec_read_bit, &ptec_write_bit,
				    &ptec_execute_bit, &ptec_supervisor_bit);

	if (read_flag) {

		if ((ptec_asid != tlb_asid) || ((ptec_vpn & vpn_mask) != (tlb_vpn & vpn_mask)) || (ptec_ppn != tlb_ppn)) {

			match_way1 = 0;

			if (archrefcompare_debug_flag)
				printf("    TLB does not match PTEC way 1 translation: RTAG_MASK: 0x%x, ASID/VPN/PPN: TLB: 0x%x/0x%x/0x%x, way 1: 0x%x/0x%x/0x%x\n",
					RTAG_MASK, tlb_asid, tlb_vpn, tlb_ppn, ptec_asid, ptec_vpn, ptec_ppn);
		}

		if ((ptec_read_bit != tlb_read_bit) || (ptec_write_bit != tlb_write_bit) || (ptec_execute_bit != tlb_execute_bit) ||
		    (ptec_supervisor_bit != tlb_supervisor_bit)) {

			match_way1 = 0;

			if (archrefcompare_debug_flag)
				printf("    TLB does not match PTEC way 1 permissions: TLB: %d%d%d%d, PTEC way 0: %d%d%d%d\n",
					tlb_read_bit, tlb_write_bit, tlb_execute_bit, tlb_supervisor_bit,
					ptec_read_bit, ptec_write_bit, ptec_execute_bit, ptec_supervisor_bit);
		}

	} else {
		match_way1 = 0;

		if (archrefcompare_debug_flag)
			printf("    Aborted way 1 check; hardware MMU never read PTEC way 1\n");
	}


	if (match_way0 && match_way1) {

		printf("TLB load matched both way 0 and way 1\n");
		printf("Possible bug in either test code, archrefcompare, trace file, or test may require TLB value checking disabled (-t option)\n");
		archrefcompare_abort();

	} else if (!match_way0 && !match_way1) {

		printf("TLB load did not match either PTEC way 0 or way 1\n");
		printf("Possible bug in either archrefcompare or trace file, or test may require TLB value checking disabled (-t option)\n");
		archrefcompare_abort();

	} else if (archrefcompare_debug_flag)

		printf("    TLB load matched only PTEC way %d\n", match_way1);
}

/*************************************
* End check PTEC check code
*************************************/
/*************************************
* Start archrefcompare MMU code
*************************************/

void debug_TLB_entry(int insn_flag, int tlb_index, TLB_ENTRY tlb_value);
void debug_TLB_entry(int insn_flag, int tlb_index, TLB_ENTRY tlb_value)
{
	int page_shifts, vaddr, paddr;

	page_shifts = (PAGE_MASK == 3) ? 14 : 13;

	vaddr = TLB_ENTRY_VPN(tlb_value) << page_shifts;
	paddr = TLB_ENTRY_PPN(tlb_value) << page_shifts;

	printf(insn_flag ? "insn" : "data");

	printf(" TLB entry: %d: ASID: 0x%03x, VPN: 0x%05x (vaddr: 0x%08x), PPN: 0x%05x (paddr: 0x%08x), R: %d, W: %d, X: %d, S: %d, V: %d\n",
		tlb_index, TLB_ENTRY_ASID(tlb_value), TLB_ENTRY_VPN(tlb_value), vaddr, TLB_ENTRY_PPN(tlb_value), paddr,
		TLB_ENTRY_READ_BIT(tlb_value), TLB_ENTRY_WRITE_BIT(tlb_value), TLB_ENTRY_EXECUTE_BIT(tlb_value),
		TLB_ENTRY_SUPERVISOR_BIT(tlb_value), TLB_ENTRY_VALID_BIT(tlb_value));
}

void TLB_write(uint32_t address, uint64_t data, int insn_flag);
void TLB_write(uint32_t address, uint64_t data, int insn_flag)
{
	TLB_ENTRY *tlb;

	if (address >= TLB_ENTRIES_NUM) {
		printf(insn_flag ? "Tried to write to invalid insn TLB entry %d" : "Tried to write to invalid data TLB entry %d", address);
		archrefcompare_abort();
	}

	/* If the page size is 16k, then we clear the LSB of the VPN (bit 34).
	   The hardware implementation does this for speed to avoid an extra gate when comparing VPNs. */

#ifdef USE_NONCACHEABLE_BIT
	if (PAGE_MASK == 3)
		data = data & ~(1LL << 35);             // Stupid host_int_64 does not support &=
#else
	if (PAGE_MASK == 3)
		data = data & ~(1LL << 34);             // Stupid host_int_64 does not support &=
#endif
	tlb = insn_flag ? insn_tlb : data_tlb;

	if (archrefcompare_debug_flag) {
		printf("        old ");
		debug_TLB_entry(insn_flag, address, tlb[address]);
	}

	tlb[address] = data;

	if (archrefcompare_debug_flag) {
		printf("        new ");
		debug_TLB_entry(insn_flag, address, tlb[address]);
	}

	/* If the value is zero, then it's a TLB invalidate, so don't check it. */

	if (PTEC_ENABLED && TLB_value_check_flag && data)
		Check_TLB_Value(data);
}

int jupiterMMU_protection_check(TLB_ENTRY *tlb, int index, int read_bit, int write_bit, int execute_bit);
int jupiterMMU_protection_check(TLB_ENTRY *tlb, int index, int read_bit, int write_bit, int execute_bit)

{
	TLB_ENTRY tlb_value;

	tlb_value = tlb[index];

	if (read_bit && !TLB_ENTRY_READ_BIT(tlb_value)) {
		printf("jupiterMMU: read protection violation\n");
		return 0;
	}

	if (write_bit && !TLB_ENTRY_WRITE_BIT(tlb_value)) {
		printf("jupiterMMU: write protection violation\n");
		return 0;
	}

	if (execute_bit && !TLB_ENTRY_EXECUTE_BIT(tlb_value)) {
		printf("jupiterMMU: execute protection violation\n");
		return 0;
	}

	if (privilege_level && !TLB_ENTRY_SUPERVISOR_BIT(tlb_value)) {
		printf("jupiterMMU: supervisor protection violation: CPU in user mode accessed TLB requiring supervisor mode\n");
		return 0;
	}

	return 1;
}

void jupiterMMU_debug_TLB_entry(int insn_flag, int tlb_index, TLB_ENTRY tlb_value);
void jupiterMMU_debug_TLB_entry(int insn_flag, int tlb_index, TLB_ENTRY tlb_value)

{
	int page_shifts, vaddr, paddr;

	if (!archrefcompare_debug_flag)
		return;

	page_shifts = (PAGE_MASK == 3) ? 14 : 13;

	vaddr = TLB_ENTRY_VPN(tlb_value) << page_shifts;
	paddr = TLB_ENTRY_PPN(tlb_value) << page_shifts;

	printf(insn_flag ? "insn" : "data");

	printf(" TLB entry: %d: ASID: 0x%03x, VPN: 0x%05x (vaddr: 0x%08x), PPN: 0x%05x (paddr: 0x%08x), R: %d, W: %d, X: %d, S: %d, V: %d\n",
		tlb_index, TLB_ENTRY_ASID(tlb_value), TLB_ENTRY_VPN(tlb_value), vaddr, TLB_ENTRY_PPN(tlb_value), paddr,
		TLB_ENTRY_READ_BIT(tlb_value), TLB_ENTRY_WRITE_BIT(tlb_value), TLB_ENTRY_EXECUTE_BIT(tlb_value),
		TLB_ENTRY_SUPERVISOR_BIT(tlb_value), TLB_ENTRY_VALID_BIT(tlb_value));
}

uint32_t archrefcompare_tlb_check(uint32_t asid, uint32_t vaddr, uint32_t *return_ptec_entry, uint32_t is_read, uint32_t is_ifetch);
uint32_t archrefcompare_tlb_check(uint32_t asid, uint32_t vaddr, uint32_t *return_ptec_entry, uint32_t is_read, uint32_t is_ifetch)

{
	int i, matches, upper_vaddr_bits;
	int read_bit, write_bit, execute_bit;
	uint32_t paddr, ptec_entry;
	TLB_ENTRY *tlb, tlb_value, matching_tlb_value;

	if (archrefcompare_debug_flag)
		printf("archrefcompare_tlb_check(): asid: %x, vaddr: %08x, is_read: %d, is_ifetch: %d\n", asid, vaddr, is_read, is_ifetch);

	tlb = is_ifetch ? insn_tlb : data_tlb;

	read_bit    = is_read;
	write_bit   = !is_read;
	execute_bit = is_ifetch;

	/* Calculate the upper vaddr bits. */

	upper_vaddr_bits = vaddr >> 13;

	/* The upper_vaddr_bits is always the highest 13 bits, so the LSB must be cleared in 16k page mode. */

	if (PAGE_MASK == 3)
		upper_vaddr_bits &= ~1;

	for (matches=i=0; i<TLB_ENTRIES_NUM; i++) {

		tlb_value = tlb[i];

		if (!TLB_ENTRY_VALID_BIT(tlb_value))
			continue;

		if (archrefcompare_debug_flag)
			printf("checking ");

		jupiterMMU_debug_TLB_entry(execute_bit, i, tlb_value);

		if (TLB_ENTRY_ASID(tlb_value) != asid) {

			if (archrefcompare_debug_flag)
				printf("...this TLB entry does not match (reason: TLB ASID: 0x%x != ASID: 0x%x)\n", TLB_ENTRY_ASID(tlb_value), asid);

			continue;

		} else if (TLB_ENTRY_VPN(tlb_value) != upper_vaddr_bits) {

			if (archrefcompare_debug_flag)
				printf("...this TLB entry does not match (reason: TLB VPN: 0x%x != VPN: 0x%x)\n", TLB_ENTRY_VPN(tlb_value), upper_vaddr_bits);

			continue;

		} else {

			matching_tlb_value = tlb_value;

			if (archrefcompare_debug_flag)
				printf("...this TLB entry matches ASID & VPN\n");
                }

		if (!jupiterMMU_protection_check(tlb, i, read_bit, write_bit, execute_bit)) {
			printf("MMU protection violation\n");
			printf("Should never happen; probably bug in trace file or archrefcompare\n");
			archrefcompare_abort();
		}

		switch (PAGE_MASK) {

			case 1:

				/* The TLB_ENTRY_PPN is always 19 bits regardless of the page size.
				   For 8k mode, we need to OR in the lower 13 bits of the virtual address. */

				paddr = (TLB_ENTRY_PPN(tlb_value) << 13) | (vaddr & ((1 << 13) - 1));
				matches++;
				break;

			case 3:

				/* The TLB_ENTRY_PPN is always 19 bits regardless of the page size.
				   For 16k mode, we need to OR in the lower 14 bits of the virtual address. */

				paddr = (TLB_ENTRY_PPN(tlb_value) << 13) | (vaddr & ((1 << 14) - 1));
				matches++;
				break;

			default:

				/* Page size is reserved (Jupiter manual section 9.1.16) */
				assert(0);
		}
	}

	if (matches != 1) {
		printf("%d TLB matches; should be one match\n", matches);
		abort();
	}

	/* Fake a PTEC entry to be compatible with the rest of QEMU. */
	/* PTEC format (big-endian): | 20 bits PFN | 7 bits rtag | 1 bit read | 1 bit write | 1 bit execute | 1 bit supervisor | 1 bit pad | */

	if (return_ptec_entry) {

		/* The caller only uses the physical page number part of the PTEC entry. */

		if (PAGE_MASK == 3)
			ptec_entry = TLB_ENTRY_PPN(matching_tlb_value) << 13;
		 else
			ptec_entry = TLB_ENTRY_PPN(matching_tlb_value) << 12;

		ptec_entry |=   (TLB_ENTRY_WRITE_BIT(matching_tlb_value) << 4) | (TLB_ENTRY_WRITE_BIT(matching_tlb_value) << 3) |
		        (TLB_ENTRY_EXECUTE_BIT(matching_tlb_value) << 2) | (TLB_ENTRY_SUPERVISOR_BIT(matching_tlb_value << 1));

		*return_ptec_entry = ptec_entry;

		if (archrefcompare_debug_flag)
			printf("...returning paddr: %08x\n", *return_ptec_entry);
	}

	return MMU_MISSQW0_TYPE_SERVICED;
}

/*************************************
* End archrefcompare MMU code
*************************************/

#define PTEC_USER	(1 << 1)
#define PTEC_EXECUTE	(1 << 2)
#define PTEC_WRITE	(1 << 3)
#define PTEC_READ	(1 << 4)

/*
 * This routine will take a vaddr and see if it is present in the ptec.
 * If it is not it will return READ_MISS/ WRITE_MISS
 * If there is a hit it will test and see the privilege bits and return the
 * PRIV_ACC_READ/Write. The caller will then have to generate the
 * proper entries in the miss queue.
 * If the asid select bombs it will generate the proper synchronous error trap
 * and fource it to bale out to QEMU outer loop
 */
uint32_t tlb_check(CPUState *env, uint32_t *vpn, uint32_t *asid, uint32_t vaddr, uint32_t *return_ptec_entry, uint32_t is_read, uint32_t is_ifetch)
{
    uint32_t ptec_index = 0;
    uint32_t thread_mask = 1 << env->cpu_index;
    uint32_t match;
    uint32_t asid_mask;
    uint32_t atag, rtag;
    uint32_t ptec_entry;
    uint32_t atag_entry;
    uint32_t user_mode = env->thread_regs[0x2d] & (1 << CSR_PRIV_BIT);

    match = select_ASID(env->cpu_index, vaddr, asid);

    /*
     * If match is any value other than 1 then trigger a ASYNC_TRAP and get out.
     */
    if (match != 1) {
	/*
	 * Generate a synchronous error trap and get out.
	 */
	if (is_ifetch) {
	    generate_trap(env, thread_mask, 1 << TRAP_CAUSE_I_SERROR);
	} else if (is_read) {
	    /*
	     * SRC1_SERROR
	     */
	    generate_trap(env, thread_mask, 1 << TRAP_CAUSE_SRC1_SERROR);
	} else {
	    /*
	     * DST_SERROR.
	     */
	    generate_trap(env, thread_mask, 1 << TRAP_CAUSE_DST_SERROR);
	}

	/*
	 * Raise the synchronous error interrupt in int_stat0
	 */
	ubicom32_raise_interrupt(31);

	return MMU_MISSQW0_TYPE_SERROR;
    }

#ifdef ARCHREFCOMPARE
    if (archrefcompare_flag)
        return archrefcompare_tlb_check(*asid, vaddr, return_ptec_entry, is_read, is_ifetch);
    else {
#endif
    /*
     * Got a valid asid. Now compute the ptec_index.
     */
    *vpn = VPN_BITS(vaddr);
    asid_mask = *asid << ubicom32_mmu.extra_asid_bits;
    ptec_index = (*vpn ^ asid_mask) & ubicom32_mmu.ptec_index_mask;
    atag = (((*vpn >> ubicom32_mmu.ptec_entries_log2) & 0x3f) << 10) | *asid;
    rtag = (*vpn >> (ubicom32_mmu.ptec_entries_log2 + 6)) & ubicom32_mmu.rtag_mask;

    /*
     * Go fetch the atag entry from PTEC.
     */
    atag_entry = ldl_phys(ubicom32_mmu.atag_base + ptec_index * 4);

    if ((atag_entry & 0xffff) == atag) {
        /*
         * hit on way 1. Read the ptec_entry from the correct array.
         */
        ptec_entry = ldl_phys(ubicom32_mmu.entry_base + (ptec_index  + ( 1 << ubicom32_mmu.ptec_entries_log2)) * 4);
    } else if (((atag_entry >> 16) & 0xffff) == atag) {
        /*
         * hit on way 0. Read the ptec_entry from the correct array.
         */
        ptec_entry = ldl_phys(ubicom32_mmu.entry_base + ptec_index * 4);
    } else {
        /*
         * Generate a TLB miss and return.
         */
        if (is_read) {
            return MMU_MISSQW0_TYPE_MISS_READ;
        }

            return MMU_MISSQW0_TYPE_MISS_WRITE;
    }

    if (return_ptec_entry) {
        *return_ptec_entry = ptec_entry;
    }
    /*
     * Test for a rtag hit.
     */
    if (((ptec_entry >> 5) & ubicom32_mmu.rtag_mask) != rtag) {
        /*
         * Rtag mismatch. Generate a TLB miss and return.
         */
        if (is_read) {
            return MMU_MISSQW0_TYPE_MISS_READ;
        }

        return MMU_MISSQW0_TYPE_MISS_WRITE;
    }

    /*
     * Atag and rtag match. Now to check if we have proper privileges.
     */
    if (user_mode && ((ptec_entry & PTEC_USER) == 0)) {
        /*
         * Privelage violation
         */
        if (is_read) {
            return MMU_MISSQW0_TYPE_PRIVACC_READ;
        }

        return MMU_MISSQW0_TYPE_PRIVACC_WRITE;
    }

    /*
     * If i_fetch and no execute then it is a READ violation
     */
    if (is_ifetch) {
        if ((ptec_entry & PTEC_EXECUTE) == 0) {
            /*
             * Privelage violation
             */
            return MMU_MISSQW0_TYPE_PRIVACC_READ;
        }

        return MMU_MISSQW0_TYPE_SERVICED;
    }


    /*
     * If read and no read permission then it is a READ violation
     */
    if (is_read) {
        if ((ptec_entry & PTEC_READ) == 0) {
            /*
             * Privelage violation
             */
            return MMU_MISSQW0_TYPE_PRIVACC_READ;
        }

        return MMU_MISSQW0_TYPE_SERVICED;
    }

    /*
     * If no write permission then this is write violation
     */
    if ((ptec_entry & PTEC_WRITE) == 0) {
        /*
         * Privelage violation
         */
        return MMU_MISSQW0_TYPE_PRIVACC_WRITE;
    }

    /*
     * This has been successfuly serviced.
     */
    return MMU_MISSQW0_TYPE_SERVICED;
#ifdef ARCHREFCOMPARE
     }
#endif
}

/*
 * This routine will take a vaddr and see if it is present in the ptec.
 * If it is not it will return READ_MISS/ WRITE_MISS
 * If there is a hit it will test and see the privilege bits and return the
 * PRIV_ACC_READ/Write. The caller will then have to generate the
 * proper entries in the miss queue.
 * If the asid select bombs it will generate the proper synchronous error trap
 * and fource it to bale out to QEMU outer loop
 */
uint32_t tlb_check_no_serror(CPUState *env, uint32_t *vpn, uint32_t *asid, uint32_t vaddr, uint32_t *return_ptec_entry, uint32_t is_read, uint32_t is_ifetch)
{
    uint32_t ptec_index = 0;
    uint32_t match;
    uint32_t asid_mask;
    uint32_t atag, rtag;
    uint32_t ptec_entry;
    uint32_t atag_entry;
    uint32_t user_mode = env->thread_regs[0x2d] & (1 << CSR_PRIV_BIT);

    match = select_ASID(env->cpu_index, vaddr, asid);

    /*
     * If match is any value other than 1 then trigger a ASYNC_TRAP and get out.
     */
    if (match != 1) {
	return MMU_MISSQW0_TYPE_SERROR;
    }

#ifdef ARCHREFCOMPARE
    if (archrefcompare_flag)
        return archrefcompare_tlb_check(*asid, vaddr, return_ptec_entry, is_read, is_ifetch);
    else {
#endif
    /*
     * Got a valid asid. Now compute the ptec_index.
     */
    *vpn = VPN_BITS(vaddr);
    asid_mask = *asid << ubicom32_mmu.extra_asid_bits;
    ptec_index = (*vpn ^ asid_mask) & ubicom32_mmu.ptec_index_mask;
    atag = (((*vpn >> ubicom32_mmu.ptec_entries_log2) & 0x3f) << 10) | *asid;
    rtag = (*vpn >> (ubicom32_mmu.ptec_entries_log2 + 6)) & ubicom32_mmu.rtag_mask;

    /*
     * Go fetch the atag entry from PTEC.
     */
    atag_entry = ldl_phys(ubicom32_mmu.atag_base + ptec_index * 4);

    if ((atag_entry & 0xffff) == atag) {
	/*
	 * hit on way 1. Read the ptec_entry from the correct array.
	 */
	ptec_entry = ldl_phys(ubicom32_mmu.entry_base + (ptec_index  + ( 1 << ubicom32_mmu.ptec_entries_log2)) * 4);
    } else if (((atag_entry >> 16) & 0xffff) == atag) {
	/*
	 * hit on way 0. Read the ptec_entry from the correct array.
	 */
	ptec_entry = ldl_phys(ubicom32_mmu.entry_base + ptec_index * 4);
    } else {
	/*
	 * Generate a TLB miss and return.
	 */
	if (is_read) {
	    return MMU_MISSQW0_TYPE_MISS_READ;
	}

	    return MMU_MISSQW0_TYPE_MISS_WRITE;
    }

    if (return_ptec_entry) {
	*return_ptec_entry = ptec_entry;
    }

    /*
     * Test for a rtag hit.
     */
    if (((ptec_entry >> 5) & ubicom32_mmu.rtag_mask) != rtag) {
	/*
	 * Rtag mismatch. Generate a TLB miss and return.
	 */
	if (is_read) {
	    return MMU_MISSQW0_TYPE_MISS_READ;
	}

	return MMU_MISSQW0_TYPE_MISS_WRITE;
    }

    /*
     * Atag and rtag match. Now to check if we have proper privileges.
     */
    if (user_mode && ((ptec_entry & PTEC_USER) == 0)) {
	/*
	 * Privelage violation
	 */
	if (is_read) {
	    return MMU_MISSQW0_TYPE_PRIVACC_READ;
	}

	return MMU_MISSQW0_TYPE_PRIVACC_WRITE;
    }

    /*
     * If i_fetch and no execute then it is a READ violation
     */
    if (is_ifetch) {
	if ((ptec_entry & PTEC_EXECUTE) == 0) {
	    /*
	     * Privelage violation
	     */
	    return MMU_MISSQW0_TYPE_PRIVACC_READ;
	}

	return MMU_MISSQW0_TYPE_SERVICED;
    }


    /*
     * If read and no read permission then it is a READ violation
     */
    if (is_read) {
	if ((ptec_entry & PTEC_READ) == 0) {
	    /*
	     * Privelage violation
	     */
	    return MMU_MISSQW0_TYPE_PRIVACC_READ;
	}

	return MMU_MISSQW0_TYPE_SERVICED;
    }

    /*
     * If no write permission then this is write violation
     */
    if ((ptec_entry & PTEC_WRITE) == 0) {
	/*
	 * Privelage violation
	 */
	return MMU_MISSQW0_TYPE_PRIVACC_WRITE;
    }

    /*
     * This has been successfuly serviced.
     */
    return MMU_MISSQW0_TYPE_SERVICED;
#ifdef ARCHREFCOMPARE
    }
#endif
}

void create_fault(uint32_t tnum, uint32_t vpn, uint32_t asid, uint32_t type, uint32_t slot, uint32_t dtlb)
{
    uint32_t w0, w1;
    uint32_t ix_q_tail = m_t_ix_queue.tail;

    if ((dtlb == 0) && (type == MMU_MISSQW0_TYPE_PRIVACC_WRITE)) {
	    printf("Bad combination\n");
    }

    /*
     * Create w0  PGD|TYPE|SRC|TNUM
     */
    w0 = PGD(tnum) | (type << 6) | (dtlb << 5) | tnum;

    /*
     * Create w1 VPN|MULTIPLE|SRC|ASID
     */
    w1 = (RESTORE_VPN_BITS(vpn)) | (dtlb << 10) | asid;

    /*
     * Load the entries.
     */
    m_queue.m_q_entries[tnum].slot[slot].w0 = w0;
    m_queue.m_q_entries[tnum].slot[slot].w1 = w1;

    /*
     * If slot number is 1 we have to set the multiple bit in slot 0.
     */
    if (slot) {
	m_queue.m_q_entries[tnum].slot[0].w1 |= (1 << 11);
    }

    /*
     * If the slot is zero see if the thread is already in the ring buffer. If it is nothing else to do.
     */
    if (slot) {
	return;
    }

    while (ix_q_tail != m_t_ix_queue.head) {
	if (m_t_ix_queue.miss_thread_index[ix_q_tail++] == tnum) {
	    /*
	     * Preexisting entry. Nothing to do.
	     */
	    return;
	}
	ix_q_tail %= 13;
    }

    /*
     * Add the thread to the queue via the head pointer.
     */
    m_t_ix_queue.miss_thread_index[m_t_ix_queue.head++] = tnum;
    m_t_ix_queue.head %= 13;

    /*
     * Raise the MMU interrupt.
     */
    ubicom32_raise_interrupt(29);

    /*
     * Block the thread.
     * BUS_ST0 set the thread bit.
     * BUS_ST1 set bit for dtlb, clrer for itlb.
     * BUS_ST2 set bit for MMU fault.
     * BUS_ST3 don't care.
     */
    tnum = 1 << tnum;
    ocp_mmu[BUS_ST0] |= tnum;
    ocp_mmu[BUS_ST2] |= tnum;
    if (dtlb) {
	/*
	 * Set bit in BUS_ST1
	 */
	ocp_mmu[BUS_ST1] |= tnum;
    } else {
	/*
	 * Clear the bit.
	 */
	ocp_mmu[BUS_ST1] &= ~tnum;
    }
}

static void ubicom32_ocp_qemu_writeb (void *opaque, target_phys_addr_t addr,
			      uint32_t val)
{
    struct ocp_info *ocp = opaque;

#ifdef ARCHREFCOMPARE
    int shifts;

    if (archrefcompare_flag) {
        shifts = (3 - (addr & 3)) << 3;
        addr = (addr & 0xfff) >> 2;
        ocp_memory[addr] &= (0xff << shifts) ^ -1;
        ocp_memory[addr] |= (val & 0xff) << shifts;
        return;
    }
#endif

    addr &= 0xfff;
    if ((addr >= 0) && addr < 0x100) {
	uint32_t word_address = addr & ~0x3;
	uint32_t sub_byte = addr & 0x3;
	uint64_t now, next;
	uint32_t shift = 3 - sub_byte;
	uint32_t mask = 0xff << shift;

	val <<= shift;

	/*
	 * Convert word address into an index
	 */
	word_address /= 4;
	ocp_general[word_address] &= ~mask;
	ocp_general[word_address] |= val;

	if (addr < 4) {
	    /*
	     * Write to core PLL.
	     */
	    uint32_t pll_val = ocp_general[word_address];

	    /*
	     * If PLL output source is external clock then change the sysval frequency to 12000000 Else compute it.
	     */
	    if (pll_val & 0x10) {
		/*
		 * Compute the PLL frequency.
		 */
		uint32_t nr = GEN_GET_CLK_PLL_NR(pll_val);
		uint32_t nf = GEN_GET_CLK_PLL_NF(pll_val);
		uint32_t od = GEN_GET_CLK_PLL_OD(pll_val);
		ocp->sysval_frequency = ((ocp->mptval_frequency / nr) * nf) / od;

		if ((pll_val & 0xf) != 1) {
		    uint32_t output_div = (pll_val & 0xf) + 1;
		    ocp->sysval_frequency /= output_div;
		}

	    } else {
		ocp->sysval_frequency = 12000000;
	    }

	    /*
	     * Readjust the sysval timer to the new frequency.
	     */
	    now = qemu_get_clock(vm_clock);
	    next = now + muldiv64(1, ticks_per_sec, ocp->sysval_frequency);
	    qemu_mod_timer(ocp->sysval_timer, next);
	    return;
	}
    }
    //printf("Byte access at address 0x%03x val = 0x%08x\n", addr&0xfff, val);
}

static void ubicom32_ocp_qemu_writew (void *opaque, target_phys_addr_t addr,
			      uint32_t val)
{
    //printf("Word access at address 0x%03x val = 0x%08x\n", addr&0xfff, val);
    struct ocp_info *ocp = opaque;

#ifdef ARCHREFCOMPARE
    int shifts;

    if (archrefcompare_flag) {
        shifts = (1 ^ (addr & 1)) << 4;
        addr = (addr & 0xfff) >> 1;
        ocp_memory[addr] &= (0xffff << shifts) ^ -1;
        ocp_memory[addr] |= (val & 0xffff) << shifts;
        return;
    }
#endif

    uint32_t word_address = addr & ~0x3;
    uint32_t sub_byte = addr & 0x3;
    addr &= 0xfff;
    if ((addr >= 0) && addr < 0x100) {
	uint64_t now, next;

	/*
	 * Convert word address into an index
	 */
	word_address /= 4;

	if (sub_byte == 0) {
	    /*
	     * Write to the upper half of the location.
	     */
	    ocp_general[word_address] &=  0x0000ffff;
	    ocp_general[word_address] |= (val << 16);
	} else {
	    /*
	     * Write to lower half of pll reg.
	     */
	    ocp_general[word_address] &= 0xffff0000;
	    ocp_general[word_address] |= (val & 0xffff);
	}

	if (addr == 0 || addr == 2) {
	    uint32_t pll_val = ocp_general[word_address];
	    /*
	     * If PLL output source is external clock then change the sysval frequency to 12000000 Else compute it.
	     */
	    if (pll_val & 0x10) {
		/*
		 * Compute the PLL frequency.
		 */
		uint32_t nr = GEN_GET_CLK_PLL_NR(pll_val);
		uint32_t nf = GEN_GET_CLK_PLL_NF(pll_val);
		uint32_t od = GEN_GET_CLK_PLL_OD(pll_val);
		ocp->sysval_frequency = ((ocp->mptval_frequency / nr) * nf) / od;

		if ((pll_val & 0xf) != 1) {
		    uint32_t output_div = (pll_val & 0xf) + 1;
		    ocp->sysval_frequency /= output_div;
		}

	    } else {
		ocp->sysval_frequency = 12000000;
	    }

	    /*
	     * Readjust the sysval timer to the new frequency.
	     */
	    now = qemu_get_clock(vm_clock);
	    next = now + muldiv64(1, ticks_per_sec, ocp->sysval_frequency);
	    qemu_mod_timer(ocp->sysval_timer, next);
	    return;
	}
    } else if ((addr >= ocp->def->timer_offset) && addr < ocp->def->timer_offset + 0x100) {

	printf("Word write to timer\n");
	if (word_address == ocp->def->timer_offset + 0x4) {
	    /*
	     * Write to timer rtcom register.
	     */
	    if (sub_byte) {
		/*
		 * Write to lower half.
		 */
		ubicom32_timer.rtcom &= ~0xffff;
		ubicom32_timer.rtcom |= val & 0xffff;
	    } else {
		/*
		 * Write to the upper half.
		 */
		ubicom32_timer.rtcom &= ~0xffff0000;
		ubicom32_timer.rtcom |= (val & 0xffff) <<16;
	    }
	    return ;
	}

	if (word_address < ocp->def->timer_offset + 0x18) {
	    /*
	     * Write to watchdog. Currently not implemented.
	     */
	    return;
	}

	/*
	 * Write to a syscom register. Convert the address to an index.
	 */
	word_address -= (ocp->def->timer_offset + 0x18);
	addr >>= 2;

	if (sub_byte) {
	    /*
	     * Write to lower half.
	     */
	    ubicom32_timer.syscom[word_address] &= ~0xffff;
	    ubicom32_timer.syscom[word_address] |= val & 0xffff;
	} else {
	    /*
	     * Write to the upper half.
	     */
	    ubicom32_timer.syscom[word_address] &= ~0xffff0000;
	    ubicom32_timer.syscom[word_address] |= (val & 0xffff) <<16;
	}
	return;
    }
}

static void ubicom32_ocp_qemu_writel (void *opaque, target_phys_addr_t addr,
			      uint32_t val)
{
    struct ocp_info *ocp = opaque;

#ifdef ARCHREFCOMPARE
    int index;
    if (archrefcompare_flag) {

//        printf("ubicom32_ocp_qemu_writel(): addr: %08x, val: %08x\n", addr, val);

        addr &= 0xfff;

        if ((addr >= ocp->def->mmu_offset) && (addr < ocp->def->mmu_offset + 0x200)) {

            index = (addr - ocp->def->mmu_offset) >> 2;
            ocp_mmu[index] = val;

            /* If INSERTW1 is being used, then turn off TLB value checking. */
            if (archrefcompare_flag && (index == INSERTW1) && TLB_value_check_flag) {
                printf("Note: write to INSERTW1 detected; TLB value checking disabled\n");
                TLB_value_check_flag = 0;
            }

        } else
            ocp_memory[addr >> 2] = val;

        return;
    }
#endif

    addr &= 0xfff;

    if ((addr >= 0) && addr < 0x100) {
	if (addr == 0) {
	    uint64_t now, next;
	    /*
	     * Write to PLL config register. Grab it a calculate core clock frequency.
	     */
	    ocp_general[0] = val;

	    /*
	     * If PLL output source is external clock then change the sysval frequency to 12000000 Else compute it.
	     */
	    if (val & 0x10) {
		/*
		 * Compute the PLL frequency.
		 */
		uint32_t nr = GEN_GET_CLK_PLL_NR(val);
		uint32_t nf = GEN_GET_CLK_PLL_NF(val);
		uint32_t od = GEN_GET_CLK_PLL_OD(val);
		ocp->sysval_frequency = ((ocp->mptval_frequency / nr) * nf) / od;

		if ((val & 0xf) != 1) {
		    uint32_t output_div = (val & 0xf) + 1;
		    ocp->sysval_frequency /= output_div;
		}

	    } else {
		ocp->sysval_frequency = 12000000;
	    }

	    /*
	     * Readjust the sysval timer to the new frequency.
	     */
	    now = qemu_get_clock(vm_clock);
	    next = now + muldiv64(1, ticks_per_sec, ocp->sysval_frequency);
	    qemu_mod_timer(ocp->sysval_timer, next);
	    return;
	}

	/*
	 * Write to a general OCP block. Convert to an index and note the value.
	 */
	addr /= 4;
	ocp_general[addr] = val;
	return;
    } else if (addr == (ocp->def->mailbox_offset + 4)) {
	/*
	 * Write to mailbox output queue. Capture the data and send it to qemu.
	 */
	const uint8_t output = val & 0xff;

	(void) qemu_chr_write(ocp->chr, &output, 1);
	return;
    } else if ((addr >= ocp->def->timer_offset) && addr < ocp->def->timer_offset + 0x100) {
	if (addr == ocp->def->timer_offset + 0x4) {
	    /*
	     * Write to timer rtcom register.
	     */
	    ubicom32_timer.rtcom = val;
	    return ;
	}

	if (addr < ocp->def->timer_offset + 0x18) {
	    /*
	     * Write to watchdog. Currently not implemented.
	     */
	    return;
	}

	/*
	 * Write to a syscom register. Convert the address to an index.
	 */
	addr -= (ocp->def->timer_offset + 0x18);
	addr >>= 2;

	ubicom32_timer.syscom[addr] = val;
	return;
    } else if ((addr >= ocp->def->mmu_offset) && addr < ocp->def->mmu_offset + 0x200) {
	uint32_t tnum;
	addr -= ocp->def->mmu_offset;

	/*
	 * Write to MMU block. Convert address into an index.
	 */
	addr >>= 2;

	switch (addr) {
	case CONFIG:
	    ocp_mmu[CONFIG] = val;
	    if (PTEC_ENABLED) {
		ubicom32_mmu.ptec_enabled = 1;
		ubicom32_mmu.extra_asid_bits = (ubicom32_mmu.ptec_entries_log2 > 10) ? (ubicom32_mmu.ptec_entries_log2 - 10) : 0;
		ubicom32_mmu.ptec_index_mask = (1 << ubicom32_mmu.ptec_entries_log2) -1;
	    } else {
		ubicom32_mmu.ptec_enabled = 0;
	    }

	    if (PAGE_MASK == 1) {
		ubicom32_mmu.vpn_bits = 19;
	    } else if (PAGE_MASK == 3) {
		ubicom32_mmu.vpn_bits = 18;
	    } else {
		ubicom32_mmu.vpn_bits = 20;
	    }

	    return;
	    break;
	case PTEC_CFG0:
	    ocp_mmu[addr] = val;
	    ubicom32_mmu.ptec_entries_log2 = PTEC_ENTRIES_LOG2;
	    ubicom32_mmu.rtag_mask = RTAG_MASK;

	    /*
	     * Set the extra_asid_bits since we know the ptec_entries_log2.
	     */
	    ubicom32_mmu.extra_asid_bits = (ubicom32_mmu.ptec_entries_log2 > 10) ? (ubicom32_mmu.ptec_entries_log2 - 10) : 0;
	    ubicom32_mmu.ptec_index_mask = (1 << ubicom32_mmu.ptec_entries_log2) -1;
	    return;
	    break;
	case PTEC_CFG1:
	    ocp_mmu[addr] = val;
	    ubicom32_mmu.atag_base = ATAG_BASE;
	    return;
	    break;
	case PTEC_CFG2:
	    ocp_mmu[addr] = val;
	    ubicom32_mmu.entry_base = ENTRY_BASE;
	    return;
	    break;
	case RESTART:
	    /*
	     * Grab the tnum from the sent value and convert it to a mask.
	     */
	    tnum = 1 << (val & 0x1f);

	    /*
	     * Clear the bit in BUS_ST0
	     */
	    ocp_mmu[BUS_ST0] &= ~tnum;
	    return;
	    break;
	case INSERTW0:
	    //printf("INSERTw0 0x%x\n", val);
	    return;
	    break;
	case INSERTW1:
	    //printf("INSERTw1 0x%x\n", val);
	    return;
	    break;
	case I_PURGE:
	case D_PURGE:
	    {
		/*
		 * Blow out all the tlb entries.
		 */
		uint32_t i;
		for (i = 0; i < ocp->def->num_threads; i++) {
		    tlb_flush(threads[i], 1);
		}
		break;
	    }
	case TLB_IDX:
	    /*
	     * Drop the write on the floor. XXXXXX if we write an extensive TLB model then this will have stuff in it.
	     */
	    break;
	    return;
	case PGD_INDEX(0):
	    ocp_mmu[addr] = val;
	    tlb_flush(threads[0], 1);
	    return;
	    break;
	case PGD_INDEX(1):
	    ocp_mmu[addr] = val;
	    tlb_flush(threads[1], 1);
	    return;
	    break;
	case PGD_INDEX(2):
	    ocp_mmu[addr] = val;
	    tlb_flush(threads[2], 1);
	    return;
	    break;
	case PGD_INDEX(3):
	    ocp_mmu[addr] = val;
	    tlb_flush(threads[3], 1);
	    return;
	    break;
	case PGD_INDEX(4):
	    ocp_mmu[addr] = val;
	    tlb_flush(threads[4], 1);
	    return;
	    break;
	case PGD_INDEX(5):
	    ocp_mmu[addr] = val;
	    tlb_flush(threads[5], 1);
	    return;
	    break;
	case PGD_INDEX(6):
	    ocp_mmu[addr] = val;
	    tlb_flush(threads[6], 1);
	    return;
	    break;
	case PGD_INDEX(7):
	    ocp_mmu[addr] = val;
	    tlb_flush(threads[7], 1);
	    return;
	    break;
	case PGD_INDEX(8):
	    ocp_mmu[addr] = val;
	    tlb_flush(threads[8], 1);
	    return;
	    break;
	case PGD_INDEX(9):
	    ocp_mmu[addr] = val;
	    tlb_flush(threads[9], 1);
	    return;
	    break;
	case PGD_INDEX(10):
	    ocp_mmu[addr] = val;
	    tlb_flush(threads[10], 1);
	    return;
	    break;
	case PGD_INDEX(11):
	    ocp_mmu[addr] = val;
	    tlb_flush(threads[11], 1);
	    return;
	    break;
	default:
	    //printf("Write to 0x%08x val 0x%08x\n", (ocp->def->mmu_offset + (addr << 2)), val);
	    fflush(stdout);
	    ocp_mmu[addr] = val;
	    return;
	}
    }
}

static uint32_t ubicom32_ocp_qemu_readl (void *opaque, target_phys_addr_t addr)
{
    struct ocp_info *ocp = opaque;
    addr &= 0xfff;

#ifdef ARCHREFCOMPARE
    if (archrefcompare_flag) {

//        printf("ubicom32_ocp_qemu_readl(%08x)\n", addr);

        addr&= 0xfff;
        if ((addr >= ocp->def->mmu_offset) && (addr < ocp->def->mmu_offset + 0x200))
            return ocp_mmu[(addr - ocp->def->mmu_offset)>> 2];
        else
            return ocp_memory[addr >> 2];

        return 0xc0edbabe;
    }
    return 0;
#else
    if ((addr >= 0) && addr < 0x100) {
	addr /= 4;
	return ocp_general[addr];
    } else if ((addr >= ocp->def->timer_offset) && addr < ocp->def->timer_offset + 0x100) {
	volatile uint32_t *ptr = &ubicom32_timer.mptval;
	/*
	 * Read from a timer register. Convert the address to an index.
	 */
	addr -= (ocp->def->timer_offset);
	addr >>= 2;
	return ptr[addr];
    } else if ((addr >= ocp->def->mailbox_offset) && addr < ocp->def->mailbox_offset + 0x100) {
	/*
	 * Read from mailbox. 0 is data from the qemu_in_data. 8 is mail stat.
	 */
	addr -= ocp->def->mailbox_offset;

	if (addr == 0) {
	    /*
	     * There better be data to read;
	     */
	    uint32_t mb_data;
	    if(qemu_in_data.tail == qemu_in_data.head)
		return 0;

	    mb_data = qemu_in_data.data[qemu_in_data.tail++];
	    qemu_in_data.tail %= 32;
	    return mb_data;
	} else if (addr == 8) {
	    uint32_t mstat = ISD_MAILBOX_STATUS_OUT_EMPTY;

	    if (qemu_in_data.tail == qemu_in_data.head) {
		mstat |= ISD_MAILBOX_STATUS_IN_EMPTY;
	    }

	    return mstat;
	}
    } else if ((addr >= ocp->def->mmu_offset) && addr < ocp->def->mmu_offset + 0x200) {
	/*
	 * Convert to an index.
	 */
	addr -= ocp->def->mmu_offset;
	addr >>= 2;
	val = ocp_mmu[addr];

	if ((CONFIG <= addr && addr < MISSQW0) || (PTEC_ERR <= addr && addr < 0x80)) {
	    return val;
	}

	if (addr == MISSQW0) {
	    uint32_t tnum = m_t_ix_queue.miss_thread_index[m_t_ix_queue.tail];
	    if (m_t_ix_queue.tail != m_t_ix_queue.head) {
		/*
		 * There is a valid entry. Read the w0 from slot 0
		 */
		return m_queue.m_q_entries[tnum].slot[0].w0;
	    } else {
		return 0;
	    }
	}

	if (addr == MISSQW1) {
	    uint32_t tnum = m_t_ix_queue.miss_thread_index[m_t_ix_queue.tail];
	    if (m_t_ix_queue.tail != m_t_ix_queue.head) {
		/*
		 * There is a valid entry. Read the w1 from slot 0
		 */
		uint32_t w1 = m_queue.m_q_entries[tnum].slot[0].w1;

		/*
		 * Test the multiple bit in W1
		 */
		if (w1 & (1 << 11)) {
		    /*
		     * We have to copy slot 1 to slot 0
		     */
		    m_queue.m_q_entries[tnum].slot[0] = m_queue.m_q_entries[tnum].slot[1];
		} else {
		    /*
		     * Take the entry off the ring buffer.
		     */
		    m_t_ix_queue.tail++;
		    m_t_ix_queue.tail %= 13;

		    if (m_t_ix_queue.tail == m_t_ix_queue.head) {
			/*
			 * The queue is empty lower the interrupt.
			 */
			ubicom32_lower_interrupt(29);
		    }
		}
		return w1;
	    } else {
		return 0;
	    }
	}

	return 0;
    } else if (addr == 0x510 || addr == 0x610) {
	/*
	 * Read of the cache control registers. Return 1.Fake it as if the done bit is set
	 */
	return 1;
    }
    return 0;
#endif
}

static CPUWriteMemoryFunc *ubicom32_ocp_qemu_write[] = {
    &ubicom32_ocp_qemu_writeb,
    &ubicom32_ocp_qemu_writew,
    &ubicom32_ocp_qemu_writel,
};

static CPUReadMemoryFunc *ubicom32_ocp_qemu_read[] = {
    &ubicom32_ocp_qemu_readl,
    &ubicom32_ocp_qemu_readl,
    &ubicom32_ocp_qemu_readl,
};

static int ubicom32_ocp_qemu_iomemtype = 0;
struct ocp_info ubicom32_ocp_info;

static void cpu_ubicom32_irq_request(void *opaque, int irq, int level)
{
    struct ocp_info *ocp = (struct ocp_info *)opaque;
    int int_reg_num = irq / 32;
    int int_bit_num = irq % 32;
    int i;
    int int_mask;
    /*
     * Set the bit in the appropriate int_stat register.
     */
    int_mask = (1 << int_bit_num);

    if (level) {
	ocp->global_regs[1 + int_reg_num] |= int_mask;

	/*
	 * See if interrupts are enabled. If they are not we are done.
	 */
	if ((ocp->global_regs[0x4d - 0x40] & 1) == 0) {
	    return;
	}

	/*
	 * Interrupts are enabled. Wake up an inactive thread that is looking for this
	 * interrupt.
	 */
	for (i = 0; i < ocp->def->num_threads; i++) {
	    /*
	     * If the thread is active continue to the next thread.
	     */
	    int thread_mask = 1 << i;
	    if (ocp->global_regs[0x4e - 0x40] & thread_mask) {
		continue;
	    }

	    /*
	     * If the bit is set in the int mask register wake the process by setting the thread bit in the mt_active register.
	     */
	    if (threads[i]->thread_regs[0x30 + int_reg_num] & int_mask) {
		/*
		 * Set the mt_active bit.
		 */
		ocp->global_regs[0x4e - 0x40] |=  thread_mask;
	    }
	}
    } else {
	/*
	 * Clear the bit.
	 */
	ocp->global_regs[1 + int_reg_num] &= ~int_mask;
    }
}

static void ubicom32_mptval_timer_cb (void *opaque)
{
    struct ocp_info *ocp = opaque;
    uint64_t now, next;

    ubicom32_timer.mptval++;

    /*
     * Restart the timer for another tick.
     */
    now = qemu_get_clock(vm_clock);
    //next = now + muldiv64(1, ticks_per_sec, ubicom32_ocp_info.mptval_frequency);
    next = now+1;
    qemu_mod_timer(ocp->mptval_timer, next);

    if ( ubicom32_timer.mptval == ubicom32_timer.rtcom ) {
	/*
	 * Fire the real time timer interrupt. (bit 32 in int_stat0)
	 */
	qemu_irq_raise(ocp->ubi_irq[30]);
    }
}

static void ubicom32_sysval_timer_cb (void *opaque)
{
    struct ocp_info *ocp = opaque;
    uint64_t now, next;
    uint32_t i;
    //int32_t old_time = (int32_t) ubicom32_timer.sysval;
    //uint32_t new_time = ubicom32_timer.sysval + 1000;

    /*
     * Restart the timer for another tick.
     */
    now = qemu_get_clock(vm_clock);
    //next = now + muldiv64(1, ticks_per_sec, ubicom32_ocp_info.sysval_frequency);
    next = now+1;
    qemu_mod_timer(ocp->sysval_timer, next);

    /*
     * Run through all the syscom registers and see if any of them is going to fire.
     * If it fires then we have to raise the appropriate interrupt.
     */
    for (i = 0; i < ocp->def->num_timers; i++) {
	//if (((int32_t) ubicom32_timer.syscom[i] - old_time ) < 1000) {
	if ( ubicom32_timer.sysval == ubicom32_timer.syscom[i]) {
	    /*
	     * Fire the interrupt.
	     */
	    qemu_irq_raise(ocp->ubi_irq[32+i]);
	}
    }
    ubicom32_timer.sysval++;
}

static int serial_can_receive(void *opaque)
{
    int ring_data = (qemu_in_data.head < qemu_in_data.tail) ? (qemu_in_data.head + 32 - qemu_in_data.tail) : (qemu_in_data.tail - qemu_in_data.head);

    return 31 - ring_data;
}

static void serial_receive(void *opaque, const uint8_t *buf, int size)
{
    struct ocp_info *ocp = opaque;
    int i;

    for (i = 0; i < size; i++) {
	qemu_in_data.data[qemu_in_data.head ++] = buf[i];
	qemu_in_data.head %= 32;
    }

    /*
     * Tell cpu there is data in mailbox by raising the mailbox interrupt (bit 30 in int_stat1).
     */
    qemu_irq_raise(ocp->ubi_irq[32+30]);
}

static void serial_event(void *opaque, int event)
{
  //struct ocm_info *ocm = opaque;
#ifdef DEBUG_SERIAL
    printf("serial: event %x\n", event);
#endif
}

void ubicom32_ocp_init (const struct ubicom32_def_t *def)
{
    uint64_t now, next;
    if (!ubicom32_ocp_qemu_iomemtype) {
        ubicom32_ocp_qemu_iomemtype = cpu_register_io_memory(ubicom32_ocp_qemu_read,
							     ubicom32_ocp_qemu_write, &ubicom32_ocp_info);
    }
    cpu_register_physical_memory(def->ocp_base, def->ocp_size, ubicom32_ocp_qemu_iomemtype);

    ubicom32_ocp_info.def = def;
    ubicom32_ocp_info.mptval_frequency = 12000000;
    ubicom32_ocp_info.sysval_frequency = 12000000;
    ubicom32_ocp_info.global_regs = ubicom32_global_registers;
    ubicom32_ocp_info.ubi_irq = qemu_allocate_irqs(cpu_ubicom32_irq_request, &ubicom32_ocp_info, def->num_interrupts);;

    /*
     * Initialize the ocp_general block.
     */
    ocp_general[0] = 0x80000080;
    ocp_general[1] = 0x0103e080;
    ocp_general[2] = 0x00000080;
    ocp_general[3] = 0x00000080;
    ocp_general[4] = 0x00000000;
    ocp_general[6] = 0x0003000e;
    ocp_general[7] = 0x00000000;
    ocp_general[0x21] = 0x00000001; /* External Reset */

    /*
     * Zero the timer block.
     */
    memset(&ubicom32_timer, 0, sizeof(ubicom32_timer));
    ubicom32_ocp_info.mptval_timer = qemu_new_timer(vm_clock, &ubicom32_mptval_timer_cb, &ubicom32_ocp_info);
    ubicom32_ocp_info.sysval_timer = qemu_new_timer(vm_clock, &ubicom32_sysval_timer_cb, &ubicom32_ocp_info);

    /*
     * Get both the timers running.
     */
    now = qemu_get_clock(vm_clock);
    next = now + muldiv64(1, ticks_per_sec, ubicom32_ocp_info.mptval_frequency);
    qemu_mod_timer(ubicom32_ocp_info.mptval_timer, next);
    qemu_mod_timer(ubicom32_ocp_info.sysval_timer, next);

    /*
     * Hook up QEMU serial_hds[0] to the qemu_in_data.
     */
    memset(&qemu_in_data, 0, sizeof(qemu_in_data));

    ubicom32_ocp_info.chr = serial_hds[0];
    qemu_in_data.head = qemu_in_data.tail = 0;

    qemu_chr_add_handlers(serial_hds[0], serial_can_receive, serial_receive,
                          serial_event, &ubicom32_ocp_info);

    /*
     * Initialize the mmu block.
     */
    memset(&ocp_mmu[0], 0, 0x200);
    memset(&ubicom32_mmu, 0, sizeof(ubicom32_mmu));
    ocp_mmu[0] = 0x0000f003;
    ocp_mmu[1] = 0x00050000;

    ubicom32_mmu.ptec_entries_log2 = PTEC_ENTRIES_LOG2;
    if (PAGE_MASK == 1) {
	ubicom32_mmu.vpn_bits = 19;
    } else {
	ubicom32_mmu.vpn_bits = 18;
    }

    m_t_ix_queue.head = m_t_ix_queue.tail = 0;
}

void irq_info(Monitor *mon)
{
}

void pic_info(Monitor *mon)
{
}

void ubicom32_raise_interrupt(uint32_t int_num)
{
    qemu_irq_raise(ubicom32_ocp_info.ubi_irq[int_num]);
}

void ubicom32_lower_interrupt(uint32_t int_num)
{
    qemu_irq_lower(ubicom32_ocp_info.ubi_irq[int_num]);
}

uint32_t mmu_runnable(CPUState *env)
{
    return ((ocp_mmu[BUS_ST0] & (1 << env->cpu_index)) == 0);
}
