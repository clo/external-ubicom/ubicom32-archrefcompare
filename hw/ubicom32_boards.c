/*
 * QEMU/UBICOM32 pseudo-board
 *
 * The pseudo board has timers.
 * The console io is simulated via the mailbox interface sitting in the OCP block.
 * The 8k has a mmu block while the 7k does not.
 *
 * Copyright (c) 2017, The Linux Foundation. All rights reserved.
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#include "sysemu.h"
#include "boards.h"
#include "qemu-log.h"
#include "cpu.h"

#define ARCHREFCOMPARE

static struct _loaderparams {
    int ram_size;
    const char *kernel_filename;
    const char *kernel_cmdline;
    //const char *initrd_filename;
} loaderparams;

struct _verilog_load {
    FILE * fp;
    uint32_t remaining;
    char *ptr;
    char buffer[4096];
} v_load;

static void read_vlog_data(void)
{
    if (v_load.remaining) {
	/*
	 * There is some unused data in the buffer. Move it to the head
	 * of the buffer.
	 */
	memcpy(v_load.buffer, v_load.ptr, v_load.remaining);

	v_load.ptr = v_load.buffer + v_load.remaining;
    } else {
	v_load.ptr = v_load.buffer;
    }

    /*
     * Read data from file.
     */
    v_load.remaining += fread(v_load.ptr, 1, (4096 - v_load.remaining), v_load.fp);
    v_load.ptr = v_load.buffer;
}

static void load_verilog(const char *verilog_filename)
{
    uint32_t data;
    uint32_t address = 0xffffffff;
    /*
     * Open the verilog file and start consuming the data.
     */
    v_load.fp = fopen(verilog_filename, "rb");
    if (!v_load.fp) {
	printf("Unable top open %s for verilog input. Bye.\n", verilog_filename);
	exit(1);
    }

    /*
     * Read the first 4k of data.
     */
    v_load.ptr = v_load.buffer;
    v_load.remaining = 0;
    read_vlog_data();

    while (v_load.remaining) {
	/*
	 * Scan the buffer looking for @, /, or 0-9, a-f, A-F.
	 */
	if (('0' <= *v_load.ptr &&  *v_load.ptr <= '9') ||
	    ('a' <= *v_load.ptr &&  *v_load.ptr <= 'f') ||
	    ('A' <= *v_load.ptr &&  *v_load.ptr <= 'F')) {
	    /*
	     * Start of a hexadecimal string. There should be at least 8 bytes in the buffer.
	     */
	    if (v_load.remaining < 8) {
		/*
		 * get more data
		 */
		read_vlog_data();
	    }

	    sscanf(v_load.ptr, "%8x", &data);
	    v_load.ptr += 8;
	    v_load.remaining -= 8;

	    if (v_load.remaining == 0) {
		read_vlog_data();

		if (v_load.remaining == 0) {
		    break;
		}
	    }

	    if (address == 0xffffffff) {
		/*
		 * No valid address to put the data into. Ignore the input.
		 */
		printf("No valid address to put data into. Skipping.\n");
		continue;
	    }

	    /*
	     * Store the data to the given address.
	     */
	    stl_phys(address, data);
	    address += 4;
	} else if (*v_load.ptr == '@') {
	    /*
	     * Start of a @ + hexadecimal string. There should be at least 9 bytes in the buffer.
	     */
	    if (v_load.remaining < 9) {
		/*
		 * get more data
		 */
		read_vlog_data();
		if (v_load.remaining == 0) {
		    break;
		}
	    }

	    sscanf(&v_load.ptr[1], "%8x", &address);
	    v_load.ptr += 9;
	    v_load.remaining -= 9;
	    if (v_load.remaining == 0) {
		read_vlog_data();

		if (v_load.remaining == 0) {
		    break;
		}
	    }
	} else if (*v_load.ptr == '/') {
	    int found_new_line = 0;
	    /*
	     * Start of a comment string. There should be at least 2 bytes in the buffer.
	     */
	    if (v_load.remaining < 2) {
		/*
		 * get more data
		 */
		read_vlog_data();
		if (v_load.remaining == 0) {
		    break;
		}
	    }

	    /*
	     * Hunt looking for \n.
	     */
	find_new_line:
	    while (v_load.remaining) {
		v_load.remaining--;
		if (*v_load.ptr++ == '\n') {
		    found_new_line = 1;
		    break;
		}
	    }

	    if (v_load.remaining == 0) {
		read_vlog_data();

		if (v_load.remaining == 0) {
		    break;
		}
		if (found_new_line == 0) {
		    goto find_new_line;
		}
	    }
	} else {
	    v_load.remaining--;
	    v_load.ptr++;
	    if (v_load.remaining == 0) {
		read_vlog_data();

		if (v_load.remaining == 0) {
		    break;
		}
	    }
	}
    }
}


static void load_kernel (CPUState *env)
{
    int64_t entry, kernel_low, kernel_high;
    long kernel_size;
    //long initrd_size;
    //ram_addr_t initrd_offset;

    kernel_size = load_elf(loaderparams.kernel_filename, 0,
                           (uint64_t *)&entry, (uint64_t *)&kernel_low,
                           (uint64_t *)&kernel_high);
    if (kernel_size < 0) {
        fprintf(stderr, "qemu: could not load kernel '%s'\n",
                loaderparams.kernel_filename);
        exit(1);
    }

    /*
     * Ubicom32 will not have a initrd for the time being.
     */
}
static void main_cpu_reset(void *opaque)
{
    CPUState *env = opaque;

    cpu_reset(env);
    env->halted = 0;
}

static void secondary_cpu_reset(void *opaque)
{
    CPUState *env = opaque;

    cpu_reset(env);
    env->halted = 1;
}

static
void ubicom32_common_init (ram_addr_t ram_size,
			 const char *boot_device,
			 const char *kernel_filename, const char *kernel_cmdline,
			 const char *initrd_filename, const char *cpu_model,
			 const struct ubicom32_def_t *def)
{
    CPUState *env = NULL;
    uint32_t i;
    ram_addr_t hrt0_offset;
    ram_addr_t flash_offset;
    ram_addr_t ocm_offset;
    ram_addr_t ddr_offset;
    ram_addr_t io_offset;
    //char *filename;
    int bios_size;
    extern const char *verilog_name;

    for (i=0; i < def->num_threads; i++) {
	env = cpu_init(def->name);
	if (!env) {
	    fprintf(stderr, "qemu: Unable to find Unicom32 CPU definition\n");
	    exit(1);
	}

	threads[i] = env;

	if (i == 0) {
	    qemu_register_reset(main_cpu_reset, env);
	} else {
	    qemu_register_reset(secondary_cpu_reset, env);
	    env->halted = 1;
	}
    }

    /*
     * Allocate various pieces of memory.
     */
    hrt0_offset = qemu_ram_alloc(1024);
    flash_offset = qemu_ram_alloc(def->flash_size);
    ocm_offset = qemu_ram_alloc(def->ocm_size);
#ifdef ARCHREFCOMPARE
    ddr_offset = qemu_ram_alloc(ram_size);
#else
    ddr_offset = qemu_ram_alloc(def->ddr_size);
#endif
    io_offset = qemu_ram_alloc(def->io_size);
    cpu_register_physical_memory(def->hrt0_base, 1024, hrt0_offset | IO_MEM_RAM);
    cpu_register_physical_memory(def->flash_base, def->flash_size, flash_offset | IO_MEM_RAM);
    cpu_register_physical_memory(def->ocm_base, def->ocm_size, ocm_offset | IO_MEM_RAM);
#ifdef ARCHREFCOMPARE
    cpu_register_physical_memory(def->ddr_base, ram_size, ddr_offset | IO_MEM_RAM);
#else
    cpu_register_physical_memory(def->ddr_base, def->ddr_size, ddr_offset | IO_MEM_RAM);
#endif
    cpu_register_physical_memory(def->io_base, def->io_size, io_offset | IO_MEM_RAM);

    /*
     * Get the off chip peripherals allocated and initialized.
     */

    ubicom32_ocp_init(def);

    /* load the BIOS image. */
    //filename = qemu_find_file(QEMU_FILE_TYPE_BIOS, bios_name);
    if (bios_name) {
	bios_size = load_image_targphys(bios_name,def->flash_base,
					def->flash_size);
    } else {
	bios_size = -1;
    }

    /*
     * Load a verilog image it the user has provided it.
     */
    if (verilog_name) {
	load_verilog(verilog_name);
    }

    /*
     * Load kernel if there is onle.
     */
    if (kernel_filename) {
	loaderparams.ram_size = ram_size;
	loaderparams.kernel_filename = kernel_filename;
	loaderparams.kernel_cmdline = kernel_cmdline;
	//loaderparams.initrd_filename = initrd_filename;
	load_kernel(env);
    }
}

static
void ubicom32_ip7k_init (ram_addr_t ram_size,
			 const char *boot_device,
			 const char *kernel_filename, const char *kernel_cmdline,
			 const char *initrd_filename, const char *cpu_model)
{
    const struct ubicom32_def_t *def = &ubicom32_hwdefs[0];
    ubicom32_common_init(ram_size, boot_device, kernel_filename, kernel_cmdline, initrd_filename, cpu_model, def);
}

static
void ubicom32_pseudo8k_init (ram_addr_t ram_size,
			     const char *boot_device,
			     const char *kernel_filename, const char *kernel_cmdline,
			     const char *initrd_filename, const char *cpu_model)
{
    const struct ubicom32_def_t *def = &ubicom32_hwdefs[1];
    ubicom32_common_init(ram_size, boot_device, kernel_filename, kernel_cmdline, initrd_filename, cpu_model, def);
}

static
void ubicom32_ip8k_init (ram_addr_t ram_size,
			 const char *boot_device,
			 const char *kernel_filename, const char *kernel_cmdline,
			 const char *initrd_filename, const char *cpu_model)
{
    const struct ubicom32_def_t *def = &ubicom32_hwdefs[2];
    ubicom32_common_init(ram_size, boot_device, kernel_filename, kernel_cmdline, initrd_filename, cpu_model, def);
}


static QEMUMachine ubicom32_ip7k_machine = {
    .name = "ubicom32_ip7k",
    .desc = "ubicom32 ip7k platform",
    .init = ubicom32_ip7k_init,
    .max_cpus = 12,
    .is_default = 1,
};

static QEMUMachine ubicom32_pseudo8k_machine = {
    .name = "ubicom32_pseudo8k",
    .desc = "ubicom32 pseudo8k platform",
    .init = ubicom32_pseudo8k_init,
    .max_cpus = 12,
};

static QEMUMachine ubicom32_ip8k_machine = {
    .name = "ubicom32_ip8k",
    .desc = "ubicom32 ip8k platform",
    .init = ubicom32_ip8k_init,
    .max_cpus = 12,
};

static void ubicom32_machine_init(void)
{
    qemu_register_machine(&ubicom32_ip7k_machine);
    qemu_register_machine(&ubicom32_pseudo8k_machine);
    qemu_register_machine(&ubicom32_ip8k_machine);
}

machine_init(ubicom32_machine_init);
