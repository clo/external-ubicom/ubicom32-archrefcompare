/*
 *  ubicom32 helper defines
 *
 * Copyright (c) 2017, The Linux Foundation. All rights reserved.
 *
 *  Copyright (c) 2006-2007 CodeSourcery
 *  Written by Paul Brook
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <string.h>

#include "config.h"
#include "cpu.h"
#include "exec-all.h"
#include "qemu-common.h"
#include "gdbstub.h"

#include "helpers.h"

#define ARCHREFCOMPARE

#define SIGNBIT (1u << 31)

struct ubicom32_def_t ubicom32_hwdefs[] = {
  /* IP 7K */
  {
    .name		= "ip7k",
    .chip_id		= 0x0003ffff,
    .hrt0_base		= 0x00000800,
    .hrt1_base		= 0x00000900,
    .nrt_base		= 0x00000a00,
    .flash_base		= 0x60000000,
    .flash_size 	= 0x00800000,
    .ocm_base		= 0x3ffc0000,
    .ocm_size		= 0x00040000,
    .ddr_base		= 0x40000000,
    .ddr_size		= 0x10000000,
    .ocp_base		= 0x01000000,
    .ocp_size		= 0x00001000,
    .io_base		= 0x02000000,
    .io_size		= 0x00010000,
    .ether_base		= 0x02010000,
    .ether_size		= 0x00010000,
    .timer_offset	= 0x00000100,
    .mailbox_offset	= 0x00000300,
    .mmu_offset		= 0x00000000,
    .pll_offset		= 0x00000000,
    .num_threads	= 12,
    .num_timers		= 10,
    .num_interrupts	= 64,
    .has_mmu 		= 0,
    .mapped_base	= 0,
    .mapped_end		= 0,
    .reset_vector	= 0x60000000
  },

  /* IP Pseudo 8K */
  {
    .name		= "ipPseudo8k",
    .chip_id		= 0x0003ffff,
    .hrt0_base		= 0x00000800,
    .hrt1_base		= 0x00000900,
    .nrt_base		= 0x00000a00,
    .flash_base		= 0x60000000,
    .flash_size 	= 0x00800000,
    .ocm_base		= 0x3ffc0000,
    .ocm_size		= 0x00040000,
    .ddr_base		= 0x40000000,
    .ddr_size		= 0x04000000,
    .ocp_base		= 0x01000000,
    .ocp_size		= 0x00001000,
    .io_base		= 0x02000000,
    .io_size		= 0x00010000,
    .timer_offset	= 0x00000100,
    .mailbox_offset	= 0x00000300,
    .mmu_offset		= 0x00000e00,
    .pll_offset		= 0x00000000,
    .num_threads	= 12,
    .num_timers		= 12,
    .num_interrupts	= 96,
    .has_mmu 		= 1,
    .mapped_base	= 0x80000000,
    .mapped_end		= 0xbfffffff,
    .reset_vector	= 0x60000000
  },

  /* IP 8K */
  {
    .name		= "ip8k",
    .chip_id		= 0x0004FFFF,
    .hrt0_base		= 0xb8000800,
    .hrt1_base		= 0xb8000900,
    .nrt_base		= 0xb8000a00,
    .flash_base		= 0xb0000000,
#ifdef ARCHREFCOMPARE
    .flash_size 	= 0x00ffffff,
#else
    .flash_size 	= 0x00800000,
#endif
    .ocm_base		= 0xbffc0000,
    .ocm_size		= 0x00040000,
    .ddr_base		= 0xc0000000,
    .ddr_size		= 0x04000000,
    .ocp_base		= 0xb9000000,
    .ocp_size		= 0x00001000,
    .io_base		= 0xba000000,
    .io_size		= 0x01000000,
    .ether_base		= 0xba0c0000,
    .ether_size		= 0x00010000,
    .timer_offset	= 0x00000100,
    .mailbox_offset	= 0x00000300,
    .mmu_offset		= 0x00000e00,
    .pll_offset		= 0x00000000,
    .num_threads	= 12,
    .num_timers		= 12,
    .num_interrupts	= 96,
    .has_mmu 		= 1,
    .mapped_base	= 0,
    .mapped_end		= 0xafffffff,
    .reset_vector	= 0xb0000000
  },

/* akrontine-IP8K */

/*
** TODO:
**
** Add local OCM configuration
*/
   
  {
    .name		= "akronite-nss",
    .chip_id		= 0x0004FFFF,
    .hrt0_base		= 0xb8000800,
    .hrt1_base		= 0xb8000900,
    .nrt_base		= 0xb8000a00,
    .flash_base		= 0,
    .flash_size 	= 0,
    .ocm_base		= 0xbffc0000,
    .ocm_size		= 0x00040000,
    .ddr_base		= 0xc0000000,
    .ddr_size		= 0x04000000,
    .ocp_base		= 0xb9000000,
    .ocp_size		= 0x00001000,
    .io_base		= 0xba000000,
    .io_size		= 0x01000000,
    .ether_base		= 0xba0c0000,
    .ether_size		= 0x00010000,
    .timer_offset	= 0x00000100,
    .mailbox_offset	= 0x00000300,
    .mmu_offset		= 0,
    .pll_offset		= 0x00000000,
    .num_threads	= 12,
    .num_timers		= 12,
    .num_interrupts	= 96,
    .has_mmu 		= 0,
    .mapped_base	= 0,
    .mapped_end		= 0,
    .reset_vector	= 0xbffc0000
  },
};

const struct ubicom32_def_t *cpu_ubicom32_find_by_name(const char *cpu_model)
{
  unsigned int i;
  
  for (i = 0; ARRAY_SIZE(ubicom32_hwdefs); i++) {
    if(strcmp(ubicom32_hwdefs[i].name, cpu_model) == 0){
      return &ubicom32_hwdefs[i];
    }
  }

  return NULL;
}

void ubicom32_cpu_list(FILE *f, int (*cpu_fprintf)(FILE *f, const char *fmt, ...))
{
    unsigned int i;

    for (i = 0; ARRAY_SIZE(ubicom32_hwdefs); i++) {
        (*cpu_fprintf)(f, "%s\n", ubicom32_hwdefs[i].name);
    }
}

/* TODO: This will need fixing once the MMU is implemented.  */
target_phys_addr_t cpu_get_phys_page_debug(CPUState *env, target_ulong addr)
{
    /*
     * Check the mapped range.
     */
    if(ubicom32_range_check(env->def->mapped_end, env->def->mapped_base, addr)) {
	/*
	 * Mapped space.
	 */
	uint32_t ptec_entry, vpn, asid, tlb_miss_type; 

	tlb_miss_type = tlb_check_no_serror(env, &vpn, &asid, addr, &ptec_entry, 1, 0);

	if (tlb_miss_type != MMU_MISSQW0_TYPE_SERVICED) {
	    return addr;
	}

	ptec_entry &= TARGET_PAGE_MASK;
	ptec_entry |= (addr & (~TARGET_PAGE_MASK));

	return (target_phys_addr_t)(ptec_entry);
    }

    return addr;
}


/*
 * We have to fix this routine when 8k mmu is implemented.
 */
int cpu_ubicom32_handle_mmu_fault (CPUState *env, target_ulong address, int rw,
                               int mmu_idx, int is_softmmu)
{
    int prot;
    uint32_t ptec_entry, vpn, asid, tlb_miss_type; 
    uint32_t is_read, dtlb, is_ifetch;

    if ((env->def->has_mmu == 0) ||
	(ubicom32_range_check(env->def->mapped_end, env->def->mapped_base, address) == 0)) {
	address &= TARGET_PAGE_MASK;
	prot = PAGE_READ | PAGE_WRITE;
	return tlb_set_page(env, address, address, prot, mmu_idx, is_softmmu);
    }

    /*
     * MMU case. Address in mapped region we have to retrieve the ptec entry for this.
     * The ptec entry is guaranteed to be there.
     */
    switch(rw) {
    case 0x2:
	/*
	 * I fetch
	 */
	is_ifetch = 1;
	is_read = 1;
	dtlb = 0;
	break;
    case 0x1:
	/*
	 * Write
	 */
	is_ifetch = 0;
	is_read = 0;
	dtlb = 1;
	break;
    default:
	/*
	 * Read
	 */
	is_ifetch = 0;
	is_read = 1;
	dtlb = 1;
    }

    tlb_miss_type = tlb_check(env, &vpn, &asid, address, &ptec_entry, is_read, is_ifetch);

    if (tlb_miss_type == MMU_MISSQW0_TYPE_SERROR) {
	return tlb_miss_type;
    } else if (tlb_miss_type != MMU_MISSQW0_TYPE_SERVICED) {
	/*
	 * Create a fault in slot 0 and just return the error.
	 */
	create_fault(env->cpu_index, vpn, asid, tlb_miss_type, 0, dtlb);
	return tlb_miss_type;
    }

    /*
     * Everything mapped. The ptec holds the physical page number and it is sitting
     * in the correct place.
     */
#if 0
    printf("Calling tlb_set address 0x%08lx ptec_entry 0x%08lx\n", 
	   (unsigned long)address, (unsigned long)ptec_entry);
    fflush(stdout);
#endif
    address &= TARGET_PAGE_MASK;
    ptec_entry &= TARGET_PAGE_MASK;
    prot = PAGE_READ | PAGE_WRITE;

    return tlb_set_page(env, address, ptec_entry, prot, mmu_idx, is_softmmu);
}

int cpu_gdb_read_extra_register(CPUState *env, uint8_t *mem_buf, int n)
{
  return cpu_gdb_read_register(env, mem_buf, n + 103);
}

int cpu_gdb_write_extra_register(CPUState *env, uint8_t *mem_buf, int n)
{
  return cpu_gdb_write_register(env, mem_buf, n + 103);
}

#include "ubicom32_gdb.c"

#if 0
uint64_t HELPER(macmuls)(CPUState *env, uint32_t op1, uint32_t op2)
{
    int64_t product;
    int64_t res;

    product = (uint64_t)op1 * op2;
    res = (product << 24) >> 24;
    if (res != product) {
        env->macsr |= MACSR_V;
        if (env->macsr & MACSR_OMC) {
            /* Make sure the accumulate operation overflows.  */
            if (product < 0)
                res = ~(1ll << 50);
            else
                res = 1ll << 50;
        }
    }
    return res;
}

uint64_t HELPER(macmulu)(CPUState *env, uint32_t op1, uint32_t op2)
{
    uint64_t product;

    product = (uint64_t)op1 * op2;
    if (product & (0xffffffull << 40)) {
        env->macsr |= MACSR_V;
        if (env->macsr & MACSR_OMC) {
            /* Make sure the accumulate operation overflows.  */
            product = 1ll << 50;
        } else {
            product &= ((1ull << 40) - 1);
        }
    }
    return product;
}

uint64_t HELPER(macmulf)(CPUState *env, uint32_t op1, uint32_t op2)
{
    uint64_t product;
    uint32_t remainder;

    product = (uint64_t)op1 * op2;
    if (env->macsr & MACSR_RT) {
        remainder = product & 0xffffff;
        product >>= 24;
        if (remainder > 0x800000)
            product++;
        else if (remainder == 0x800000)
            product += (product & 1);
    } else {
        product >>= 24;
    }
    return product;
}

void HELPER(macsats)(CPUState *env, uint32_t acc)
{
    int64_t tmp;
    int64_t result;
    tmp = env->macc[acc];
    result = ((tmp << 16) >> 16);
    if (result != tmp) {
        env->macsr |= MACSR_V;
    }
    if (env->macsr & MACSR_V) {
        env->macsr |= MACSR_PAV0 << acc;
        if (env->macsr & MACSR_OMC) {
            /* The result is saturated to 32 bits, despite overflow occuring
               at 48 bits.  Seems weird, but that's what the hardware docs
               say.  */
            result = (result >> 63) ^ 0x7fffffff;
        }
    }
    env->macc[acc] = result;
}

void HELPER(macsatu)(CPUState *env, uint32_t acc)
{
    uint64_t val;

    val = env->macc[acc];
    if (val & (0xffffull << 48)) {
        env->macsr |= MACSR_V;
    }
    if (env->macsr & MACSR_V) {
        env->macsr |= MACSR_PAV0 << acc;
        if (env->macsr & MACSR_OMC) {
            if (val > (1ull << 53))
                val = 0;
            else
                val = (1ull << 48) - 1;
        } else {
            val &= ((1ull << 48) - 1);
        }
    }
    env->macc[acc] = val;
}

void HELPER(macsatf)(CPUState *env, uint32_t acc)
{
    int64_t sum;
    int64_t result;

    sum = env->macc[acc];
    result = (sum << 16) >> 16;
    if (result != sum) {
        env->macsr |= MACSR_V;
    }
    if (env->macsr & MACSR_V) {
        env->macsr |= MACSR_PAV0 << acc;
        if (env->macsr & MACSR_OMC) {
            result = (result >> 63) ^ 0x7fffffffffffll;
        }
    }
    env->macc[acc] = result;
}

void HELPER(mac_set_flags)(CPUState *env, uint32_t acc)
{
    uint64_t val;
    val = env->macc[acc];
    if (val == 0)
        env->macsr |= MACSR_Z;
    else if (val & (1ull << 47));
        env->macsr |= MACSR_N;
    if (env->macsr & (MACSR_PAV0 << acc)) {
        env->macsr |= MACSR_V;
    }
    if (env->macsr & MACSR_FI) {
        val = ((int64_t)val) >> 40;
        if (val != 0 && val != -1)
            env->macsr |= MACSR_EV;
    } else if (env->macsr & MACSR_SU) {
        val = ((int64_t)val) >> 32;
        if (val != 0 && val != -1)
            env->macsr |= MACSR_EV;
    } else {
        if ((val >> 32) != 0)
            env->macsr |= MACSR_EV;
    }
}

void HELPER(flush_flags)(CPUState *env, uint32_t cc_op)
{
    cpu_m68k_flush_flags(env, cc_op);
}

uint32_t HELPER(get_macf)(CPUState *env, uint64_t val)
{
    int rem;
    uint32_t result;

    if (env->macsr & MACSR_SU) {
        /* 16-bit rounding.  */
        rem = val & 0xffffff;
        val = (val >> 24) & 0xffffu;
        if (rem > 0x800000)
            val++;
        else if (rem == 0x800000)
            val += (val & 1);
    } else if (env->macsr & MACSR_RT) {
        /* 32-bit rounding.  */
        rem = val & 0xff;
        val >>= 8;
        if (rem > 0x80)
            val++;
        else if (rem == 0x80)
            val += (val & 1);
    } else {
        /* No rounding.  */
        val >>= 8;
    }
    if (env->macsr & MACSR_OMC) {
        /* Saturate.  */
        if (env->macsr & MACSR_SU) {
            if (val != (uint16_t) val) {
                result = ((val >> 63) ^ 0x7fff) & 0xffff;
            } else {
                result = val & 0xffff;
            }
        } else {
            if (val != (uint32_t)val) {
                result = ((uint32_t)(val >> 63) & 0x7fffffff);
            } else {
                result = (uint32_t)val;
            }
        }
    } else {
        /* No saturation.  */
        if (env->macsr & MACSR_SU) {
            result = val & 0xffff;
        } else {
            result = (uint32_t)val;
        }
    }
    return result;
}

uint32_t HELPER(get_macs)(uint64_t val)
{
    if (val == (int32_t)val) {
        return (int32_t)val;
    } else {
        return (val >> 61) ^ ~SIGNBIT;
    }
}

uint32_t HELPER(get_macu)(uint64_t val)
{
    if ((val >> 32) == 0) {
        return (uint32_t)val;
    } else {
        return 0xffffffffu;
    }
}

uint32_t HELPER(get_mac_extf)(CPUState *env, uint32_t acc)
{
    uint32_t val;
    val = env->macc[acc] & 0x00ff;
    val = (env->macc[acc] >> 32) & 0xff00;
    val |= (env->macc[acc + 1] << 16) & 0x00ff0000;
    val |= (env->macc[acc + 1] >> 16) & 0xff000000;
    return val;
}

uint32_t HELPER(get_mac_exti)(CPUState *env, uint32_t acc)
{
    uint32_t val;
    val = (env->macc[acc] >> 32) & 0xffff;
    val |= (env->macc[acc + 1] >> 16) & 0xffff0000;
    return val;
}

void HELPER(set_mac_extf)(CPUState *env, uint32_t val, uint32_t acc)
{
    int64_t res;
    int32_t tmp;
    res = env->macc[acc] & 0xffffffff00ull;
    tmp = (int16_t)(val & 0xff00);
    res |= ((int64_t)tmp) << 32;
    res |= val & 0xff;
    env->macc[acc] = res;
    res = env->macc[acc + 1] & 0xffffffff00ull;
    tmp = (val & 0xff000000);
    res |= ((int64_t)tmp) << 16;
    res |= (val >> 16) & 0xff;
    env->macc[acc + 1] = res;
}

void HELPER(set_mac_exts)(CPUState *env, uint32_t val, uint32_t acc)
{
    int64_t res;
    int32_t tmp;
    res = (uint32_t)env->macc[acc];
    tmp = (int16_t)val;
    res |= ((int64_t)tmp) << 32;
    env->macc[acc] = res;
    res = (uint32_t)env->macc[acc + 1];
    tmp = val & 0xffff0000;
    res |= (int64_t)tmp << 16;
    env->macc[acc + 1] = res;
}

void HELPER(set_mac_extu)(CPUState *env, uint32_t val, uint32_t acc)
{
    uint64_t res;
    res = (uint32_t)env->macc[acc];
    res |= ((uint64_t)(val & 0xffff)) << 32;
    env->macc[acc] = res;
    res = (uint32_t)env->macc[acc + 1];
    res |= (uint64_t)(val & 0xffff0000) << 16;
    env->macc[acc + 1] = res;
}
#endif
