/*-----------------------------------------------------------------------
 *  ubicom32 gdb register defines
 *
 * Copyright (c) 2017, The Linux Foundation. All rights reserved.
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 * -----------------------------------------------------------------------
 */
struct ubicom32_regs
{
  const char *name;			// Register name
  int  index;			// GDB index
  unsigned int addr;		// Direct space address for register;
};

struct ubicom32_regs ubicom32v4_regs[] = {
  // Mars registers
  /* General data.  */
  { "d0", 0, 0x0 },
  { "d1", 1, 0x4 },
  { "d2", 2, 0x8 },
  { "d3", 3, 0xc },
  { "d4", 4, 0x10 },
  { "d5", 5, 0x14 },
  { "d6", 6, 0x18 },
  { "d7", 7, 0x1c },
  { "d8", 8, 0x20 },
  { "d9", 9, 0x24 },
  { "d10", 10, 0x28 },
  { "d11", 11, 0x2c },
  { "d12", 12, 0x30 },
  { "d13", 13, 0x34 },
  { "d14", 14, 0x38 },
  { "d15", 15, 0x3c },
 /* General address.  */
  { "a0", 16, 0x80 },
  { "a1", 17, 0x84 },
  { "a2", 18, 0x88 },
  { "a3", 19, 0x8c },
  { "a4", 20, 0x90 },
  { "a5", 21, 0x94 },
  { "a6", 22, 0x98 },
  /* Stack pointer.  */
  { "sp", 23, 0x9c },
  /* High 16 bits of multiply accumulator.  */
  { "mac_hi", 24, 0xa0 },
  /* Low 32 bits of multiply accumulator.  */
  { "mac_lo", 25, 0xa4 },
  /* Rounded and clipped S16.15 image of multiply accumulator.  */
  { "mac_rc16", 26, 0xa8 },
  /* 3rd source operand.  */
  { "source_3", 27,0xac },
  /* Current thread's execution count. */
  { "context_cnt", 28, 0xb0 },
  /* Control/status.  */
  { "csr", 29, 0xb4 },
  /* Read-only status.  */
  { "rosr", 30, 0xb8 },
  /* Iread output.  */
  { "iread_data", 31, 0xbc },
  /* Low 32 bits of interrupt mask.  */
  { "int_mask0", 32, 0xc0 },
  /* High 32 bits of interrupt mask.  */
  { "int_mask1", 33, 0xc4 },
  /* Program counter.  */
  { "pc", 34, 0xd0 },
  /* Chip identification.  */

  { "trap_cause", 35, 0xd4 },
  { "acc1_hi", 36, 0xd8 },
  { "acc1_lo", 37, 0xdc },
  { "previous_pc", 38, 0xe0 },

  /* 0xd0 - 0xff  Reserved */
  /* Chip identification.  */
  { "chip_id", 39, 0x100 },
  /* Low 32 bits of interrupt status.  */
  { "int_stat0", 40, 0x104 },
  /* High 32 bits of interrupt status.  */
  { "int_stat1", 41, 0x108 },

  /* 0x10c - 0x113 Reserved */
  /* Set bits in int_stat0.  */
  { "int_set0", 42, 0x114 },
  /* Set bits in int_stat1.  */
  { "int_set1", 43, 0x118 },

  /* 0x11c - 0x123 Reserved */
  /* Clear bits in int_stat0.  */
  { "int_clr0", 44, 0x124 },
  /* Clear bits in int_stat1.  */
  { "int_clr1", 45, 0x128 },
  
  /* 0x12c - 0x133 Reserved */
  /* Global control.  */
  { "global_ctrl", 46, 0x134 },
  /* Threads active status.  */
  { "mt_active", 47, 0x138 },
  /* Set bits in mt_active.  */
  { "mt_active_set", 48, 0x13c },
  /* Clear bits in mt_active.  */
  { "mt_active_clr", 49, 0x140 },
  /* Debugging threads active status.  */
  { "mt_dbg_active", 50, 0x144 },
  /* Set bits in mt_dbg_active.  */
  { "mt_dbg_active_set", 51, 0x148 },
  /* Threads enabled.  */
  { "mt_en", 52, 0x14C },
  /* Thread priorities.  */
  { "mt_pri", 53, 0x150 },
  /* Thread scheduling policies.  */
  { "mt_sched", 54, 0x154 },
  /* Threads stopped on a break condition.  */
  { "mt_break", 55, 0x158 },
  /* Clear bits in mt_break.  */
  { "mt_break_clr", 56, 0x15C },
  /* Single-step threads.  */
  { "mt_single_step", 57, 0x160 },
  /* Threads with minimum delay scheduling constraing.  */
  { "mt_min_del_en", 58, 0x164 },
  { "mt_break_set", 59, 0x168 },

  /* 0x16c - 0x16f reserved */
  /* Data capture address.  */
  { "dcapt", 60, 0x170 },

  /* 0x174 - 0x17b reserved */
  /* Debugging threads active status clear register.  */
  { "mt_dbg_active_clr", 61, 0x17c },
  /* scratchpad registers */
  { "scratchpad0", 62, 0x180 },
  { "scratchpad1", 63, 0x184 },
  { "scratchpad2", 64, 0x188 },
  { "scratchpad3", 65, 0x18c },

  /* 0x190 - 0x19f Reserved */
  { "chip_cfg", 66, 0x1a0 },
  { "mt_i_blocked", 67, 0x1a4 },
  { "mt_d_blocked", 68, 0x1a8 },
  { "mt_i_blocked_set", 69, 0x1ac },
  { "mt_d_blocked_set", 70, 0x1b0 },
  { "mt_blocked_clr", 71, 0x1b4 },
  { "mt_trap_en", 72, 0x1b8 },
  { "mt_trap", 73, 0x1bc },
  { "mt_trap_set", 74, 0x1c0 },
  { "mt_trap_clr", 75, 0x1c4 },

  /* 0x1c8-0x1FF Reserved */
  { "i_range0_hi", 76, 0x200 },
  { "i_range1_hi", 77, 0x204 },
  { "i_range2_hi", 78, 0x208 },
  { "i_range3_hi", 79, 0x20c },

  /* 0x210-0x21f Reserved */
  { "i_range0_lo", 80, 0x220 },
  { "i_range1_lo", 81, 0x224 },
  { "i_range2_lo", 82, 0x228 },
  { "i_range3_lo", 83, 0x22c },

  /* 0x230-0x23f Reserved */
  { "i_range0_en", 84, 0x240 },
  { "i_range1_en", 85, 0x244 },
  { "i_range2_en", 86, 0x248 },
  { "i_range3_en", 87, 0x24c },
  
  /* 0x250-0x25f Reserved */
  { "d_range0_hi", 88, 0x260 },
  { "d_range1_hi", 89, 0x264 },
  { "d_range2_hi", 90, 0x268 },
  { "d_range3_hi", 91, 0x26c },
  { "d_range4_hi", 92, 0x270 },

  /* 0x274-0x27f Reserved */
  { "d_range0_lo", 93, 0x280 },
  { "d_range1_lo", 94, 0x284 },
  { "d_range2_lo", 95, 0x288 },
  { "d_range3_lo", 96, 0x28c },
  { "d_range4_lo", 97, 0x290 },
  
  /* 0x294-0x29f Reserved */
  { "d_range0_en", 98, 0x2a0 },
  { "d_range1_en", 99, 0x2a4 },
  { "d_range2_en", 100, 0x2a8 },
  { "d_range3_en", 101, 0x2ac },
  { "d_range4_en", 102, 0x2b0 },
};

struct ubicom32_regs ubicom32v5_regs[] = {
  /* General data.  */
  { "d0", 0, 0x0 },
  { "d1", 1, 0x4 },
  { "d2", 2, 0x8 },
  { "d3", 3, 0xc },
  { "d4", 4, 0x10 },
  { "d5", 5, 0x14 },
  { "d6", 6, 0x18 },
  { "d7", 7, 0x1c },
  { "d8", 8, 0x20 },
  { "d9", 9, 0x24 },
  { "d10", 10, 0x28 },
  { "d11", 11, 0x2c },
  { "d12", 12, 0x30 },
  { "d13", 13, 0x34 },
  { "d14", 14, 0x38 },
  { "d15", 15, 0x3c },
 /* General address.  */
  { "a0", 16, 0x80 },
  { "a1", 17, 0x84 },
  { "a2", 18, 0x88 },
  { "a3", 19, 0x8c },
  { "a4", 20, 0x90 },
  { "a5", 21, 0x94 },
  { "a6", 22, 0x98 },
  /* Stack pointer.  */
  { "sp", 23, 0x9c },
  /* High 16 bits of multiply accumulator.  */
  { "mac_hi", 24, 0xa0 },
  /* Low 32 bits of multiply accumulator.  */
  { "mac_lo", 25, 0xa4 },
  /* Rounded and clipped S16.15 image of multiply accumulator.  */
  { "mac_rc16", 26, 0xa8 },
  /* 3rd source operand.  */
  { "source_3", 27,0xac },
  /* Current thread's execution count. */
  { "context_cnt", 28, 0xb0 },
  /* Control/status.  */
  { "csr", 29, 0xb4 },
  /* Read-only status.  */
  { "rosr", 30, 0xb8 },
  /* Iread output.  */
  { "iread_data", 31, 0xbc },
  /* Low 32 bits of interrupt mask.  */
  { "int_mask0", 32, 0xc0 },
  /* High 32 bits of interrupt mask.  */
  { "int_mask1", 33, 0xc4 },
  { "int_mask2", 34, 0xc8 },

  /* 0xc8 - 0xcf Reserved */
  /* Program counter.  */
  { "pc", 35, 0xd0 },
  { "trap_cause", 36, 0xd4 },
  { "acc1_hi", 37, 0xd8 },
  { "acc1_lo", 38, 0xdc },
  { "previous_pc", 39, 0xe0 },
  { "ucsr", 40, 0xe4 },

  /* 0xd0 - 0xff  Reserved */
  /* Chip identification.  */
  { "chip_id", 41, 0x100 },
  /* Low 32 bits of interrupt status.  */
  { "int_stat0", 42, 0x104 },
  /* High 32 bits of interrupt status.  */
  { "int_stat1", 43, 0x108 },
  { "int_stat2", 44, 0x10c },

  /* 0x110 - 0x113 Reserved */
  /* Set bits in int_stat0.  */
  { "int_set0", 45, 0x114 },
  /* Set bits in int_stat1.  */
  { "int_set1", 46, 0x118 },
  { "int_set2", 47, 0x11c },

  /* 0x120 - 0x123 Reserved */
  /* Clear bits in int_stat0.  */
  { "int_clr0", 48, 0x124 },
  /* Clear bits in int_stat1.  */
  { "int_clr1", 49, 0x128 },
  { "int_clr2", 50, 0x12c },

  /* 0x130 - 0x133 Reserved */
  /* Global control.  */
  { "global_ctrl", 51, 0x134 },
  /* Threads active status.  */
  { "mt_active", 52, 0x138 },
  /* Set bits in mt_active.  */
  { "mt_active_set", 53, 0x13c },
  /* Clear bits in mt_active.  */
  { "mt_active_clr", 54, 0x140 },
  /* Debugging threads active status.  */
  { "mt_dbg_active", 55, 0x144 },
  /* Set bits in mt_dbg_active.  */
  { "mt_dbg_active_set", 56, 0x148 },
  /* Threads enabled.  */
  { "mt_en", 57, 0x14C },
  /* Thread priorities.  */
  { "mt_pri", 58, 0x150 },
  /* Thread scheduling policies.  */
  { "mt_sched", 59, 0x154 },
  /* Threads stopped on a break condition.  */
  { "mt_break", 60, 0x158 },
  /* Clear bits in mt_break.  */
  { "mt_break_clr", 61, 0x15C },
  /* Single-step threads.  */
  { "mt_single_step", 62, 0x160 },
  /* Threads with minimum delay scheduling constraing.  */
  { "mt_min_del_en", 63, 0x164 },
  { "mt_break_set", 64, 0x168 },

  /* 0x16c mt_fp_flush is at the end of this structure.  */
  /* Data capture address.  */
  { "dcapt", 65, 0x170 },

  /* 0x174 - 0x17b mt_fp_blocked and mt_fp_blocked_set are at the end of this structure. */
  /* Debugging threads active status clear register.  */
  { "mt_dbg_active_clr", 66, 0x17c },
  /* scratchpad registers */
  { "scratchpad0", 67, 0x180 },
  { "scratchpad1", 68, 0x184 },
  { "scratchpad2", 69, 0x188 },
  { "scratchpad3", 70, 0x18c },
  { "scratchpad4", 71, 0x190 },
  { "scratchpad5", 72, 0x194 },
  { "scratchpad6", 73, 0x198 },

  /* 0x19c - 0x19f Reserved */
  { "chip_cfg", 74, 0x1a0 },
  { "mt_i_blocked", 75, 0x1a4 },
  { "mt_d_blocked", 76, 0x1a8 },
  { "mt_i_blocked_set", 77, 0x1ac },
  { "mt_d_blocked_set", 78, 0x1b0 },
  { "mt_blocked_clr", 79, 0x1b4 },
  { "mt_trap_en", 80, 0x1b8 },
  { "mt_trap", 81, 0x1bc },
  { "mt_trap_set", 82, 0x1c0 },
  { "mt_trap_clr", 83, 0x1c4 },
  { "sep", 84, 0x1c8 },

  /* 0x1cc-0x1FF Reserved */
  { "i_range0_hi", 85, 0x200 },
  { "i_range1_hi", 86, 0x204 },
  { "i_range2_hi", 87, 0x208 },
  { "i_range3_hi", 88, 0x20c },

  /* 0x210-0x21f Reserved */
  { "i_range0_lo", 89, 0x220 },
  { "i_range1_lo", 90, 0x224 },
  { "i_range2_lo", 91, 0x228 },
  { "i_range3_lo", 92, 0x22c },

  /* 0x230-0x23f Reserved */
  { "i_range0_en", 93, 0x240 },
  { "i_range1_en", 94, 0x244 },
  { "i_range2_en", 95, 0x248 },
  { "i_range3_en", 96, 0x24c },

  /* 0x250-0x25f Reserved */
  { "d_range0_hi", 97, 0x260 },
  { "d_range1_hi", 98, 0x264 },
  { "d_range2_hi", 99, 0x268 },
  { "d_range3_hi", 100, 0x26c },
  { "d_range4_hi", 101, 0x270 },
  { "d_range5_hi", 102, 0x274 },

  /* 0x278-0x27f Reserved */
  { "d_range0_lo", 103, 0x280 },
  { "d_range1_lo", 104, 0x284 },
  { "d_range2_lo", 105, 0x288 },
  { "d_range3_lo", 106, 0x28c },
  { "d_range4_lo", 107, 0x290 },
  { "d_range5_lo", 108, 0x294 },

  /* 0x298-0x29f Reserved */
  { "d_range0_en", 109, 0x2a0 },
  { "d_range1_en", 110, 0x2a4 },
  { "d_range2_en", 111, 0x2a8 },
  { "d_range3_en", 112, 0x2ac },
  { "d_range4_en", 113, 0x2b0 },
  { "d_range5_en", 114, 0x2b4 },

  /* 0x2b8 - 0x2bf Reserved */
  { "i_range0_user_en", 115, 0x2c0 },
  { "i_range1_user_en", 116, 0x2c4 },
  { "i_range2_user_en", 117, 0x2c8 },
  { "i_range3_user_en", 118, 0x2cc },

  /* 0x2d0-0x2df Reserved */
  { "d_range0_user_en", 119, 0x2e0 },
  { "d_range1_user_en", 120, 0x2e4 },
  { "d_range2_user_en", 121, 0x2e8 },
  { "d_range3_user_en", 122, 0x2ec },
  { "d_range4_user_en", 123, 0x2f0 },
  { "d_range5_user_en", 124, 0x2f4 },

  /* fp registers. */
  { "mt_fp_flush", 125, 0x16c },
  { "mt_fp_blocked", 126, 0x174 },
  { "mt_fp_blocked_set", 127, 0x178 },
};

struct ubicom32_regs *u32_regs[] = {
  ubicom32v4_regs,
  ubicom32v5_regs,
};

unsigned int ubicom32_gdb_index_to_address(CPUState *env, int n)
{
    /*
     * Given a gdb index this will return the ubicom32 direct address for that register.
     */
    struct ubicom32_regs *u_regs;
    if(env->def->chip_id == UBICOM32_7K_CHIP_ID) {
	u_regs = u32_regs[0];
    } else {
	u_regs = u32_regs[1];
    }

    return (u_regs[n].addr);
}
