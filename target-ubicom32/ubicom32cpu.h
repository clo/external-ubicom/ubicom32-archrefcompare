/*-----------------------------------------------------------------------
 * ubicom32cpu.h
 *
 * Copyright (c) 2017, The Linux Foundation. All rights reserved.
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 * -----------------------------------------------------------------------
 */

#ifndef OPCODE_H
#define OPCODE_H

/*
** Number of hardware threads
*/

#define HW_THREADS_NUM	12

/*
** General defines
*/

#define UINT48_MAX      0xffffffffffffLL

#define BIT_0		1
#define BIT_1		(1 << 1)
#define BIT_2		(1 << 2)
#define BIT_3		(1 << 3)
#define BIT_4		(1 << 4)
#define BIT_5		(1 << 5)
#define BIT_6		(1 << 6)
#define BIT_7		(1 << 7)
#define BIT_8		(1 << 8)
#define BIT_9		(1 << 9)
#define BIT_10		(1 << 10)
#define BIT_11		(1 << 11)
#define BIT_12		(1 << 12)
#define BIT_13		(1 << 13)
#define BIT_14		(1 << 14)
#define BIT_15		(1 << 15)
#define BIT_16		(1 << 16)
#define BIT_17		(1 << 17)
#define BIT_18		(1 << 18)
#define BIT_19		(1 << 19)
#define BIT_20		(1 << 20)
#define BIT_21		(1 << 21)
#define BIT_22		(1 << 22)
#define BIT_23		(1 << 23)
#define BIT_24		(1 << 24)
#define BIT_25		(1 << 25)
#define BIT_26		(1 << 26)
#define BIT_27		(1 << 27)
#define BIT_28		(1 << 28)
#define BIT_29		(1 << 29)
#define BIT_30		(1 << 30)
#define BIT_31		(1 << 31)

/*
** Instruction encoding defines
*/

#define DSP_C_BIT	BIT_20
#define DSP_T_BIT	BIT_19
#define DSP_S_BIT	BIT_18
#define DSP_O_BIT	BIT_17
#define DSP_A_BIT	BIT_16

/*
** CSR bits
*/

#define CSR_O_BIT	BIT_20

/* Ubicom32 FPU-related UCSR bits */

#define UCSR_FPU_ALL_BITS               (((1 << 5) - 1) << 15)
#define UCSR_FPU_INEXACT_BIT		BIT_15
#define UCSR_FPU_UNDERFLOW_BIT		BIT_16
#define UCSR_FPU_OVERFLOW_BIT		BIT_17
#define UCSR_FPU_DIVIDE_BY_ZERO_BIT	BIT_18
#define UCSR_FPU_INVALID_BIT            (1 << 19)

/* Macros to decode 11 bit source1/dest encoding */

#define SOURCE11_IS_IMMED(x)			((x >> 8) == 0)
#define SOURCE11_IS_DIRECT_REGISTER(x)		((x >> 8) == 1)
#define SOURCE11_IS_MEMORY(x)			(!SOURCE11_IS_IMMED(x) && !IS_REGISTER(x))
#define SOURCE11_REGISTER_NUM(x)		(x & 0xff)

/* CPU registers */

/* Obsolete names for some registers */
#define REGISTER_MAC_HI REGISTER_ACC0_HI
#define REGISTER_MAC_LO REGISTER_ACC0_LO

typedef enum {

	REGISTER_D0		= 0,
	REGISTER_D1,
	REGISTER_D2,
	REGISTER_D3,
	REGISTER_D4,
	REGISTER_D5,
	REGISTER_D6,
	REGISTER_D7,
	REGISTER_D8,
	REGISTER_D9,
	REGISTER_D10,
	REGISTER_D11,
	REGISTER_D12,
	REGISTER_D13,
	REGISTER_D14,
	REGISTER_D15,

	REGISTER_A0		= 0x20,
	REGISTER_A1,
	REGISTER_A2,
	REGISTER_A3,
	REGISTER_A4,
	REGISTER_A5,
	REGISTER_A6,
	REGISTER_A7,

	REGISTER_ACC0_HI,
	REGISTER_ACC0_LO,
	REGISTER_MAC_RC16,
	REGISTER_SOURCE3,
	REGISTER_INST_CNT,
	REGISTER_CSR,
	REGISTER_ROSR,
	REGISTER_IREAD_DATA,
	REGISTER_INT_MASK0,
	REGISTER_INT_MASK1,
	REGISTER_INT_MASK2,

	REGISTER_PC		= 0x34,
	REGISTER_TRAP_CAUSE,
	REGISTER_ACC1_HI,
	REGISTER_ACC1_LO,
	REGISTER_PREVIOUS_PC	= 0x38,
	REGISTER_UCSR,

	/* Global registers */

	REGISTER_CHIP_ID	= 0x40,
	REGISTER_INT_STAT0,
	REGISTER_INT_STAT1,
	REGISTER_INT_STAT2,

	REGISTER_INT_SET0	= 0x45,
	REGISTER_INT_SET1,
	REGISTER_INT_SET2,

	REGISTER_INT_CLR0	= 0x49,
	REGISTER_INT_CLR1,
	REGISTER_INT_CLR2,

	REGISTER_GLOBAL_CTRL	= 0x4d,
	REGISTER_MT_ACTIVE,
	REGISTER_MT_ACTIVE_SET,
	REGISTER_MT_ACTIVE_CLR,
	REGISTER_MT_DBG_ACTIVE,
	REGISTER_MT_DBG_ACTIVE_SET,
	REGISTER_MT_EN,
	REGISTER_MT_HPRI,
	REGISTER_MT_HRT,
	REGISTER_MT_BREAK,
	REGISTER_MT_BREAK_CLR,
	REGISTER_MT_SINGLE_STEP,
	REGISTER_MT_MIN_DELAY_EN,
	REGISTER_MT_BREAK_SET,
	REGISTER_PERR_ADDR,
	REGISTER_DCAPT,

	REGISTER_MT_DBG_ACTIVE_CLR	= 0x5f,
	REGISTER_SCRATCHPAD0,
	REGISTER_SCRATCHPAD1,
	REGISTER_SCRATCHPAD2,
	REGISTER_SCRATCHPAD3,
	REGISTER_SCRATCHPAD4,
	REGISTER_SCRATCHPAD5,

	REGISTER_CHIP_CFG		= 0x68,
	REGISTER_MT_I_BLOCKED,
	REGISTER_MT_D_BLOCKED,
	REGISTER_MT_I_BLOCKED_SET,
	REGISTER_MT_D_BLOCKED_SET,
	REGISTER_MT_BLOCKED_CLR,
	REGISTER_MT_TRAP_EN,
	REGISTER_MT_TRAP,

	REGISTER_MT_TRAP_SET		= 0x70,
	REGISTER_MT_TRAP_CLR,
	REGISTER_SEP,
	REGISTER_MT_BTB_EN,
	REGISTER_BTB_ENTRIES,
	REGISTER_TNUM,

	REGISTER_I_RANGE0_HI		= 0x80,
	REGISTER_I_RANGE1_HI,
	REGISTER_I_RANGE2_HI,
	REGISTER_I_RANGE3_HI,

	REGISTER_I_RANGE0_LO		= 0x88,
	REGISTER_I_RANGE1_LO,
	REGISTER_I_RANGE2_LO,
	REGISTER_I_RANGE3_LO,

	REGISTER_I_RANGE0_EN		= 0x90,
	REGISTER_I_RANGE1_EN,
	REGISTER_I_RANGE2_EN,
	REGISTER_I_RANGE3_EN,

	REGISTER_D_RANGE0_HI		= 0x98,
	REGISTER_D_RANGE1_HI,
	REGISTER_D_RANGE2_HI,
	REGISTER_D_RANGE3_HI,
	REGISTER_D_RANGE4_HI,
	REGISTER_D_RANGE5_HI,

	REGISTER_D_RANGE0_LO		= 0xa0,
	REGISTER_D_RANGE1_LO,
	REGISTER_D_RANGE2_LO,
	REGISTER_D_RANGE3_LO,
	REGISTER_D_RANGE4_LO,

	REGISTER_D_RANGE0_EN		= 0xa8,
	REGISTER_D_RANGE1_EN,
	REGISTER_D_RANGE2_EN,
	REGISTER_D_RANGE3_EN,
	REGISTER_D_RANGE4_EN,

	REGISTER_I_RANGE0_USER_EN	= 0xb0,
	REGISTER_I_RANGE1_USER_EN,
	REGISTER_I_RANGE2_USER_EN,
	REGISTER_I_RANGE3_USER_EN,

	REGISTER_D_RANGE0_USER_EN	= 0xb8,
	REGISTER_D_RANGE1_USER_EN,
	REGISTER_D_RANGE2_USER_EN,
	REGISTER_D_RANGE3_USER_EN,
	REGISTER_D_RANGE4_USER_EN,
	REGISTER_D_RANGE5_USER_EN

} UBICOM32_REGISTER;

/* Instruction formats */

typedef enum {

	INSN_FORMAT_1A,
	INSN_FORMAT_1B,
	INSN_FORMAT_1C,
	INSN_FORMAT_1D,
	INSN_FORMAT_2,
	INSN_FORMAT_3,
	INSN_FORMAT_4A,
	INSN_FORMAT_4B,
	INSN_FORMAT_5,
	INSN_FORMAT_6,
	INSN_FORMAT_7,
	INSN_FORMAT_8,
	INSN_FORMAT_9,
	INSN_FORMAT_10A,
	INSN_FORMAT_10B,
	INSN_FORMAT_11A,
	INSN_FORMAT_11B,
	INSN_FORMAT_11C,
	INSN_FORMAT_11D,
	INSN_FORMAT_12A,
	INSN_FORMAT_12B,
	INSN_FORMAT_12C,
	INSN_FORMAT_12D

} INSN_FORMAT;

#endif


