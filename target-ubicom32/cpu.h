/*
 * ubicom32 virtual CPU header
 *
 * Copyright (c) 2017, The Linux Foundation. All rights reserved.
 *
 *  Copyright (c) 2005-2007 CodeSourcery
 *  Written by Paul Brook
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CPU_UBICOM32_H
#define CPU_UBICOM32_H

#define TARGET_LONG_BITS 32

#define CPUState struct CPUUBICOM32State

#include "cpu-defs.h"
#include "softfloat.h"

#define TARGET_HAS_ICE 1
#define UBICOM32_7K_CHIP_ID 0x0003ffff
#define UBICOM32_8K_CHIP_ID 0x0004ffff

#define GLOBAL_REG(x) ((x) - 0x40)

/*
 * TRAP_CAUSE register
 */
#define TRAP_CAUSE_TOTAL		14
#define TRAP_CAUSE_PRIVILEGE		13
#define TRAP_CAUSE_DST_RANGE_ERR	12
#define TRAP_CAUSE_SRC1_RANGE_ERR	11
#define TRAP_CAUSE_I_RANGE_ERR		10
#define TRAP_CAUSE_DCAPT		9
#define TRAP_CAUSE_DST_SERROR		8
#define TRAP_CAUSE_SRC1_SERROR		7
#define TRAP_CAUSE_DST_MISALIGNED	6
#define TRAP_CAUSE_SRC1_MISALIGNED	5
#define TRAP_CAUSE_DST_DECODE_ERR	4
#define TRAP_CAUSE_SRC1_DECODE_ERR	3
#define TRAP_CAUSE_ILLEGAL_INST		2
#define TRAP_CAUSE_I_SERROR		1
#define TRAP_CAUSE_I_DECODE_ERR		0

/*
 * Cpu description structure will contain information describing the cpu.
 * Start addresses of the various blocks of memory OCM/DDR etc.
 * It will contain chip_id, number of thread, number of timers etc.
 */
struct ubicom32_def_t {
    const char *name;
    uint32_t chip_id;
    uint32_t hrt0_base;
    uint32_t hrt1_base;
    uint32_t nrt_base;
    uint32_t flash_base;
    uint32_t flash_size;
    uint32_t ocm_base;
    uint32_t ocm_size;
    uint32_t ddr_base;
    uint32_t ddr_size;
    uint32_t ocp_base;
    uint32_t ocp_size;
    uint32_t io_base;
    uint32_t io_size;
    uint32_t ether_base;
    uint32_t ether_size;
    uint32_t timer_offset;
    uint32_t mailbox_offset;
    uint32_t mmu_offset;
    uint32_t pll_offset;
    uint32_t num_threads;
    uint32_t num_timers;
    uint32_t num_interrupts;
    uint32_t has_mmu;
    uint32_t mapped_base;
    uint32_t mapped_end;
    uint32_t reset_vector;
};

extern struct ubicom32_def_t ubicom32_hwdefs[];
extern uint32_t init_pc;

#define NUM_HWDEFS (sizeof(ubicom32_hwdefs) / sizeof(struct ubicom32_def_t))

#define EM_UBICOM32 	        0xde3d	/* Ubicom32; no ABI */
#define ELF_MACHINE	EM_UBICOM32
#define NUM_CONTEXTS 16
#define NB_MMU_MODES 2

typedef struct CPUUBICOM32State {
    uint32_t thread_regs[64];
    uint32_t thread_number;	/* Thread ID . */
    uint32_t pc;
    uint32_t dest_data;
    uint32_t src1_data;
    uint32_t src2_data;
    uint32_t dest_address;	/* Destination address. Load it and this will be used by to helper to do alignment and destination checks. */
    uint32_t src1_address;	/* Src1 address. Needs to be loaded and checked by the helper for alignment and range checks. */
    uint32_t bset_clr_tmp;	/* bset, bclr temporary. Holds the mask. */
    uint32_t postinc_areg;
    uint32_t increment;
    uint32_t current_pc;
    uint64_t src1_64;
    uint64_t src2_64;
    uint64_t temp_64;
    uint64_t dest_64;		/* 64 bit entries needed for DSP .4 computations. */
    uint32_t cc_sr_index;		/* For jupiter this is 0xe4/4 for Ares this is 0xb4/4 */
    CPU_COMMON
    const struct ubicom32_def_t *def;	/* Pointer to the structure that contain cpu address layout. */
    uint32_t *global_regs;	/* Pointer to global registers */
    uint32_t features;
} CPUUBICOM32State;

extern uint32_t ubicom32_global_registers[192];

enum {
    // condition codes for jmpcc instruction
    CC_F,
    CC_NC,
    CC_C,
    CC_Z,
    CC_GE,
    CC_GT,
    CC_HI,
    CC_LE,
    CC_LS,
    CC_LT,
    CC_MI,
    CC_NZ,
    CC_PL,
    CC_T,
    CC_NV,
    CC_V,
};

void ubicom32_tcg_init(void);
CPUUBICOM32State *cpu_ubicom32_init(const char *cpu_model);
int cpu_ubicom32_exec(CPUUBICOM32State *s);
void cpu_ubicom32_close(CPUUBICOM32State *s);
void do_interrupt(int is_hw);
const struct ubicom32_def_t *cpu_ubicom32_find_by_name(const char *cpu_model);
void ubicom32_ocp_init (const struct ubicom32_def_t *def);
void ubicom32_raise_interrupt(uint32_t int_num);
void ubicom32_lower_interrupt(uint32_t int_num);
uint32_t mmu_runnable(CPUState *env);
void generate_trap(CPUState *env, uint32_t thread_mask, uint32_t trap_cause);
unsigned int ubicom32_gdb_index_to_address(CPUState *env, int n);
void inst_mapping_check(CPUState *env, uint32_t i_vaddr);
void mapping_check(CPUState *env, uint32_t s1_vaddr, uint32_t d_vaddr, uint32_t check);
void create_fault(uint32_t tnum, uint32_t vpn, uint32_t asid, uint32_t type, uint32_t slot, uint32_t dtlb);
uint32_t tlb_check(CPUState *env, uint32_t *vpn, uint32_t *asid, uint32_t vaddr, uint32_t *return_ptec_entry, uint32_t is_read, uint32_t is_ifetch);
uint32_t tlb_check_no_serror(CPUState *env, uint32_t *vpn, uint32_t *asid, uint32_t vaddr, uint32_t *return_ptec_entry, uint32_t is_read, uint32_t is_ifetch);
int cpu_gdb_read_extra_register(CPUState *env, uint8_t *mem_buf, int n);
int cpu_gdb_write_extra_register(CPUState *env, uint8_t *mem_buf, int n);
int cpu_gdb_read_register(CPUState *env, uint8_t *mem_buf, int n);
int cpu_gdb_write_register(CPUState *env, uint8_t *mem_buf, int n);


static inline uint32_t ubicom32_range_check(uint32_t high, uint32_t lo, uint32_t address) 
{
    return ((address >= lo) && (address <= high));
};

uint32_t ip7k_hole_check(CPUState *env, uint32_t address);
uint32_t ip7k_data_range_check(CPUState *env, uint32_t address);
uint32_t ip7k_instruction_range_check(CPUState *env, uint32_t address);

uint32_t ip8k_hole_check(CPUState *env, uint32_t address);
uint32_t ip8k_data_range_check(CPUState *env, uint32_t address);
uint32_t ip8k_instruction_range_check(CPUState *env, uint32_t address);

extern CPUUBICOM32State *threads[NUM_CONTEXTS];

#ifdef CONFIG_USER_ONLY
/* Linux uses 8k pages.  */
#define TARGET_PAGE_BITS 14
#else
/* Smallest TLB entry size is 4k.  */
#define TARGET_PAGE_BITS 14
#endif

#define cpu_init cpu_ubicom32_init
#define cpu_exec cpu_ubicom32_exec
#define cpu_gen_code cpu_ubicom32_gen_code
#define cpu_signal_handler cpu_ubicom32_signal_handler
#define cpu_list ubicom32_cpu_list

int cpu_ubicom32_handle_mmu_fault(CPUState *env, target_ulong address, int rw,
                              int mmu_idx, int is_softmmu);



static inline int cpu_mmu_index (CPUState *env)
{
    return 0;
}

void ubicom32_cpu_list(FILE *f, int (*cpu_fprintf)(FILE *f, const char *fmt, ...));

#include "cpu-all.h"
#include "exec-all.h"

static inline void cpu_pc_from_tb(CPUState *env, TranslationBlock *tb)
{
    env->thread_regs[0x34] = tb->pc;
}

static inline void cpu_get_tb_cpu_state(CPUState *env, target_ulong *pc,
                                        target_ulong *cs_base, int *flags)
{
    *pc = env->thread_regs[0x34];
    *cs_base = 0;
    *flags = 0;
    env->current_pc = env->thread_regs[0x34];
}

#define CCF_16_C 0x01
#define CCF_16_V 0x02
#define CCF_16_Z 0x04
#define CCF_16_N 0x08
#define CCF_32_C 0x10
#define CCF_32_V 0x20
#define CCF_32_Z 0x40
#define CCF_32_N 0x80
#define CCF_DSP_O 0x100000


#define CSR_PRIV_BIT 21		/* Privilege bit in CSR */
#define CSR_PREV_PRIV_BIT 22	/* Previous Privilege bit in CSR */
#define CSR_PRIV_MASK   (0x3 << CSR_PRIV_BIT)

#define S1_CHECK 0x01
#define DEST_CHECK 0x02
#define INST_CHECK 0x04

#define MMU_MISSQW0_TYPE_MISS_READ 	0x0
#define MMU_MISSQW0_TYPE_MISS_WRITE 	0x1
#define MMU_MISSQW0_TYPE_PRIVACC_READ 	0x2
#define MMU_MISSQW0_TYPE_PRIVACC_WRITE	0x3
#define MMU_MISSQW0_TYPE_PRIVACC_MASK	0x2

#define MMU_MISSQW0_TYPE_SERVICED	0x4
#define MMU_MISSQW0_TYPE_SERROR		0x8
 
#if 0
/* you can call this signal handler from your SIGBUS and SIGSEGV
   signal handlers to inform the virtual CPU of exceptions. non zero
   is returned if the signal was handled by the virtual CPU.  */
int cpu_ubicom32_signal_handler(int host_signum, void *pinfo,
                           void *puc);
void cpu_ubicom32_flush_flags(CPUUBICOM32State *, int);

enum {
    CC_OP_DYNAMIC, /* Use env->cc_op  */
    CC_OP_FLAGS, /* CC_DEST = CVZN, CC_SRC = unused */
    CC_OP_LOGIC, /* CC_DEST = result, CC_SRC = unused */
    CC_OP_ADD,   /* CC_DEST = result, CC_SRC = source */
    CC_OP_SUB,   /* CC_DEST = result, CC_SRC = source */
    CC_OP_CMPB,  /* CC_DEST = result, CC_SRC = source */
    CC_OP_CMPW,  /* CC_DEST = result, CC_SRC = source */
    CC_OP_ADDX,  /* CC_DEST = result, CC_SRC = source */
    CC_OP_SUBX,  /* CC_DEST = result, CC_SRC = source */
    CC_OP_SHIFT, /* CC_DEST = result, CC_SRC = carry */
};

#define SR_I_SHIFT 8
#define SR_I  0x0700
#define SR_M  0x1000
#define SR_S  0x2000
#define SR_T  0x8000

#define UBICOM32_SSP    0
#define UBICOM32_USP    1

/* CACR fields are implementation defined, but some bits are common.  */
#define UBICOM32_CACR_EUSP  0x10

#define MACSR_PAV0  0x100
#define MACSR_OMC   0x080
#define MACSR_SU    0x040
#define MACSR_FI    0x020
#define MACSR_RT    0x010
#define MACSR_N     0x008
#define MACSR_Z     0x004
#define MACSR_V     0x002
#define MACSR_EV    0x001

void ubicom32_set_irq_level(CPUUBICOM32State *env, int level, uint8_t vector);
void ubicom32_set_macsr(CPUUBICOM32State *env, uint32_t val);
void ubicom32_switch_sp(CPUUBICOM32State *env);

#define UBICOM32_FPCR_PREC (1 << 6)

void do_ubicom32_semihosting(CPUUBICOM32State *env, int nr);

/* There are 4 ColdFire core ISA revisions: A, A+, B and C.
   Each feature covers the subset of instructions common to the
   ISA revisions mentioned.  */

enum ubicom32_features {
    UBICOM32_FEATURE_CF_ISA_A,
    UBICOM32_FEATURE_CF_ISA_B, /* (ISA B or C).  */
    UBICOM32_FEATURE_CF_ISA_APLUSC, /* BIT/BITREV, FF1, STRLDSR (ISA A+ or C).  */
    UBICOM32_FEATURE_BRAL, /* Long unconditional branch.  (ISA A+ or B).  */
    UBICOM32_FEATURE_CF_FPU,
    UBICOM32_FEATURE_CF_MAC,
    UBICOM32_FEATURE_CF_EMAC,
    UBICOM32_FEATURE_CF_EMAC_B, /* Revision B EMAC (dual accumulate).  */
    UBICOM32_FEATURE_USP, /* User Stack Pointer.  (ISA A+, B or C).  */
    UBICOM32_FEATURE_EXT_FULL, /* 68020+ full extension word.  */
    UBICOM32_FEATURE_WORD_INDEX /* word sized address index registers.  */
};

static inline int ubicom32_feature(CPUUBICOM32State *env, int feature)
{
    return (env->features & (1u << feature)) != 0;
}

void ubicom32_cpu_list(FILE *f, int (*cpu_fprintf)(FILE *f, const char *fmt, ...));

void register_ubicom32_insns (CPUUBICOM32State *env);

#ifdef CONFIG_USER_ONLY
/* Linux uses 8k pages.  */
#define TARGET_PAGE_BITS 13
#else
/* Smallest TLB entry size is 1k.  */
#define TARGET_PAGE_BITS 10
#endif

#define cpu_init cpu_ubicom32_init
#define cpu_exec cpu_ubicom32_exec
#define cpu_gen_code cpu_ubicom32_gen_code
#define cpu_signal_handler cpu_ubicom32_signal_handler
#define cpu_list ubicom32_cpu_list

/* MMU modes definitions */
#define MMU_MODE0_SUFFIX _kernel
#define MMU_MODE1_SUFFIX _user
#define MMU_USER_IDX 1
static inline int cpu_mmu_index (CPUState *env)
{
    return (env->sr & SR_S) == 0 ? 1 : 0;
}

int cpu_ubicom32_handle_mmu_fault(CPUState *env, target_ulong address, int rw,
                              int mmu_idx, int is_softmmu);

#if defined(CONFIG_USER_ONLY)
static inline void cpu_clone_regs(CPUState *env, target_ulong newsp)
{
    if (newsp)
        env->aregs[7] = newsp;
    env->dregs[0] = 0;
}
#endif

#endif /* if 0 */
#endif
