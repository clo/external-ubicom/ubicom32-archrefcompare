/*-----------------------------------------------------------------------
 *  ubicom32 opc block structure defines
 *
 * Copyright (c) 2017, The Linux Foundation. All rights reserved.
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 * -----------------------------------------------------------------------
 */

struct ocp_info {
    const struct ubicom32_def_t *def;
    struct QEMUTimer *mptval_timer;
    struct QEMUTimer *sysval_timer;
    uint32_t sysval_frequency;
    uint32_t mptval_frequency;
    uint32_t *global_regs;
    qemu_irq *ubi_irq;
    uint32_t pll_reg;		/* This represents location 0000 of the ocp block which for the 7k contains the PLL config. */
    CharDriverState *chr;
};

extern struct ocp_info ubicom32_ocp_info;
