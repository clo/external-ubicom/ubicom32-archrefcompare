/*
 *  ubicom32 translation
 *
 * Copyright (c) 2017, The Linux Foundation. All rights reserved.
 *
 *  Copyright (c) 2005-2007 CodeSourcery
 *  Written by Paul Brook
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <inttypes.h>

#include "config.h"
#include "cpu.h"
#include "exec-all.h"
#include "disas.h"
#include "tcg-op.h"
#include "qemu-log.h"
#include "gdbstub.h"
#include "ubicom32cpu.h"

#include "helpers.h"
#define GEN_HELPER 1
#include "helpers.h"

extern uint32_t init_pc;

struct cpu_register_info {
  int index;
  int address;
  const char *name;
};

struct cpu_register_info reg_info[] = {
	/* Per Thread registers */
	{0x00, 0x000, "d0"},
	{0x01, 0x004, "d1"},
	{0x02, 0x008, "d2"},
	{0x03, 0x00c, "d3"},
	{0x04, 0x010, "d4"},
	{0x05, 0x014, "d5"},
	{0x06, 0x018, "d6"},
	{0x07, 0x01c, "d7"},
	{0x08, 0x020, "d8"},
	{0x09, 0x024, "d9"},
	{0x0a, 0x028, "d10"},
	{0x0b, 0x02c, "d11"},
	{0x0c, 0x030, "d12"},
	{0x0d, 0x034, "d13"},
	{0x0e, 0x038, "d14"},
	{0x0f, 0x03c, "d15"},
	{0x10, 0x040, "u16"},
	{0x11, 0x044, "u17"},
	{0x12, 0x048, "u18"},
	{0x13, 0x04c, "u19"},
	{0x14, 0x050, "u20"},
	{0x15, 0x054, "u21"},
	{0x16, 0x058, "u22"},
	{0x17, 0x05c, "u23"},
	{0x18, 0x060, "u24"},
	{0x19, 0x064, "u25"},
	{0x1a, 0x068, "u26"},
	{0x1b, 0x06c, "u27"},
	{0x1c, 0x070, "u28"},
	{0x1d, 0x074, "u29"},
	{0x1e, 0x078, "u30"},
	{0x1f, 0x07c, "u31"},
	{0x20, 0x080, "a0"},
	{0x21, 0x084, "a1"},
	{0x22, 0x088, "a2"},
	{0x23, 0x08c, "a3"},
	{0x24, 0x090, "a4"},
	{0x25, 0x094, "a5"},
	{0x26, 0x098, "a6"},
	{0x27, 0x09c, "a7"},
	{0x28, 0x0a0, "acc0_hi"},
	{0x29, 0x0a4, "acc0_lo"},
	{0x2a, 0x0a8, "mac_rc16"},
	{0x2b, 0x0ac, "source3"},
	{0x2c, 0x0b0, "inst_cnt"},
	{0x2d, 0x0b4, "csr"},
	{0x2e, 0x0b8, "rosr"},
	{0x2f, 0x0bc, "iread_data"},
	{0x30, 0x0c0, "int_mask0"},
	{0x31, 0x0c4, "int_mask1"},
	{0x32, 0x0c8, "int_mask2"},
	{0x33, 0x0cc, "u51"},
	{0x34, 0x0d0, "pc"},
	{0x35, 0x0d4, "trap_cause"},
	{0x36, 0x0d8, "acc1_hi"},
	{0x37, 0x0dc, "acc1_lo"},
	{0x38, 0x0e0, "previous_pc"},
	{0x39, 0x0e4, "ucsr"},
	{0x3a, 0x0e8, "u58"},
	{0x3b, 0x0ec, "u59"},
	{0x3c, 0x0f0, "u60"},
	{0x3d, 0x0f4, "u61"},
	{0x3e, 0x0f8, "u62"},
	{0x3f, 0x0fc, "u63"},

	/* Global registers */
	{0x40, 0x100, "chip_id"},
	{0x41, 0x104, "int_stat0"},
	{0x42, 0x108, "int_stat1"},
	{0x43, 0x10c, "int_stat2"},
	{0x44, 0x110, "u68"},
	{0x45, 0x114, "int_set0"},
	{0x46, 0x118, "int_set1"},
	{0x47, 0x11c, "int_set2"},
	{0x48, 0x120, "u72"},
	{0x49, 0x124, "int_clr0"},
	{0x4a, 0x128, "int_clr1"},
	{0x4b, 0x12c, "int_clr2"},
	{0x4c, 0x130, "u76"},
	{0x4d, 0x134, "global_ctrl"},
	{0x4e, 0x138, "mt_active"},
	{0x4f, 0x13c, "mt_active_set"},
	{0x50, 0x140, "mt_active_clr"},
	{0x51, 0x144, "mt_dbg_active"},
	{0x52, 0x148, "mt_dbg_active_set"},
	{0x53, 0x14c, "mt_en"},
	{0x54, 0x150, "mt_hpri"},
	{0x55, 0x154, "mt_hrt"},
	{0x56, 0x158, "mt_break"},
	{0x57, 0x15c, "mt_break_clr"},
	{0x58, 0x160, "mt_single_step"},
	{0x59, 0x164, "mt_min_delay_en"},
	{0x5a, 0x168, "mt_break_set"},
	{0x5b, 0x16c, "perr_addr"},
	{0x5c, 0x170, "dcapt"},
	{0x5d, 0x174, "u93"},
	{0x5e, 0x178, "u94"},
	{0x5f, 0x17c, "mt_dbg_active_clr"},
	{0x60, 0x180, "scratchpad0"},
	{0x61, 0x184, "scratchpad1"},
	{0x62, 0x188, "scratchpad2"},
	{0x63, 0x18c, "scratchpad3"},
	{0x64, 0x190, "scratchpad4"},
	{0x65, 0x194, "scratchpad5"},
	{0x66, 0x198, "scratchpad6"},
	{0x67, 0x19c, "u103"},
	{0x68, 0x1a0, "chip_cfg"},
	{0x69, 0x1a4, "mt_i_blocked"},
	{0x6a, 0x1a8, "mt_d_blocked"},
	{0x6b, 0x1ac, "mt_i_blocked_set"},
	{0x6c, 0x1b0, "mt_d_blocked_set"},
	{0x6d, 0x1b4, "mt_blocked_clr"},
	{0x6e, 0x1b8, "mt_trap_en"},
	{0x6f, 0x1bc, "mt_trap"},
	{0x70, 0x1c0, "mt_trap_set"},
	{0x71, 0x1c4, "mt_trap_clr"},
	{0x72, 0x1c8, "sep"},
	{0x73, 0x1cc, "u115"},
	{0x74, 0x1d0, "u116"},
	{0x75, 0x1d4, "u117"},
	{0x76, 0x1d8, "u118"},
	{0x77, 0x1dc, "u119"},
	{0x78, 0x1e0, "u120"},
	{0x79, 0x1e4, "u121"},
	{0x7a, 0x1e8, "u122"},
	{0x7b, 0x1ec, "u123"},
	{0x7c, 0x1f0, "u124"},
	{0x7d, 0x1f4, "u125"},
	{0x7e, 0x1f8, "u126"},
	{0x7f, 0x1fc, "u127"},
	{0x80, 0x200, "i_range0_hi"},
	{0x81, 0x204, "i_range1_hi"},
	{0x82, 0x208, "i_range2_hi"},
	{0x83, 0x20c, "i_range3_hi"},
	{0x84, 0x210, "u132"},
	{0x85, 0x214, "u133"},
	{0x86, 0x218, "u134"},
	{0x87, 0x21c, "u135"},
	{0x88, 0x220, "i_range0_lo"},
	{0x89, 0x224, "i_range1_lo"},
	{0x8a, 0x228, "i_range2_lo"},
	{0x8b, 0x22c, "i_rnage3_lo"},
	{0x8c, 0x230, "u140"},
	{0x8d, 0x234, "u141"},
	{0x8e, 0x238, "u142"},
	{0x8f, 0x23c, "u143"},
	{0x90, 0x240, "i_range0_en"},
	{0x91, 0x244, "i_range1_en"},
	{0x92, 0x248, "i_range2_en"},
	{0x93, 0x24c, "i_rnage3_en"},
	{0x94, 0x250, "u148"},
	{0x95, 0x254, "u149"},
	{0x96, 0x258, "u140"},
	{0x97, 0x25c, "u151"},
	{0x98, 0x260, "d_range0_hi"},
	{0x99, 0x264, "d_range1_hi"},
	{0x9a, 0x268, "d_range2_hi"},
	{0x9b, 0x26c, "d_range3_hi"},
	{0x9c, 0x270, "d_range4_hi"},
	{0x9d, 0x274, "d_range5_hi"},
	{0x9e, 0x278, "u158"},
	{0x9f, 0x27c, "u159"},
	{0xa0, 0x280, "d_range0_lo"},
	{0xa1, 0x284, "d_range1_lo"},
	{0xa2, 0x288, "d_range2_lo"},
	{0xa3, 0x28c, "d_range3_lo"},
	{0xa4, 0x290, "d_range4_lo"},
	{0xa5, 0x294, "d_range5_lo"},
	{0xa6, 0x298, "u166"},
	{0xa7, 0x29c, "u167"},
	{0xa8, 0x2a0, "d_range0_en"},
	{0xa9, 0x2a4, "d_range1_en"},
	{0xaa, 0x2a8, "d_range2_en"},
	{0xab, 0x2ac, "d_range3_en"},
	{0xac, 0x2b0, "d_range4_en"},
	{0xad, 0x2b4, "d_range5_en"},
	{0xae, 0x2b8, "u174"},
	{0xaf, 0x2bc, "u175"},
	{0xb0, 0x2c0, "i_range0_user_en"},
	{0xb1, 0x2c4, "i_range1_user_en"},
	{0xb2, 0x2c8, "i_range2_user_en"},
	{0xb3, 0x2cc, "i_range3_user_en"},
	{0xb4, 0x2d0, "u180"},
	{0xb5, 0x2d4, "u181"},
	{0xb6, 0x2d8, "u182"},
	{0xb7, 0x2dc, "u183"},
	{0xb8, 0x2e0, "d_range0_user_en"},
	{0xb9, 0x2e4, "d_range1_user_en"},
	{0xba, 0x2e8, "d_range2_user_en"},
	{0xbb, 0x2ec, "d_range3_user_en"},
	{0xbc, 0x2f0, "d_range4_user_en"},
	{0xbd, 0x2f4, "d_range5_user_en"},
	{0xbe, 0x2f8, "u190"},
	{0xbf, 0x2fc, "u191"},
};


/* Register Indices. */
static TCGv_ptr cpu_env;
static TCGv thread_number;
struct reg_indices {
    TCGv thread_regs[64];
    TCGv global_regs[64];
} ubicom32_regs;

static TCGv dest_data;
static TCGv src1_data;
static TCGv src2_data;
static TCGv src1_address;
static TCGv dest_address;
static TCGv bset_clr_tmp;
static TCGv postinc_areg;
static TCGv increment;
static TCGv current_pc;
static TCGv_i64 src1_64;
static TCGv_i64 src2_64;
static TCGv_i64 temp_64;
static TCGv_i64 dest_64;
#include "gen-icount.h"

CPUUBICOM32State *threads[NUM_CONTEXTS];
uint32_t ubicom32_global_registers[192];

void ubicom32_tcg_init(void)
{
    int j;
    cpu_env = tcg_global_reg_new_ptr(TCG_AREG0, "env");

    /*
     * Setup thread registers.
     */
    for (j=0; j<64; j++) {
	//printf("Setting up %s\n", reg_info[j].name);
	ubicom32_regs.thread_regs[j] =
	    tcg_global_mem_new(TCG_AREG0, offsetof(CPUUBICOM32State, thread_regs[j]),
			       reg_info[j].name);
    }

    /*
     * Thread_number
     */
    thread_number = tcg_global_mem_new(TCG_AREG0, offsetof(CPUUBICOM32State, thread_number),
				       "thread_number");
    /*
     * dest_data
     */
    dest_data = tcg_global_mem_new(TCG_AREG0, offsetof(CPUUBICOM32State, dest_data),
					 "dest_data");

    /*
     * src1_data
     */
    src1_data = tcg_global_mem_new(TCG_AREG0, offsetof(CPUUBICOM32State, src1_data),
					 "src1_data");

    /*
     * src2_data
     */
    src2_data = tcg_global_mem_new(TCG_AREG0, offsetof(CPUUBICOM32State, src2_data),
					 "src2_data");

    /*
     * dest_address
     */
    dest_address = tcg_global_mem_new(TCG_AREG0, offsetof(CPUUBICOM32State, dest_address),
					 "dest_address");

    /*
     * src1_address
     */
    src1_address = tcg_global_mem_new(TCG_AREG0, offsetof(CPUUBICOM32State, src1_address),
					 "src1_address");

    /*
     * bset_clr_tmp
     */
    bset_clr_tmp = tcg_global_mem_new(TCG_AREG0, offsetof(CPUUBICOM32State, bset_clr_tmp),
					 "bset_clr_tmp");

    /*
     * postinc_areg
     */
    postinc_areg = tcg_global_mem_new(TCG_AREG0, offsetof(CPUUBICOM32State, postinc_areg),
					 "postinc_areg");

    /*
     * increment
     */
    increment = tcg_global_mem_new(TCG_AREG0, offsetof(CPUUBICOM32State, increment),
					 "increment");

    /*
     * current_pc
     */
    current_pc = tcg_global_mem_new(TCG_AREG0, offsetof(CPUUBICOM32State, current_pc),
					 "current_pc");

    /*
     * src1_64
     */
    src1_64 = tcg_global_mem_new_i64(TCG_AREG0, offsetof(CPUUBICOM32State, src1_64),
					 "src1_64");

    /*
     * src2_64
     */
    src2_64 = tcg_global_mem_new_i64(TCG_AREG0, offsetof(CPUUBICOM32State, src2_64),
					 "src2_64");

    /*
     * temp_64
     */
    temp_64 = tcg_global_mem_new_i64(TCG_AREG0, offsetof(CPUUBICOM32State, temp_64),
					 "temp_64");

    /*
     * dest_64
     */
    dest_64 = tcg_global_mem_new_i64(TCG_AREG0, offsetof(CPUUBICOM32State, dest_64),
					 "dest_64");

#define GEN_HELPER 2
#include "helpers.h"
}

/* internal defines */
typedef struct DisasContext {
    CPUUBICOM32State *env;
    target_ulong insn_pc; /* Start of the current instruction.  */
    target_ulong pc;
    int is_jmp;
    int cc_op;
    int user;
    struct TranslationBlock *tb;
    int singlestep_enabled;
    int is_mem;
    uint32_t opcode;
    int need_dest_checking;
    int need_src1_checking;
    int is_lea_pdec;
} DisasContext;

struct auto_increment {
  uint32_t areg_index;
  int increment;
};

#if defined(CONFIG_USER_ONLY)
#define IS_USER(s) 1
#else
#define IS_USER(s) s->user
#endif

static void gen_store_auto_inc(DisasContext *s, struct auto_increment *auto_inc)
{
    if(!auto_inc->areg_index) {
	/*
	 * Nothing to do.
	 */
	return;
    }

    /*
     * Store the incremented address back into the A register.
     */
    tcg_gen_add_i32(ubicom32_regs.thread_regs[auto_inc->areg_index], ubicom32_regs.thread_regs[auto_inc->areg_index], tcg_const_i32(auto_inc->increment));
}

/**************************************
* Start source1 operand routines
**************************************/

/*
** static void gen_src1_general(DisasContext *s, uint32_t insn, int opsize, int signed_flag)
**
** Handles the general case of src1 encoding in bits 0-10 of the instruction.
** if opsize == 8, then loads the data into ->src1_64, otherwise loads data into ->src1_data.
*/

static void gen_src1_general(DisasContext *s, uint32_t insn, int opsize, int signed_flag)
{
	uint32_t address_mode = (insn >> 8) & 0x7;
	int index = IS_USER(s);
	int data = insn & 0xff;

	switch (address_mode) {

		case 0: /* 8 bit signed immediate */

			/* Check for a negative number.  */

			if (data >> 7)
				data |= 0xffffff00;

			/* Load immediate data into src1_data */

			if (opsize == 8)
				tcg_gen_mov_i64(src1_64, tcg_const_i64(data));
			else
				tcg_gen_mov_i32(src1_data, tcg_const_i32(data));

			break;

		case 1: /* Direct register */

			if (opsize < 8)
				gen_helper_src1_direct(cpu_env, tcg_const_i32(data));
			else
				gen_helper_src1_64_direct(cpu_env, tcg_const_i32(data));

			break;

		default: /* Move the data from memory into src1_data */

			switch(opsize) {

				case 1:
					if (signed_flag)
						tcg_gen_qemu_ld8s(src1_data, src1_address, index);
					else
						tcg_gen_qemu_ld8u(src1_data, src1_address, index);
					break;

				case 2:
					if (signed_flag)
						tcg_gen_qemu_ld16s(src1_data, src1_address, index);
					else
						tcg_gen_qemu_ld16u(src1_data, src1_address, index);
					break;

				case 4:
					if (signed_flag)
						tcg_gen_qemu_ld32s(src1_data, src1_address, index);
					else
						tcg_gen_qemu_ld32u(src1_data, src1_address, index);
					break;

				case 8:
					tcg_gen_qemu_ld64(src1_64, src1_address, index);
					break;
			}

			break;
	}
}

static void gen_src1(DisasContext *s, uint32_t insn, int opsize)
{
	gen_src1_general(s, insn, opsize, 1);
}

static void gen_src1_address(DisasContext *s, uint32_t insn, int opsize, struct auto_increment *auto_inc)
{
    uint32_t address_mode = (insn >> 8) & 0x7;

    auto_inc->areg_index = 0;
    s->need_src1_checking = 1;
    tcg_gen_movi_i32(postinc_areg, 0);
    switch(address_mode) {
    case 0: /* 8 bit signed immediate */
	{
	    s->need_src1_checking = 0;
	    return;
	    break;
	}
    case 1: /* Direct register */
	{
	    s->need_src1_checking = 0;

	    if (s->env->def->chip_id == UBICOM32_8K_CHIP_ID) {
		/*
		 * Trigger direct dest register privilege checking.
		 */
		int reg_index = (insn & 0xff);
		gen_helper_direct_register_privilege_check(cpu_env, tcg_const_i32(reg_index));
	    }
	    return;
	    break;
	}
    case 2: /* Indirect with pre/post increment */
	{
	    uint32 areg_index = ((insn >> 5) & 0x7) + 0x20;
	    int increment = (insn & 0xf);

	    if (increment >> 3) {
		/*
		 * Negative increment.
		 */
		increment |= 0xfffffff0;
	    }

	    /*
	     * Scale the increment.
	     */
	    increment *= opsize;

	    if (insn & 0x10) {
		/*
		 * Need to preincrement the address.
		 */
		tcg_gen_add_i32(src1_address, ubicom32_regs.thread_regs[areg_index], tcg_const_i32(increment));
	    } else {
		tcg_gen_mov_i32(src1_address, ubicom32_regs.thread_regs[areg_index]);
	    }

	    /*
	     * Tell the caller that it has to generate store of incremented A register.
	     */
	    auto_inc->areg_index = areg_index;
	    auto_inc->increment = increment;
	    tcg_gen_movi_i32(postinc_areg, areg_index);
	    tcg_gen_movi_i32(postinc_areg, increment);
	    break;
	}
    case 3: /* Indirect with index */
	{
	    uint32 areg_index = ((insn >> 5) & 0x7) + 0x20;
	    uint32 dreg_index = (insn & 0xf);

	    /*
	     * Take content of D register and scale it by opsize and store into src1_address.
	     */
	    tcg_gen_muli_i32(src1_address, ubicom32_regs.thread_regs[dreg_index], opsize);

	    /*
	     * Add content of A register to the above value to get the effective address.
	     */
	    tcg_gen_add_i32(src1_address, src1_address, ubicom32_regs.thread_regs[areg_index]);

	    break;
	}
    default: /* Indirect with offset */
	{
	    int offset = (insn & 0x1f) | ((insn >> 3) & 0x60);
	    uint32 areg_index = ((insn >> 5) & 0x7) + 0x20;

	    /*
	     * Scale the offset by opsize.
	     */
	    offset *= opsize;

	    /*
	     * Add a register contents to create the effective address.
	     */
	    tcg_gen_add_i32(src1_address, tcg_const_i32(offset), ubicom32_regs.thread_regs[areg_index]);

	    break;
	}
    }

    if (s->is_lea_pdec) {
	/*
	 * For lea and Pdec instructions the generated src1_address is the src1_data.
	 * We will not alignment check it. Things will blow up when they use it if the
	 * the address is wrong.
	 */
	s->need_src1_checking = 0;
	return;
    }

    /*
     * Trigger src1 address checking.
    gen_helper_src1_check(cpu_env, tcg_const_i32(opsize));
     */

    return;
}

/**************************************
* End source1 operand routines
**************************************/
/**************************************
* Start source2 operand routines
**************************************/

static void gen_src2_32_Dn_or_acc(uint32_t register_index)
{
	switch (register_index) {

		case 16:

			register_index = REGISTER_ACC0_LO;
			break;

		case 17:

			register_index = REGISTER_ACC0_HI;
			break;

		case 18:

			register_index = REGISTER_ACC1_LO;
			break;

		case 19:

			register_index = REGISTER_ACC1_HI;
			break;
	}

	tcg_gen_mov_i32(src2_data, ubicom32_regs.thread_regs[register_index]);
}

static void gen_src2_64_direct(uint32_t register_index)
{
//	printf("xxx gen_src2_64_direct(): high register: 0x%08x, low_register: 0x%08x\n", register_index, register_index + 1);

	tcg_gen_ext_i32_i64(src2_64, ubicom32_regs.thread_regs[register_index]);

	if ((register_index == REGISTER_ACC0_HI) || (register_index == REGISTER_ACC1_HI)) {

		/* Register is an accumulator, so shift the value left and OR in the low 32-bits.
		   Ensure value is properly sign-extended from 48 bits to 64 bits. */

		tcg_gen_shli_i64(src2_64, src2_64, 48);
		tcg_gen_sari_i64(src2_64, src2_64, 16);
		tcg_gen_ext_i32_i64(temp_64, ubicom32_regs.thread_regs[register_index + 1]);
		tcg_gen_andi_i64(temp_64, temp_64, 0xffffffffLL);
		tcg_gen_or_i64(src2_64, src2_64, temp_64);
	}
}

/**************************************
* End source2 operand routines
**************************************/
/**************************************
* Start dest operand routines
**************************************/

static void gen_dest(DisasContext *s, uint32_t insn, int opsize)
{
	uint32_t address_mode;
	int index = IS_USER(s);
	int label;

	insn = (insn >> 16) & 0x7ff;
	address_mode = (insn >> 8) & 0x7;

	switch(address_mode) {

		case 0: /* 8 bit signed immediate */

			/*
			* If destination field is immediate then throw away the result.
			*/
			return;

		case 1: /* Direct register */

			{
				int reg_index = (insn & 0xff);

				//printf("gen_dest(): direct register 0x%x\n", reg_index);

				gen_helper_dest_direct(cpu_env, tcg_const_i32(reg_index));
				return;
			}

		default:

			/*
			 * Move the data from dest_data to memory. We will call a helper function which will test and see if
			 * any mmu registers are being manipulated and take appropriate action.
			*/

			switch (opsize) {

				case 1:

					tcg_gen_qemu_st8(dest_data, dest_address, index);
					break;

				case 2:

					/* Branch around the store instruction if the destination address is not aligned. */

	    				label = gen_new_label();
					tcg_gen_andi_i32(bset_clr_tmp, dest_address, 1);
	    				tcg_gen_brcondi_i32(TCG_COND_NE, bset_clr_tmp, 0, label);
					tcg_gen_qemu_st16(dest_data, dest_address, index);
	    				gen_set_label(label);
					break;

				case 4:

					/* Branch around the store instruction if the destination address is not aligned. */

	    				label = gen_new_label();
					tcg_gen_andi_i32(bset_clr_tmp, dest_address, 3);
	    				tcg_gen_brcondi_i32(TCG_COND_NE, bset_clr_tmp, 0, label);
					tcg_gen_qemu_st32(dest_data, dest_address, index);
	    				gen_set_label(label);
					break;

				case 8:

					/* Branch around the store instruction if the destination address is not aligned. */

	    				label = gen_new_label();
					tcg_gen_andi_i32(bset_clr_tmp, dest_address, 7);
	    				tcg_gen_brcondi_i32(TCG_COND_NE, bset_clr_tmp, 0, label);
					tcg_gen_qemu_st64(dest_64, dest_address, index);
	    				gen_set_label(label);
					break;
			}
	}
}

static void gen_dest_address(DisasContext *s, uint32_t insn, int opsize, struct auto_increment *auto_inc)

{
    uint32_t address_mode;

    s->need_dest_checking = 1;
    auto_inc->areg_index = 0;
    insn = (insn >> 16) & 0x7ff;
    address_mode = (insn >> 8) & 0x7;
    switch(address_mode) {
    case 0: /* 8 bit signed immediate */
	{
	    s->need_dest_checking = 0;
	    return;
	    break;
	}
    case 1: /* Direct register */
	{
	    s->need_dest_checking = 0;

	    if (s->env->def->chip_id == UBICOM32_8K_CHIP_ID) {
		/*
		 * Trigger direct dest register privilege checking.
		 */
                int reg_index = (insn & 0xff);
		gen_helper_direct_register_privilege_check(cpu_env, tcg_const_i32(reg_index));
	    }

	    return;
	    break;
	}
    case 2: /* Indirect with pre/post increment */
	{
	    uint32 areg_index = ((insn >> 5) & 0x7) + 0x20;
	    int increment = (insn & 0xf);

	    if (increment >> 3) {
		/*
		 * Negative increment.
		 */
		increment |= 0xfffffff0;
	    }

	    /*
	     * Scale the increment.
	     */
	    increment *= opsize;

	    if (insn & 0x10) {
		/*
		 * Need to preincrement the address.
		 */
		tcg_gen_add_i32(dest_address, ubicom32_regs.thread_regs[areg_index], tcg_const_i32(increment));
	    } else {
		tcg_gen_mov_i32(dest_address, ubicom32_regs.thread_regs[areg_index]);
	    }

	    /*
	     * Tell the caller that it has to generate store of incremented A register.
	     */
	    auto_inc->areg_index = areg_index;
	    auto_inc->increment = increment;

	    break;
	}
    case 3: /* Indirect with index */
	{
	    uint32 areg_index = ((insn >> 5) & 0x7) + 0x20;
	    uint32 dreg_index = (insn & 0xf);

	    /*
	     * Take content of D register and scale it by opsize and store into dest_address.
	     */
	    tcg_gen_muli_i32(dest_address, ubicom32_regs.thread_regs[dreg_index], opsize);

	    /*
	     * Add content of A register to the above value to get the effective address.
	     */
	    tcg_gen_add_i32(dest_address, dest_address, ubicom32_regs.thread_regs[areg_index]);

	    break;
	}
    default: /* Indirect with offset */
	{
	    int offset = (insn & 0x1f) | ((insn >> 3) & 0x60);
	    uint32 areg_index = ((insn >> 5) & 0x7) + 0x20;

	    /*
	     * Scale the offset by opsize.
	     */
	    offset *= opsize;

	    /*
	     * Add a register contents to create the effective address.
	     */
	    tcg_gen_add_i32(dest_address, tcg_const_i32(offset), ubicom32_regs.thread_regs[areg_index]);

	    break;
	}
    }

    /*
     * Trigger dest address checking.
    gen_helper_dest_check(cpu_env, tcg_const_i32(opsize));
     */

    return;
}

/**************************************
* End dest operand routines
**************************************/

static void gen_address_check(DisasContext *s, int opsize)
{
    uint32_t check = 0;
    if (s->need_src1_checking) {
	/*
	 * src1 access needs checkng.
	 */
	check |= S1_CHECK;
    }

    if (s->need_dest_checking) {
	/*
	 * destination access needs checking.
	 */
	check |= DEST_CHECK;
    }

    if (check) {
	gen_helper_address_check(cpu_env, tcg_const_i32(opsize), tcg_const_i32(check));
    }
}

static void init_globals(uint32_t *ptr, uint32_t init_val, uint32_t num_regs)
{
    while(num_regs--) {
	*ptr++ = init_val;
    }
}

void cpu_reset (CPUUBICOM32State *env)
{
    if (qemu_loglevel_mask(CPU_LOG_RESET)) {
	qemu_log("CPU Reset (CPU %d)\n", env->cpu_index);
	log_cpu_state(env, 0);
    }

    memset(env, 0, offsetof(CPUUBICOM32State, breakpoints));

    tlb_flush(env, 1);

    /*
     * Set rosr from env->cpu_index
     */
    env->thread_regs[0x2e] = env->cpu_index << 2;

    /*
     * Set pc to flash_base
     */
//    env->thread_regs[0x34] = env->def->flash_base;
    env->thread_regs[0x34] = init_pc ? init_pc : env->def->reset_vector;

    /*
     * Setup chip_id from env->dev
     */
    env->global_regs[GLOBAL_REG(0x40)] = env->def->chip_id;

    /*
     * Setup cc_sr_index;
     */
    if (env->def->chip_id == UBICOM32_7K_CHIP_ID) {
	/*
	 * 7k. CSR is the cc reg.
	 */
	env->cc_sr_index = 0x2d;
    } else {
	/*
	 * 7k. CSR is the cc reg.
	 */
	env->cc_sr_index = 0x39;
    }

    /*
     * Set up mt_en, mt_active, mt_hpri, mt_dbg_active
     */
    env->global_regs[GLOBAL_REG(0x4e)] = 0x1;
    env->global_regs[GLOBAL_REG(0x53)] = 0x1;
    env->global_regs[GLOBAL_REG(0x54)] = 0x1;
    env->global_regs[GLOBAL_REG(0x51)] = 0x1;

    /*
     * Reset i_rangeX_hi registers to 0xfffffffc
     */
    init_globals(&env->global_regs[GLOBAL_REG(0x80)], 0xfffffffc, 4);

    /*
     * Reset i_rangeX_en to 0xfff
     */
    init_globals(&env->global_regs[GLOBAL_REG(0x90)], 0xfff, 4);

    /*
     * Reset d_rangeX_hi registers to 0xfffffffc
     */
    init_globals(&env->global_regs[GLOBAL_REG(0x98)], 0xfffffffc, 6);

    /*
     * Reset d_rangeX_en to 0xfff
     */
    init_globals(&env->global_regs[GLOBAL_REG(0xa8)], 0xfff, 6);

    /*
     * Reset i_rangeX_user_en to 0xfff
     */
    init_globals(&env->global_regs[GLOBAL_REG(0xb0)], 0xfff, 4);

    /*
     * Reset d_rangeX_user_en to 0xfff
     */
    init_globals(&env->global_regs[GLOBAL_REG(0xb8)], 0xfff, 6);
}

CPUUBICOM32State *cpu_ubicom32_init (const char *cpu_model)
{
    CPUUBICOM32State *env;
    const struct ubicom32_def_t *def;
    static int tcg_inited = 0;

    def = cpu_ubicom32_find_by_name(cpu_model);
    if (!def)
        return NULL;
    env = qemu_mallocz(sizeof(CPUUBICOM32State));
    env->def = def;
    env->global_regs = ubicom32_global_registers;
    cpu_exec_init(env);
    env->cpu_model_str = cpu_model;

    if (!tcg_inited) {
	ubicom32_tcg_init();
	tcg_inited = 1;

	/*
	 * If the cpu is an 8k it has 25 more registers and we have to list them like a coprocessor.
	 */
	if (def->chip_id == 0x4ffff) {
	  /*
	   * 8k.
	   */
	  gdb_register_coprocessor(env, cpu_gdb_read_extra_register, cpu_gdb_write_extra_register, 25, NULL, 103);
	}
    } else {
      env->gdb_regs = threads[0]->gdb_regs;
    }

    cpu_reset(env);
    qemu_init_vcpu(env);
    return env;
}

void cpu_dump_state(CPUState *env, FILE *f,
                    int (*cpu_fprintf)(FILE *f, const char *fmt, ...),
                    int flags)
{
    int i;

    uint32_t *ptr = env->thread_regs;
    if (env->def->has_mmu) {
	cpu_fprintf (f, "Printing data for Ubicom32 8K family Thread %d\n", env->cpu_index);
    } else {
	cpu_fprintf (f, "Printing data for Ubicom32 7K family Thread %d\n", env->cpu_index);
    }

    /*
     * Print the d registers 4 per row.
     */
    for (i=0; i< 16; i+= 4, ptr+= 4) {
	cpu_fprintf (f, "D%d = %08x D%d = %08x D%d = %08x D%d = %08x\n",
		     i, ptr[0], i+1, ptr[1], i+2, ptr[2], i+3, ptr[3]);
    }

    /*
     * Print the A registers 4 per row.
     */
    ptr += 16;
    for (i=0; i< 8; i+= 4, ptr+= 4) {
	cpu_fprintf (f, "A%d = %08x A%d = %08x A%d = %08x A%d = %08x\n",
		     i, ptr[0], i+1, ptr[1], i+2, ptr[2], i+3, ptr[3]);
    }


    /*
     * Print PC and CSR.
     */
    if (env->def->has_mmu) {
	/*
	 * 8K Print PC, CSR, UCSR
	 */
	cpu_fprintf (f, "PC = %08x PREV_PC = %08x CSR = %08x UCSR = %08x\n",
		     env->thread_regs[0x34], env->thread_regs[0x38], env->thread_regs[0x2d], env->thread_regs[0x39]);

	/*
	 * Print int_mask0 - 2
	 */
	cpu_fprintf (f, "int_mask0 = %08x int_mask1 = %08x int_maks2 = %08x\n",
		     env->thread_regs[0x30], env->thread_regs[0x31], env->thread_regs[0x32]);

	/*
	 * Print trap_cause.
	 */
	cpu_fprintf (f, "trap_cause = %08x\n",
		     env->thread_regs[0x35]);
    } else {
	/*
	 * 7K Print PC, CSR
	 */
	cpu_fprintf (f, "PC = %08x PREV_PC = %08x CSR = %08x\n",
		     env->thread_regs[0x34], env->thread_regs[0x38], env->thread_regs[0x2d]);

	/*
	 * Print int_mask0 - 1
	 */
	cpu_fprintf (f, "int_mask0 = %08x int_mask1 = %08x\n",
		     env->thread_regs[0x30], env->thread_regs[0x31]);

	/*
	 * Print trap_cause.
	 */
	cpu_fprintf (f, "trap_cause = %08x\n",
		     env->thread_regs[0x35]);
    }
}

void gen_pc_load(CPUState *env, TranslationBlock *tb,
		 unsigned long searched_pc, int pc_pos, void *puc)
{
    env->thread_regs[0x34] = gen_opc_pc[pc_pos];
}

/* Generate a jump to an immediate address.  */
static void gen_jmp_tb(DisasContext *s, int n, uint32_t dest)
{
    TranslationBlock *tb;

    tb = s->tb;
    if (unlikely(s->singlestep_enabled)) {
	tcg_gen_mov_i32(ubicom32_regs.thread_regs[0x34], tcg_const_i32(dest));
	gen_helper_raise_exception(tcg_const_i32(EXCP_DEBUG));
    } else if ((tb->pc & TARGET_PAGE_MASK) == (dest & TARGET_PAGE_MASK) ||
               (s->pc & TARGET_PAGE_MASK) == (dest & TARGET_PAGE_MASK)) {
        tcg_gen_goto_tb(n);
	tcg_gen_mov_i32(ubicom32_regs.thread_regs[0x34], tcg_const_i32(dest));
        tcg_gen_exit_tb((long)tb + n);
    } else {
	tcg_gen_mov_i32(ubicom32_regs.thread_regs[0x34], tcg_const_i32(dest));
        tcg_gen_exit_tb(0);
    }
    s->is_jmp = DISAS_TB_JUMP;
}

typedef enum {

	SRC1_UNKNOWN,
	SRC1_GENERAL			/* general addressing mode in bits 10-0 */

} SRC1_TYPE;

typedef enum {

	SRC2_UNKNOWN,
	SRC2_DSP_IMM_OR_DN_OR_ACC,		/* opcode bit 26 determines whether immed or reg
						   opcode bit 16 determines whether bits 14-11 are Dn or acc */


} SRC2_TYPE;

static void gen_src1_type(CPUState *env, DisasContext *dc, SRC1_TYPE src1_type, int src1_size, struct auto_increment *s1_inc)
{
        switch (src1_type) {

                case SRC1_GENERAL:

                        /* Set up src1 address and get it range checked. */

                        gen_src1_address(dc, dc->opcode, src1_size, s1_inc);
                        gen_address_check(dc, src1_size);

                        /* Move src1 into src1_data in env. */

                        gen_src1(dc, dc->opcode, src1_size);

                        break;

                default:

                        printf("gen_src1_type(): unknown src1_type %d\n", src1_type);
                        abort();
        }
}

static void gen_dsp_src2_type(CPUState *env, DisasContext *dc, SRC2_TYPE src2_type, int src2_size)
{
	int src2_reg_index;
	uint32_t opcode;

	opcode = dc->opcode;

	switch (src2_type) {

		case SRC2_DSP_IMM_OR_DN_OR_ACC:

			if (opcode & BIT_26) {

				/* Source2 is a Dn or accumulator */

				if (opcode & DSP_S_BIT)
					src2_reg_index = ((opcode >> 11) & 1) ? REGISTER_ACC1_HI : REGISTER_ACC0_HI;
				else
					src2_reg_index = (opcode >> 11) & 0xf;

//				printf("source2 register: 0x%x\n", src2_reg_index);

				gen_src2_64_direct(src2_reg_index);
				break;

			} else {

				/* Source2 is an immediate operand */

				tcg_gen_mov_i64(src2_64, tcg_const_i64((opcode >> 11) & 0x1f));
				break;
			}

			break;

		default:

			printf("gen_dsp_src2_type(): unknown src2_type %d\n", src2_type);
			abort();
        }

	/* Apparently the hardware allows setting the DSP_T_BIT on immediate src2 operands,
	   which is pointless, but required to pass verification tests. */

	if (opcode & DSP_T_BIT)
		tcg_gen_shri_i64(src2_64, src2_64, 16);
}

static void decode_insn_mop6 (CPUState *env, DisasContext *dc)
{
	uint32_t opcode = dc->opcode;
	uint32_t opcode_extension = (dc->opcode >> 21) & 0x1f;
	struct auto_increment s1_inc;

	SRC1_TYPE src1_type;
	SRC2_TYPE src2_type;
	int src1_size, src2_size;

	/*
	** All instructions in major opcode group 6 use instruction format 10b.
	**
	** source1: 11 bits, general operand
	** source2: 4 bits, either imm, Dn or acc depending on various bits
	*/

	src1_type = SRC1_GENERAL;
	src2_type = SRC2_DSP_IMM_OR_DN_OR_ACC;

	switch (opcode_extension) {

		case 0x0:	/* MULS */
		case 0x1:	/* MACS */
		case 0x2:	/* MULU */
		case 0x3:	/* MACU */
		case 0x4:	/* MULF */
		case 0x5:	/* MACF */
		case 0x7:	/* MACUS */
		case 0x9:	/* MSUF */
		case 0x11:	/* MADD.2 */
		case 0x13:	/* MSUB.2 */

			src1_size = 2;
			break;

		case 0x6:	/* MACS.4 */
		case 0x8:	/* MULS.4 */
		case 0xa:	/* MULU.4 */
		case 0x0b:	/* MACU.4 */
		case 0x10:	/* MADD.4 */
		case 0x12:	/* MSUB.4 */

			src1_size = 4;
			break;

		default:

			/* This an illegal instruction. Generate the illegal instruction trap.  */
			generate_trap(env, 1 << dc->env->cpu_index, 1 << TRAP_CAUSE_ILLEGAL_INST);
			dc->is_jmp = DISAS_UPDATE;
			return;
	}

//	printf("src1_type: %d, src1_size: %d\n", src1_type, src1_size);
//	printf("src2_type: %d, src2_size: %d\n", src2_type, src2_size);

	if (!src1_type || !src2_type) {
		printf("Error: unknown insn format for opcode 0x%08x\n", opcode);
		abort();
	}

	gen_src1_type(env, dc, src1_type, src1_size, &s1_inc);
	gen_dsp_src2_type(env, dc, src2_type, src2_size);

//	printf("major op: 6, opcode extension: %d\n", opcode_extension);

	switch(opcode_extension) {

		case 0x0:	/* MULS */

			gen_helper_muls(cpu_env, tcg_const_i32(opcode));
			break;

		case 0x1:	/* MACS */

			gen_helper_macs(cpu_env, tcg_const_i32(opcode));
			break;

		case 0x2:	/* MULU */

			gen_helper_mulu(cpu_env, tcg_const_i32(opcode));
			break;

		case 0x3:	/* MACU */

			gen_helper_macu(cpu_env, tcg_const_i32(opcode));
			break;

		case 0x4:	/* MULF */

			gen_helper_mulf(cpu_env, tcg_const_i32(opcode));
			break;

		case 0x5:	/* MACF */

			gen_helper_macf(cpu_env, tcg_const_i32(opcode));
			break;

		case 0x6:	/* MACS.4 */

			gen_helper_macs4(cpu_env, tcg_const_i32(opcode));
			break;

		case 0x7:	/* MACUS */

			gen_helper_macus(cpu_env, tcg_const_i32(opcode));
			break;

		case 0x8:	/* MULS.4 */

			gen_helper_muls4(cpu_env, tcg_const_i32(opcode));
			break;

		case 0x9:	/* MSUF */

			gen_helper_msuf(cpu_env, tcg_const_i32(opcode));
			break;

		case 0xa:	/* MULU.4 */

			gen_helper_mulu4(cpu_env, tcg_const_i32(opcode));
			break;

		case 0xb:	/* MACU.4 */

			gen_helper_macu4(cpu_env, tcg_const_i32(opcode));
			break;

		case 0x10:	/* MADD.4 */

			gen_helper_madd4(cpu_env, tcg_const_i32(opcode));
			break;

		case 0x11:	/* MADD.2 */

			gen_helper_madd2(cpu_env, tcg_const_i32(opcode));
			break;

		case 0x12:	/* MSUB.4 */

			gen_helper_msub4(cpu_env, tcg_const_i32(opcode));
			break;

		case 0x13:	/* MSUB.2 */

			gen_helper_msub2(cpu_env, tcg_const_i32(opcode));
			break;

		default:

			/* This an illegal instruction. Generate the illegal instruction trap.  */

			generate_trap(env, 1 << dc->env->cpu_index, 1 << TRAP_CAUSE_ILLEGAL_INST);
			dc->is_jmp = DISAS_UPDATE;
			return;
	}

	/* If the destination register was ACC0, then update MAC_RC16 */

	if (!(opcode & DSP_A_BIT))
		gen_helper_finish_multiply(cpu_env);

	/* Generate the post increment.  */

	if (src1_type == SRC1_GENERAL)
		gen_store_auto_inc(dc, &s1_inc);

	/* Move PC to Previous PC and increment PC by 4. */

	tcg_gen_movi_i32(ubicom32_regs.thread_regs[REGISTER_PREVIOUS_PC], dc->pc);
}

static void decode_insn_mop2 (CPUState *env, DisasContext *dc)
{
    /*
     * The major opcode for this entire group is 2. Extract the extension from bits 21-26..
     */
    uint32_t opcode_extension = (dc->opcode >> 21) & 0x1f;
    uint32_t dest_reg_index = (dc->opcode >> 16) & 0xf;
    uint32_t dn_imm = (dc->opcode >> 11) & 0x1f;
    uint32_t bit_26 = dc->opcode & 0x4000000;
    uint32_t opsize;
    struct auto_increment s1_inc;

    switch(opcode_extension) {
    case 0x6:
	{
	    /*
	     * BTST: 5 Major opcode:1 immediate/Dn select: 5 op ext: 5 0: 5 #bit_num/Dn: 11 s1.
	     * BTST destination is the Z bit in the condition code register.
	     * Setup src1 and get it range checked.
	     */

	    opsize = 4;
	    gen_src1_address(dc, dc->opcode, opsize, &s1_inc);
	    gen_address_check(dc, opsize);

	    /*
	     * Move src1 into src1_data in env.
	     */
	    gen_src1(dc, dc->opcode, opsize);

	    if (bit_26) {
		/*
		 * The bit number is in a Dn register. tmp = dn & 0x1f.
		 */
		tcg_gen_andi_i32(bset_clr_tmp, ubicom32_regs.thread_regs[dn_imm], 0x1f);

		/*
		 * Mask = 1 << tmp.
		 */
		tcg_gen_shl_i32(bset_clr_tmp, tcg_const_i32(1), bset_clr_tmp);
	    } else {
		/*
		 * Bit # is an immediate value. Mask = 1 << immediate value.
		 */
		tcg_gen_shli_i32(bset_clr_tmp, tcg_const_i32(1), dn_imm);
	    }

	    gen_helper_btst_clr_set(cpu_env, tcg_const_i32(0), tcg_const_i32(dc->opcode));

	    /*
	     * Generate the post increment.
	     */
	    gen_store_auto_inc(dc, &s1_inc);
	    break;
	}
    case 0x8:
	{
	    /*
	     * CRCGEN: 5 Major opcode:1 immediate/Dn select: 5 op ext: 5 0: 5 #polynomial/Dn: 11 s1.
	     * CRCGEN destination is the ACC0_LO and ACC_HI registers.
	     * Setup src1 and get it range checked.
	     */
	    opsize = 1;
	    gen_src1_address(dc, dc->opcode, opsize, &s1_inc);
	    gen_address_check(dc, opsize);

	    /*
	     * Move src1 into src1_data in env.
	     */
	    gen_src1(dc, dc->opcode, opsize);

	    if (bit_26) {
		/*
		 * The polynomial is in a Dn register.
		 */
		tcg_gen_mov_i32(src2_data, ubicom32_regs.thread_regs[dn_imm]);
	    } else {
		/*
		 * Polynomial is zero extented immediate value
		 */
		tcg_gen_movi_i32(src2_data, dn_imm);
	    }

	    /*
	     * Call the crcgen helper to get it executed.
	     */
	    gen_helper_crcgen(cpu_env);

	    /*
	     * Generate the post increment.
	     */
	    gen_store_auto_inc(dc, &s1_inc);
	    break;
	}
    case 0xa:
    case 0x11:
    case 0x10:
    case 0xb:
    case 0x13:
    case 0x12:
	{
	    /*
	     * LSL/LSR: 5 Major opcode:1 immediate/Dn select: 5 op ext: 1 0: 4 Destination data register select: 5 #shift/Dn: 11 s1.
	     * LSL.1 OP_EXT = 0xa
	     * LSL.2 OP_EXT = 0x11
	     * LSL.4 OP_EXT = 0x10
	     * LSR.1 OP_EXT = 0xb
	     * LSR.2 OP_EXT = 0x13
	     * LSR.4 OP_EXT = 0x12
	     */
	    uint32_t src1_truncate_mask = 0xffffffff;

	    /*
	     * Setup opsize
	     */
	    if ((opcode_extension == 0xa) || (opcode_extension == 0xb)) {
		opsize = 1;
		src1_truncate_mask = 0xff;
	    } else if ((opcode_extension == 0x11) || (opcode_extension == 0x13)) {
		opsize = 2;
		src1_truncate_mask = 0xffff;
	    }  else {
		opsize = 4;
	    }

	    /*
	     * Setup src1 and get it range checked.
	     */
	    gen_src1_address(dc, dc->opcode, opsize, &s1_inc);
	    gen_address_check(dc, opsize);

	    /*
	     * Move src1 into src1_data in env and truncate it.
	     */
	    gen_src1_general(dc, dc->opcode, opsize, 0);
//	    tcg_gen_andi_i32(src1_data, src1_data, src1_truncate_mask);

	    if (bit_26) {
		/*
		 * The bit shift amount is in a Dn register. bset_clr_tmp = dn & 0x1f.
		 */
		tcg_gen_andi_i32(bset_clr_tmp, ubicom32_regs.thread_regs[dn_imm], 0x1f);
	    } else {
		/*
		 * The bit shift amount in the immediate value.
		 */
		tcg_gen_movi_i32(bset_clr_tmp, dn_imm);
	    }

	    if ((opcode_extension == 0xa) || (opcode_extension == 0x11) || (opcode_extension == 0x10)) {
		/*
		 * dest_data = src1_data << shift.
		 */

		tcg_gen_shl_i32(dest_data, src1_data, bset_clr_tmp);

	    } else {
		/*
		 * dest_data = src1_data >> shift.
		 */
#if 0
                tcg_gen_mov_i32(dest_data, src1_data);
#else
		tcg_gen_shr_i32(dest_data, src1_data, bset_clr_tmp);
#endif
	    }

	    /*
	     * Run the NZ flag settings via a helper.
	     */
	    gen_helper_nzflag(cpu_env, tcg_const_i32(opsize));

	    /*
	     * Move the dest_data to final destination.
	     */
	    gen_helper_dest_direct(cpu_env, tcg_const_i32(dest_reg_index));

	    /*
	     * Generate the post increments.
	     */
	    gen_store_auto_inc(dc, &s1_inc);
	    break;
	}
    case 0x16:
	{
	    /*
	     * BFEXTU: 5 Major opcode:1 immediate/Dn select: 5 op ext: 1 0: 4 Destination data register select: 5 #shift/Dn: 11 s1.
	     *
	     * Setup opsize
	     */
	    opsize = 4;

	    /*
	     * Setup src1 and get it range checked.
	     */
	    gen_src1_address(dc, dc->opcode, opsize, &s1_inc);
	    gen_address_check(dc, opsize);

	    /*
	     * Move src1 into src1_data in env.
	     */
	    gen_src1(dc, dc->opcode, opsize);

//            printf("BFEXTU: opcode: %08x, src1_data: %08x\n", dc->opcode, src1_data);

	    if (bit_26) {
		/*
		 * Bit field start position is in 5 bits starting at bit 8 in the Dn regester.
		 * To extract it bset_clr_tmp = (dn >> 8) & 0x1f
		 */
		tcg_gen_shri_i32(bset_clr_tmp, ubicom32_regs.thread_regs[dn_imm], 8);
		tcg_gen_andi_i32(bset_clr_tmp, bset_clr_tmp, 0x1f);

		/*
		 * src1_data = src1_data >> bset_clr_tmp. This will trim the right side of the data.
		 */
		tcg_gen_shr_i32(src1_data, src1_data, bset_clr_tmp);

		/*
		 * The field width is in dn bits 0-4. bset_clr_tmp = dn & 0x1f.
		 */
		tcg_gen_andi_i32(bset_clr_tmp, ubicom32_regs.thread_regs[dn_imm], 0x1f);

	    } else {
		/*
		 * For immediate# cases the start for the field is at position 0. Right of src1_data is already in a trimmed state.
		 * The immediate value specifies the width of the field. Store it in bset_clr_tmp.
		 * The bit shift amount in the immediate value.
		 */
		tcg_gen_movi_i32(bset_clr_tmp, dn_imm);
	    }

#if 0
            /* This code fails for field widths of 0 because it generates a shift left of 32-bits and shift right of 32 bits.
               Unfortunately, shift counts of >31 bits are undefined on many architectures, including Intel 80x86.
               For 80x86, it becomes a NOP. - TM */

	    /*
	     * Calculate 32 - field width. This gives you the number of leading zeros the final result will have.
	     * bset_clr_tmp = 32 - bset_clr_tmp.
	     */
	    tcg_gen_sub_i32(bset_clr_tmp, tcg_const_i32(32), bset_clr_tmp);

	    /*
	     * dest_data = src1_data << bset_clr_tmp.
	     * dest_data = dest_data >> bset_clr_tmp
	     */
	    tcg_gen_shl_i32(dest_data, src1_data, bset_clr_tmp);
	    tcg_gen_shr_i32(dest_data, dest_data, bset_clr_tmp);
#else
            tcg_gen_movi_i32(src2_data, 1);
            tcg_gen_shl_i32(src2_data, src2_data, bset_clr_tmp);
            tcg_gen_subi_i32(src2_data, src2_data, 1);
            tcg_gen_and_i32(dest_data, src1_data, src2_data);
#endif

	    /*
	     * Run the NZ flag settings via a helper.
	     */
	    gen_helper_nzflag(cpu_env, tcg_const_i32(opsize));

	    /*
	     * Move the dest_data to final destination.
	     */
	    gen_helper_dest_direct(cpu_env, tcg_const_i32(dest_reg_index));

	    /*
	     * Generate the post increments.
	     */
	    gen_store_auto_inc(dc, &s1_inc);
	    break;
	}
    case 0x18:
	{
	    /*
	     * BFRVRS: 5 Major opcode:1 immediate/Dn select: 5 op ext: 1 0: 4 Destination data register select: 5 #shift/Dn: 11 s1.
	     *
	     * Setup opsize
	     */
	    opsize = 4;

	    /*
	     * Setup src1 and get it range checked.
	     */
	    gen_src1_address(dc, dc->opcode, opsize, &s1_inc);
	    gen_address_check(dc, opsize);

	    /*
	     * Move src1 into src1_data in env.
	     */
	    gen_src1(dc, dc->opcode, opsize);

	    if (bit_26) {
		/*
		 * The bit shift amount is in a Dn register. bset_clr_tmp = dn & 0x1f.
		 */
		gen_helper_bfrvrs(cpu_env, src1_data, ubicom32_regs.thread_regs[dn_imm]);
	    } else {
		/*
		 * The bit shift amount in the immediate value.
		 */
		gen_helper_bfrvrs(cpu_env, src1_data, tcg_const_i32(dn_imm));
	    }

	    /*
	     * Move the dest_data to final destination. The NZ calculation happens in the helper routine.
	     */
	    gen_helper_dest_direct(cpu_env, tcg_const_i32(dest_reg_index));

	    /*
	     * Generate the post increments.
	     */
	    gen_store_auto_inc(dc, &s1_inc);
	    break;
	}
    case 0x1a:
	{
	    /*
	     * SHFTD: 5 Major opcode:1 immediate/Dn select: 5 op ext: 1 0: 4 Destination data register select: 5 #shift/Dn: 11 s1.
	     *
	     * Setup opsize
	     */
	    opsize = 4;

	    /*
	     * Setup src1 and get it range checked.
	     */
	    gen_src1_address(dc, dc->opcode, opsize, &s1_inc);
	    gen_address_check(dc, opsize);

	    /*
	     * Move src1 into src1_data in env.
	     */
	    gen_src1(dc, dc->opcode, opsize);

	    if (bit_26) {
		/*
		 * The bit shift amount is in a Dn register. bset_clr_tmp = dn & 0x1f.
		 */
		tcg_gen_andi_i32(bset_clr_tmp, ubicom32_regs.thread_regs[dn_imm], 0x1f);
	    } else {
		/*
		 * The bit shift amount in the immediate value.
		 */
		tcg_gen_movi_i32(bset_clr_tmp, dn_imm);
	    }

#if 0
	    /*
	     * dest_data = src1_data >> shift.
	     */
	    tcg_gen_shr_i32(dest_data, src1_data, bset_clr_tmp);

	    /*
	     * bset_clr_tmp = 32 - bset_clr_tmp.
	     */
	    tcg_gen_sub_i32(bset_clr_tmp, tcg_const_i32(32), bset_clr_tmp);

	    /*
	     * src2_data = srouce3 << bset_clr_tmp
	     */
	    tcg_gen_shl_i32(src2_data, ubicom32_regs.thread_regs[0x2b], bset_clr_tmp);

	    /*
	     * dst_data = dst_data | src2_data.
	     */
	    tcg_gen_or_i32(dest_data, dest_data, src2_data);
#else
            /* data_data = (uint32_t)((((uint64_t)source3 << 32) | ((uint64_t)source1)) >> bset_clr_tmp); */

            tcg_gen_ext_i32_i64(dest_64, ubicom32_regs.thread_regs[0x2b]);
            tcg_gen_shli_i64(dest_64, dest_64, 32);
            tcg_gen_ext_i32_i64(src1_64, src1_data);
            tcg_gen_andi_i64(src1_64, src1_64, 0xffffffff);	/* tcg_gen_ext_i32_i64 apparently sign extends...grrr */
            tcg_gen_or_i64(dest_64, dest_64, src1_64);
            tcg_gen_ext_i32_i64(src2_64, bset_clr_tmp);
            tcg_gen_shr_i64(dest_64, dest_64, src2_64);
            tcg_gen_trunc_i64_i32(dest_data, dest_64);
#endif

	    /*
	     * Run the NZ flag settings via a helper.
	     */
	    gen_helper_nzflag(cpu_env, tcg_const_i32(opsize));

	    /*
	     * Move the dest_data to final destination.
	     */
	    gen_helper_dest_direct(cpu_env, tcg_const_i32(dest_reg_index));

	    /*
	     * Generate the post increments.
	     */
	    gen_store_auto_inc(dc, &s1_inc);
	    break;
	}
    case 0xc:
    case 0x14:
    case 0x15:
	{
	    /*
	     * ASR: 5 Major opcode:1 immediate/Dn select: 5 op ext: 1 0: 4 Destination data register select: 5 #shift/Dn: 11 s1.
	     * ASR.1 OP_EXT = 0xc
	     * ASR.2 OP_EXT = 0x15
	     * ASR.4 OP_EXT = 0x14
	     *
	     * Setup opsize
	     */
	    if (opcode_extension == 0xc)
		opsize = 1;
	    else if (opcode_extension == 0x15)
		opsize = 2;
	    else
		opsize = 4;

	    /*
	     * Setup src1 and get it range checked.
	     */
	    gen_src1_address(dc, dc->opcode, opsize, &s1_inc);
	    gen_address_check(dc, opsize);

	    /*
	     * Move src1 into src1_data in env.
	     */
	    gen_src1(dc, dc->opcode, opsize);

	    if (bit_26) {
		/*
		 * The bit shift amount is in a Dn register. bset_clr_tmp = dn & 0x1f.
		 */
		tcg_gen_andi_i32(bset_clr_tmp, ubicom32_regs.thread_regs[dn_imm], 0x1f);
	    } else {
		/*
		 * The bit shift amount in the immediate value.
		 */
		tcg_gen_movi_i32(bset_clr_tmp, dn_imm);
	    }

	    /*
	     * dest_data = src1_data >> shift.
	     */
	    tcg_gen_sar_i32(dest_data, src1_data, bset_clr_tmp);

	    /*
	     * Run the NZ flag settings via a helper.
	     */
	    gen_helper_nzflag(cpu_env, tcg_const_i32(opsize));

	    /*
	     * Move the dest_data to final destination.
	     */
	    gen_helper_dest_direct(cpu_env, tcg_const_i32(dest_reg_index));

	    /*
	     * Generate the post increments.
	     */
	    gen_store_auto_inc(dc, &s1_inc);
	    break;
	}
    case 0x1c:
	{
	    /*
	     * MERGE: 5 Major opcode:1 immediate/Dn select: 5 op ext: 1 0: 4 Destination data register select: 5 #imm/Dn: 11 s1.
	     * Source 3 register has the mask.
	     *
	     * Setup opsize
	     */
	    opsize = 4;

	    /*
	     * Setup src1 and get it range checked.
	     */
	    gen_src1_address(dc, dc->opcode, opsize, &s1_inc);
	    gen_address_check(dc, opsize);

	    /*
	     * Move src1 into src1_data in env.
	     */
	    gen_src1(dc, dc->opcode, opsize);

	    /*
	     * src2_data = src1_data & source3
	     */
	    tcg_gen_and_i32(src2_data, src1_data, ubicom32_regs.thread_regs[0x2b]);

	    /*
	     * dest_data = !source3
	     */
	    tcg_gen_not_i32(dest_data, ubicom32_regs.thread_regs[0x2b]);

	    if (bit_26) {
		/*
		 * The merge data is in a Dn register. dest_data = dn & dest_data
		 */
		tcg_gen_and_i32(dest_data, ubicom32_regs.thread_regs[dn_imm], dest_data);
	    } else {
		/*
		 * XXXXXXXXXX Does the data have to be sign extended?????
		 * The merge data is the immediate vaue. dest_data = dest_data & dn_imm
		 */
		tcg_gen_andi_i32(dest_data, dest_data, dn_imm );
	    }

	    /*
	     * dst_data = dst_data | src2_data.
	     */
	    tcg_gen_or_i32(dest_data, dest_data, src2_data);

	    /*
	     * Run the NZ flag settings via a helper.
	     */
	    gen_helper_nzflag(cpu_env, tcg_const_i32(opsize));

	    /*
	     * Move the dest_data to final destination.
	     */
	    gen_helper_dest_direct(cpu_env, tcg_const_i32(dest_reg_index));

	    /*
	     * Generate the post increments.
	     */
	    gen_store_auto_inc(dc, &s1_inc);
	    break;
	}
    case 0x1e:
    case 0x1f:
	{
	    /*
	     * SHMRG: 5 Major opcode:1 immediate/Dn select: 5 op ext: 1 0: 4 Destination data register select: 5 #imm/Dn: 11 s1.
	     * SHMRG.1 OP_EXT = 0x1f
	     * SHMRG.2 OP_EXT = 0x1e
	     */
	    uint32_t src1_truncate_mask;
	    uint32_t merge_shift;

	    /*
	     * Setup opsize
	     */
	    if (opcode_extension == 0x1f) {
		opsize = 1;
		src1_truncate_mask = 0xff;
		merge_shift = 8;
	    } else {
		opsize = 2;
		src1_truncate_mask = 0xffff;
		merge_shift = 16;
	    }

	    /*
	     * Setup src1 and get it range checked.
	     */
	    gen_src1_address(dc, dc->opcode, opsize, &s1_inc);
	    gen_address_check(dc, opsize);

	    /*
	     * Move src1 into src1_data in env.
	     */
	    gen_src1(dc, dc->opcode, opsize);

	    /*
	     * src2_data = src1_data & src1_truncate_mask
	     */
	    tcg_gen_andi_i32(src2_data, src1_data, src1_truncate_mask);

	    if (bit_26) {
		/*
		 * The merge data is in a Dn register. dest_data = dn << merge_shift
		 */
		tcg_gen_shli_i32(dest_data, ubicom32_regs.thread_regs[dn_imm], merge_shift);
	    } else {
		/*
		 * XXXXXXXXXX Does the data have to be sign extended?????
		 * The merge data is the immediate vaue. dest_data = dn_imm << merge_shift.
		 */
		tcg_gen_shli_i32(dest_data, tcg_const_i32(dn_imm), merge_shift);
	    }

	    /*
	     * dst_data = dst_data | src2_data.
	     */
	    tcg_gen_or_i32(dest_data, dest_data, src2_data);

	    /*
	     * Run the NZ flag settings via a helper.
             * SHMRG sets NZ flags based on result size, not operand size so multiply size by two.
	     */
	    gen_helper_nzflag(cpu_env, tcg_const_i32(opsize * 2));

	    /*
	     * Move the dest_data to final destination.
	     */
	    gen_helper_dest_direct(cpu_env, tcg_const_i32(dest_reg_index));

	    /*
	     * Generate the post increments.
	     */
	    gen_store_auto_inc(dc, &s1_inc);
	    break;
	}

    case 0x00:	/* PXHI */
    case 0x02:	/* PXHI.S */
	{
		int bit_22 = (dc->opcode >> 22) & 1;

//		printf("PXHI instruction\n");

		/* Setup src1 and get it range checked */

		gen_src1_address(dc, dc->opcode, 4, &s1_inc);

		/* Move src1 into env->src1_data, and set up src2_data */

		gen_src1(dc, dc->opcode, 4);
		tcg_gen_mov_i32(src2_data, ubicom32_regs.thread_regs[(dc->opcode >> 11) & 0xf]);

		/* Generate a call to the pxhi helper */

		gen_helper_pxhi(cpu_env, tcg_const_i32(bit_22));

		/* Move the dest_data to final destination */

		gen_helper_dest_direct(cpu_env, tcg_const_i32(dest_reg_index));

		/* Generate the post increments */

		gen_store_auto_inc(dc, &s1_inc);
		break;
	}

    default:
	/*
	 * This an illegal instruction. Generate the illegal instruction trap.
	 */
	generate_trap(env, 1 << dc->env->cpu_index, 1 << TRAP_CAUSE_ILLEGAL_INST);
	dc->is_jmp = DISAS_UPDATE;
    }

    /*
     * Move previous PC to Previous PC and set PC to PC + 4.
     */
    tcg_gen_movi_i32(ubicom32_regs.thread_regs[0x38], dc->pc);
}

static void decode_insn_mop0 (CPUState *env, DisasContext *dc)
{
    /*
     * The major opcode for this entire group is 0. Extract the extension.
     */
    uint32_t opcode_extension = (dc->opcode >> 11) & 0x1f;
    uint32_t opsize;
    struct auto_increment s1_inc, dest_inc;

    switch(opcode_extension) {
    case 0x1:
	{
	    /*
	     * SUSPEND: Call the suspend helper routine to do its thing.
	     */
	    if (env->def->chip_id == UBICOM32_8K_CHIP_ID) {
		gen_helper_iprivilege_check(cpu_env);
	    }

	    gen_helper_suspend(cpu_env);

	    /*
	     * Load the address in src1_address and exit the block.
	     *
	     * Move previous PC to Previous PC and set PC to PC + 4.
	     */
	    tcg_gen_movi_i32(ubicom32_regs.thread_regs[0x38], dc->pc);
	    tcg_gen_mov_i32(ubicom32_regs.thread_regs[0x34], tcg_const_i32(dc->pc+4));
	    dc->is_jmp = DISAS_JUMP;
	    gen_helper_raise_exception(tcg_const_i32(EXCP_INTERRUPT));

	    break;
	}
    case 0x2:
    case 0x3:
    case 0x5:
    case 0x11:
    case 0x16:
	{
	    /*
	     * SYNC/FLUSH/FLUSHNI/INVAL/PREFETCH Encoding 5 Major op: 11 d:5 OP extension: 11 0
	     *
	     * Instruction OP size is 4.
	     *
	     * Setup dest and get it range checked.
	     */
	    opsize = 4;
	    gen_dest_address(dc, dc->opcode, opsize, &dest_inc);
	    gen_address_check(dc, opsize);

	    /*
	     * Generate the post increments.
	     */
	    gen_store_auto_inc(dc, &dest_inc);
	    break;
	}
    case 0x4:
	{
	    /*
	     * RET
	     * Encoding: 5 Major OPC :11 unused: 5 bit opcode extension: 11 bit s1
	     */
	    opsize = 4;

	    /*
	     * Setup src1 address.That is the return address.
	     */
	    gen_src1_address(dc, dc->opcode, opsize, &s1_inc);
	    gen_address_check(dc, opsize);

	    /*
	     * Move the data into src1_data;
	     */
	    gen_src1(dc, dc->opcode, opsize);

	    /*
	     * Load the address in src1_address and exit the block.
	     */
	    tcg_gen_movi_i32(ubicom32_regs.thread_regs[0x38], dc->pc);
	    tcg_gen_mov_i32(ubicom32_regs.thread_regs[0x34], src1_data);
	    dc->is_jmp = DISAS_JUMP;

	    /*
	     * Generate the post increment.
	     */
	    gen_store_auto_inc(dc, &s1_inc);
	    break;
	}
    case 0x7:
	{
	    /*
	     * BKPT
	     * Coding 5 bit Major OPC: 11 unused: 5 bit opcode extension: 11 bit s1
	     */
	    if (env->def->chip_id == UBICOM32_8K_CHIP_ID) {
		gen_helper_iprivilege_check(cpu_env);
	    }

	    opsize = 4;

	    /*
	     * Setup src1 address. Get it range checked.
	     */
	    gen_src1_address(dc, dc->opcode, opsize, &s1_inc);
	    gen_address_check(dc, opsize);

	    /*
	     * Move the data into src1_data;
	     */
	    gen_src1(dc, dc->opcode, opsize);

	    /*
	     * Generate the post increment.
	     */
	    gen_store_auto_inc(dc, &s1_inc);

	    /*
	     * Call the bkpt helper routine to do its thing.
	     */
	    gen_helper_bkpt(cpu_env);

	    /*
	     * Force the pc to the same instruction and kill this block.
	     */
	    tcg_gen_movi_i32(ubicom32_regs.thread_regs[0x38], dc->pc);
	    tcg_gen_mov_i32(ubicom32_regs.thread_regs[0x34], tcg_const_i32(dc->pc));
	    dc->is_jmp = DISAS_JUMP;
	    gen_helper_raise_exception(tcg_const_i32(EXCP_INTERRUPT));
	    break;
	}
    case 0x8:
	{
	    /*
	     * SYSRET: Encoding 5 Major opcode, : 11 d:5 OP extension: 11 s1
	     */
	    if (env->def->chip_id == UBICOM32_8K_CHIP_ID) {
		gen_helper_iprivilege_check(cpu_env);
	    }

	    opsize = 4;

	    /*
	     * Setup src1 and get it range checked.
	     */
	    gen_src1_address(dc, dc->opcode, opsize, &s1_inc);

	    /*
	     * Setup dest and get it range checked.
	     */
	    gen_dest_address(dc, dc->opcode, opsize, &dest_inc);
	    gen_address_check(dc, opsize);

	    /*
	     * Move src1 into src1_data in env. src1_data contains the next PC.
	     */
	    gen_src1(dc, dc->opcode, opsize);

	    /*
	     * Extract the PREV privilege bit from CSR into dest_data and shift it into Previlege bit position.
	     */
	    tcg_gen_andi_i32(dest_data, ubicom32_regs.thread_regs[0x2d], (1 << CSR_PREV_PRIV_BIT));
	    tcg_gen_shri_i32(dest_data, dest_data, 1);

	    /*
	     * Drive the PRIV bits to zero in CSR.
	     */
	    tcg_gen_andi_i32(ubicom32_regs.thread_regs[0x2d], ubicom32_regs.thread_regs[0x2d], ~CSR_PRIV_MASK);

	    /*
	     * Or in the privilege bit into CSR.
	     */
	    tcg_gen_or_i32(ubicom32_regs.thread_regs[0x2d], ubicom32_regs.thread_regs[0x2d], dest_data);

	    /*
	     * Store src1_data to PC register.
	     */
	    tcg_gen_mov_i32(ubicom32_regs.thread_regs[0x34], src1_data);

	    /*
	     * Set dest_data to 0
	     */
	    tcg_gen_movi_i32(dest_data, 0);

	    /*
	     * Move the dest_data to final destination.
	     */
	    gen_dest(dc, dc->opcode, opsize);

	    /*
	     * Generate the post increments.
	     */
	    gen_store_auto_inc(dc, &s1_inc);
	    gen_store_auto_inc(dc, &dest_inc);

	    /*
	     * Exit out of this block.
	     */
	    tcg_gen_movi_i32(ubicom32_regs.thread_regs[0x38], dc->pc);
	    dc->is_jmp = DISAS_JUMP;
	    break;
	}
    case 0x9:
	{
	    /*
	     * SYSCALL: Encoding 5 Major opcode, : 11 d:5 OP extension: 11 0
	     */
	    uint32_t return_address = dc->pc + 4;
	    opsize = 4;

	    /*
	     * Set the PC from the SEP register by calling helper. The helper will also
	     * set the privilege flags properly.
	     */
	    gen_helper_syscall(cpu_env);
	    tcg_gen_movi_i32(ubicom32_regs.thread_regs[0x38], dc->pc);

	    /*
	     * Generate the destination address and check it.
	     */
	    gen_dest_address(dc, dc->opcode, opsize, &dest_inc);
	    gen_address_check(dc, opsize);

	    /*
	     * Move return address into dest_data.
	     */
	    tcg_gen_movi_i32(dest_data, return_address);

	    /*
	     * Move the dest_data to final destination.
	     */
	    gen_dest(dc, dc->opcode, opsize);

	    /*
	     * Generate the post increments.
	     */
	    gen_store_auto_inc(dc, &dest_inc);

            /*
             * Exit this block.
             */
	    dc->is_jmp = DISAS_JUMP;
	    break;
	}
    case 0xa:
    case 0xb:
	{
	    /*
	     * NOT.4 NOT.2
	     * Enoding 5 bit Major OPC: 11 d: 5 bit opcode extension: 11 bit s1
	     * NOT.4 = 0xa  NOT.2 = 0xb
	     */
	    if (opcode_extension == 0xb)
		opsize = 2;
	    else
		opsize = 4;

	    /*
	     * Setup src1 and get it range checked.
	     */
	    gen_src1_address(dc, dc->opcode, opsize, &s1_inc);

	    /*
	     * Setup dest and get it range checked.
	     */
	    gen_dest_address(dc, dc->opcode, opsize, &dest_inc);
	    gen_address_check(dc, opsize);

	    /*
	     * Move src1 into src1_data in env.
	     */
	    gen_src1(dc, dc->opcode, opsize);

	    /*
	     * dest_data = ~src1_data
	     */
	    tcg_gen_not_i32(dest_data, src1_data);

	    /*
	     * Run the NZ flag settings via a helper.
	     */
	    gen_helper_nzflag(cpu_env, tcg_const_i32(opsize));

	    /*
	     * Move the dest_data to final destination.
	     */
	    gen_dest(dc, dc->opcode, opsize);

	    /*
	     * Generate the post increments.
	     */
	    gen_store_auto_inc(dc, &s1_inc);
	    gen_store_auto_inc(dc, &dest_inc);
	    break;
	}
    case 0xc:
    case 0xd:
    case 0xe:
    case 0xf:
	{
	    /*
	     * move.4 move.2 move.1 mova Group
	     * Enoding 5 bit Major OPC: 11 d: 5 bit opcode extension: 11 bit s1
	     */
	    uint32_t truncate_mask = 0xffffffff;
	    if (opcode_extension == 0xf) {
		opsize = 1;
		truncate_mask = 0x000000ff;
	    } else if (opcode_extension == 0xd) {
		opsize = 2;
		truncate_mask = 0x0000ffff;
	    } else {
		opsize = 4;
	    }

	    /*
	     * Setup src1 and get it range checked.
	     */
	    gen_src1_address(dc, dc->opcode, opsize, &s1_inc);

	    /*
	     * Setup dest and get it range checked.
	     */
	    gen_dest_address(dc, dc->opcode, opsize, &dest_inc);
	    gen_address_check(dc, opsize);

	    /*
	     * Move src1 into src1_data in env.
	     */
	    gen_src1(dc, dc->opcode, opsize);

	    /*
	     * Move src1_data into dest_data truncating as needed.
	     */
	    tcg_gen_andi_i32(dest_data, src1_data, truncate_mask);

	    /*
	     * Move the dest_data to final destination.
	     */
	    gen_dest(dc, dc->opcode, opsize);

	    /*
	     * Generate the post increments.
	     */
	    gen_store_auto_inc(dc, &s1_inc);
	    gen_store_auto_inc(dc, &dest_inc);
	    break;
	}
    case 0x12:
	{
	    /*
	     * SETCSR
	     * Coding 5 bit Major OPC: 11 unused: 5 bit opcode extension: 11 bit s1
	     */
	    opsize = 4;

	    /*
	     * Setup src1 address. Get it range checked.
	     */
	    gen_src1_address(dc, dc->opcode, opsize, &s1_inc);
	    gen_address_check(dc, opsize);

	    /*
	     * Move the data into src1_data;
	     */
	    gen_src1(dc, dc->opcode, opsize);

	    /*
	     * Store the data in src1_data into the CSR register.
	     */
	    tcg_gen_mov_i32(ubicom32_regs.thread_regs[0x2d], src1_data);

	    /*
	     * Generate the post increment.
	     */
	    gen_store_auto_inc(dc, &s1_inc);

	    break;
	}
    case 0x13:
    case 0x14:
	{
	    /*
	     * TBSET = 0x13
	     * TBCLR = 0x14
	     * Coding:  5 bits opcode: 11 bit dest: 5 bit opcode extension: 11 bit s1
	     */

	    /*
	     * Setup src1 and get it range checked.
	     */
	    opsize = 4;
	    gen_src1_address(dc, dc->opcode, opsize, &s1_inc);

	    /*
	     * Setup dest and get it range checked.
	     */
	    gen_dest_address(dc, dc->opcode, opsize, &dest_inc);
	    gen_address_check(dc, opsize);

	    /*
	     * Move src1 into src1_data in env.
	     */
	    gen_src1(dc, dc->opcode, opsize);

	    if (opcode_extension == 0x14) {
		/*
		 * TBCLR
		 */
		gen_helper_btst_clr_set(cpu_env, tcg_const_i32(3), tcg_const_i32(dc->opcode));
	    } else {
		/*
		 * TBSET
		 */
		gen_helper_btst_clr_set(cpu_env, tcg_const_i32(4), tcg_const_i32(dc->opcode));
	    }


	    /*
	     * Write back stage: Move the dest_data to final destination.
	     */
	    gen_dest(dc, dc->opcode, opsize);

	    /*
	     * Generate the post increments.
	     */
	    gen_store_auto_inc(dc, &s1_inc);
	    gen_store_auto_inc(dc, &dest_inc);
	    break;
	}
    case 0x15:
    case 0x17:
	{
	    /*
	     * EXT.1 0x17
	     * EXT.2 0x15
	     * Encoding 5 Major OPC: 11 d: 5 OP Extension: 11 s1
	     */
	    if (opcode_extension == 0x17)
		opsize = 1;
	    else
		opsize = 2;

	    /*
	     * Setup src1 and get it range checked.
	     */
	    gen_src1_address(dc, dc->opcode, opsize, &s1_inc);

	    /*
	     * Setup dest and get it range checked.
	     */
	    gen_dest_address(dc, dc->opcode, opsize, &dest_inc);
	    gen_address_check(dc, opsize);

	    /*
	     * Move src1 into src1_data in env.
	     */
	    gen_src1(dc, dc->opcode, opsize);

	    if (opsize == 1) {
		/*
		 * Take the bottom 8 bits and sign extend to 32.
		 */
		tcg_gen_ext8s_i32(dest_data, src1_data);
	    } else {
		/*
		 * Take the bottom 8 bits and sign extend to 32.
		 */
		tcg_gen_ext16s_i32(dest_data, src1_data);
	    }

	    /*
	     * Run the NZ flag settings via a helper.
	     */
	    gen_helper_nzflag(cpu_env, tcg_const_i32(opsize));

	    /*
	     * Move the dest_data to final destination.
	     */
	    gen_dest(dc, dc->opcode, opsize);

	    /*
	     * Generate the post increments.
	     */
	    gen_store_auto_inc(dc, &s1_inc);
	    gen_store_auto_inc(dc, &dest_inc);
	    break;
	}
    case 0x18:
    case 0x19:
	{
	    /*
	     * SWAPB.2 0x18
	     * SWAPB.4 0x19
	     * Encoding 5 Major OPC: 11 d: 5 OP Extension: 11 s1
	     */
	    if (opcode_extension == 0x18)
		opsize = 2;
	    else
		opsize = 4;

	    /*
	     * Setup src1 and get it range checked.
	     */
	    gen_src1_address(dc, dc->opcode, opsize, &s1_inc);

	    /*
	     * Setup dest and get it range checked.
	     */
	    gen_dest_address(dc, dc->opcode, opsize, &dest_inc);
	    gen_address_check(dc, opsize);

	    /*
	     * Move src1 into src1_data in env.
	     */
	    gen_src1(dc, dc->opcode, opsize);

	    /*
	     * Move src1_data into dest_data
	     */
	    if (opcode_extension == 0x18) {
		/*
		 * Need to drive the high bits to zero before we can do the swap.
		 */
		tcg_gen_andi_i32(src1_data, src1_data, 0x0000ffff);
		tcg_gen_bswap16_i32(dest_data, src1_data);
	    } else {
		tcg_gen_bswap32_i32(dest_data, src1_data);
	    }

	    /*
	     * Move the dest_data to final destination.
	     */
	    gen_dest(dc, dc->opcode, opsize);

	    /*
	     * Generate the post increments.
	     */
	    gen_store_auto_inc(dc, &s1_inc);
	    gen_store_auto_inc(dc, &dest_inc);
	    break;
	}

    case 0x1a:	/* PXCNV */
    case 0x1b:	/* PXCNV.T */
	{
//		printf("PXCNV instruction\n");

		/* Setup src1 and get it range checked */

		gen_src1_address(dc, dc->opcode, 4, &s1_inc);

		/* Setup dest and get it range checked */

		gen_dest_address(dc, dc->opcode, 2, &dest_inc);
		gen_address_check(dc, 2);

		/* Move src1 into env->src1_data, and set up src2_data */

		gen_src1(dc, dc->opcode, 4);
		tcg_gen_mov_i32(src2_data, ubicom32_regs.thread_regs[(dc->opcode >> 11) & 0xf]);

		/* Generate a call to the pxcnv helper */

		if (opcode_extension == 0x1b)
			gen_helper_pxcnv(cpu_env, tcg_const_i32(1));
		else
			gen_helper_pxcnv(cpu_env, tcg_const_i32(0));

		/* Move the dest_data to final destination */

		gen_dest(dc, dc->opcode, 2);

		/* Generate the post increments */

		gen_store_auto_inc(dc, &s1_inc);
		gen_store_auto_inc(dc, &dest_inc);
		break;
	}

    case 0x1c:
    case 0x1d:
    case 0x1f:
	{
	    /*
	     * lea.1 lea.2 lea.4 group
	     *
	     * lea the destination is always 32 bits.
	     */
	    dc->is_lea_pdec = 1;

	    if (opcode_extension == 0x1f)
		opsize = 1;
	    else if (opcode_extension == 0x1d)
		opsize = 2;
	    else
		opsize = 4;
	    /*
	     * Setup src1_address.
	     */
	    gen_src1_address(dc, dc->opcode, opsize, &s1_inc);

	    /*
	     * Setup dest and get it range checked.
	     */
	    opsize = 4;
	    gen_dest_address(dc, dc->opcode, opsize, &dest_inc);
	    gen_address_check(dc, opsize);

	    /*
	     * Move src1_address into dest_data
	     */
	    tcg_gen_mov_i32(dest_data, src1_address);

	    /*
	     * Move the dest_data to final destination.
	     */
	    gen_dest(dc, dc->opcode, opsize);

	    /*
	     * Generate the post increments.
	     */
	    gen_store_auto_inc(dc, &s1_inc);
	    gen_store_auto_inc(dc, &dest_inc);
	    break;
	}
    case 0x1e:
	{
	    /*
	     * PDEC
	     * Encoding 5 Major OP: 11 dest:5 op extension: 11 modified s1
	     */
	    uint32 offset = (dc->opcode & 0x1f) | ((dc->opcode >> 3) & 0x60);
	    uint32 areg_index = ((dc->opcode >> 5) & 0x7) + 0x20;

	    /*
	     * Convert to a byte offset and sign extend.
	     */
	    offset <<= 2;
	    offset |= 0xfffffe00;
	    opsize = 4;

	    /*
	     * Setup dest and get it range checked.
	     */
	    gen_dest_address(dc, dc->opcode, opsize, &dest_inc);
	    gen_address_check(dc, opsize);

	    /*
	     * Setup src1_address directly in dest_data.
	     */
	    tcg_gen_add_i32(dest_data, tcg_const_i32(offset), ubicom32_regs.thread_regs[areg_index]);

	    /*
	     * Move the dest_data to final destination.
	     */
	    gen_dest(dc, dc->opcode, opsize);

	    /*
	     * Generate the post increments.
	     */
	    gen_store_auto_inc(dc, &dest_inc);
	    break;
	}
    default:
	/*
	 * This an illegal instruction. Generate the illegal instruction trap.
	 */
	generate_trap(env, 1 << dc->env->cpu_index, 1 << TRAP_CAUSE_ILLEGAL_INST);
	dc->is_jmp = DISAS_UPDATE;
	return;
    }

    if (dc->is_jmp == DISAS_NEXT) {
	/*
	 * Move previous PC to Previous PC and set PC to PC + 4.
	 */
	tcg_gen_movi_i32(ubicom32_regs.thread_regs[0x38], dc->pc);
    }
}
static void decode_insn_mop3(CPUState *env, DisasContext *dc)
{
	uint32_t dest_reg_index = (dc->opcode >> 16) & 3;
	uint32_t src1_reg_index = (dc->opcode >>  1) & 0x1f;
	uint32_t src2_reg_index = (dc->opcode >> 12) & 0x1f;
	uint32_t opcode_extension = (dc->opcode >> 21) & 0x1f;
	struct auto_increment s1_inc;

	switch(opcode_extension) {

		case 0x00:	/* FADDS */
		case 0x01:	/* FSUBS */
		case 0x02:	/* FMULS */
		case 0x03:	/* FDIVS */

			/* Setup src1 and get it range checked */

			gen_src1_address(dc, dc->opcode, 4, &s1_inc);

			/* Move src1 into env->src1_data, and set up src2_data */

			gen_src1(dc, dc->opcode, 4);
			gen_src2_32_Dn_or_acc((dc->opcode >> 11) & 0x1f);

			/* Generate a call to the appropriate helper */

			if (opcode_extension == 0)
				gen_helper_fadds(cpu_env);
			else if (opcode_extension == 1)
				gen_helper_fsubs(cpu_env);
			else if (opcode_extension == 2)
				gen_helper_fmuls(cpu_env);
			else if (opcode_extension == 3)
				gen_helper_fdivs(cpu_env);

			/* Move the dest_data to final destination */

	   		 gen_helper_dest_fpd32(cpu_env, tcg_const_i32(dest_reg_index));

			/* Generate the post increments */

			gen_store_auto_inc(dc, &s1_inc);
			break;

		case 0x04:	/* FI2D */
		case 0x05:	/* FS2D */
		case 0x06:	/* FS2L */

			/* Setup src1 and get it range checked */

			gen_src1_address(dc, dc->opcode, 4, &s1_inc);

			/* Move src1 into env->src1_data */

			gen_src1(dc, dc->opcode, 4);

			/* Generate a call to the appropriate helper */

			if (opcode_extension == 4)
				gen_helper_fi2d(cpu_env);
			else if (opcode_extension == 5)
				gen_helper_fs2d(cpu_env);
			else if (opcode_extension == 6)
				gen_helper_fs2l(cpu_env);

			/* Move the dest_data to final destination */

	   		gen_helper_dest_fpd64(cpu_env, tcg_const_i32(dest_reg_index >> 1));

			/* Generate the post increments */

			gen_store_auto_inc(dc, &s1_inc);
			break;

		case 0x07:	/* FSQRTS */
		case 0x08:	/* FNEGS */
		case 0x09:	/* FABSS */
		case 0x0a:	/* FI2S */
		case 0x0b:	/* FS2I */

			/* Setup src1 and get it range checked */

			gen_src1_address(dc, dc->opcode, 4, &s1_inc);

			/* Move src1 into env->src1_data */

			gen_src1(dc, dc->opcode, 4);

			/* Generate a call to the appropriate helper */

			if (opcode_extension == 7)
				gen_helper_fsqrts(cpu_env);
			else if (opcode_extension == 8)
				gen_helper_fnegs(cpu_env);
			else if (opcode_extension == 9)
				gen_helper_fabss(cpu_env);
			else if (opcode_extension == 0x0a)
				gen_helper_fi2s(cpu_env);
			else if (opcode_extension == 0x0b)
				gen_helper_fs2i(cpu_env);

			/* Move the dest_data to final destination */

	   		gen_helper_dest_fpd32(cpu_env, tcg_const_i32(dest_reg_index));

			/* Generate the post increments */

			gen_store_auto_inc(dc, &s1_inc);
			break;

		case 0x0c:	/* FCMPS */

			/* Setup src1 and get it range checked */

			gen_src1_address(dc, dc->opcode, 4, &s1_inc);

			/* Move src1 into env->src1_data, and set up src2_data */

			gen_src1(dc, dc->opcode, 4);
			gen_src2_32_Dn_or_acc((dc->opcode >> 11) & 0x1f);

//			gen_helper_debug(cpu_env);

			/* Generate a call to the appropriate helper */

			gen_helper_fcmps(cpu_env);

			/* Generate the post increments */

			gen_store_auto_inc(dc, &s1_inc);
			break;

		case 0x10:	/* FADDD */
		case 0x11:	/* FSUBD */
		case 0x12:	/* FMULD */
		case 0x13:	/* FDIVD */

			/* Move src1 into env->src1_data, and set up src2_data */

//			printf("src1_reg_index: %d, src2_reg_index: %d\n", src1_reg_index, src2_reg_index);

			gen_helper_src1_64_direct(cpu_env, tcg_const_i32(src1_reg_index));
			gen_helper_src2_64_direct(cpu_env, tcg_const_i32(src2_reg_index));

			/* Generate a call to the appropriate helper */

			if (opcode_extension == 0x10)
				gen_helper_faddd(cpu_env);
			else if (opcode_extension == 0x11)
				gen_helper_fsubd(cpu_env);
			else if (opcode_extension == 0x12)
				gen_helper_fmuld(cpu_env);
			else if (opcode_extension == 0x13)
				gen_helper_fdivd(cpu_env);

			/* Move the dest_data to final destination */

			dest_reg_index = (dest_reg_index >> 1) & 1;

//			printf("generating store to index: %d\n", dest_reg_index);

	   		gen_helper_dest_fpd64(cpu_env, tcg_const_i32(dest_reg_index));

			/* No auto-increment addressing mode supported for this instruction format. */

			break;

		case 0x14:	/* FL2S */
		case 0x15:	/* FD2S */
		case 0x16:	/* FD2I */

			/* Move src1 into env->src1_data, and set up src2_data */

//			printf("src1_reg_index: %d, src2_reg_index: %d\n", src1_reg_index, src2_reg_index);

			gen_helper_src1_64_direct(cpu_env, tcg_const_i32(src1_reg_index));
			gen_helper_src2_64_direct(cpu_env, tcg_const_i32(src2_reg_index));

			/* Generate a call to the appropriate helper */

			if (opcode_extension == 0x14)
				gen_helper_fl2s(cpu_env);
			else if (opcode_extension == 0x15)
				gen_helper_fd2s(cpu_env);
			else if (opcode_extension == 0x16)
				gen_helper_fd2i(cpu_env);

			/* Move the dest_data to final destination */

//			printf("generating store to index: %d\n", dest_reg_index);

	   		gen_helper_dest_fpd32(cpu_env, tcg_const_i32(dest_reg_index));

			/* No auto-increment addressing mode supported for this instruction format. */

			break;

		case 0x17:	/* FSQRTD */
		case 0x18:	/* FNEGD */
		case 0x19:	/* FABSD */
		case 0x1a:	/* FL2D */
		case 0x1b:	/* FD2L */

			/* Move src1 into env->src1_64 and set up src2_data */

//			printf("src1_reg_index: %d, src2_reg_index: %d\n", src1_reg_index, src2_reg_index);

			gen_helper_src1_64_direct(cpu_env, tcg_const_i32(src1_reg_index));

			/* Generate a call to the appropriate helper */

			if (opcode_extension == 0x17)
				gen_helper_fsqrtd(cpu_env);
			else if (opcode_extension == 0x18)
				gen_helper_fnegd(cpu_env);
			else if (opcode_extension == 0x19)
				gen_helper_fabsd(cpu_env);
			else if (opcode_extension == 0x1a)
				gen_helper_fl2d(cpu_env);
			else if (opcode_extension == 0x1b)
				gen_helper_fd2l(cpu_env);

			/* Move the dest_data to final destination */

			dest_reg_index = (dest_reg_index >> 1) & 1;

//			printf("generating store to index: %d\n", dest_reg_index);

	   		gen_helper_dest_fpd64(cpu_env, tcg_const_i32(dest_reg_index));

			/* No auto-increment addressing mode supported for this instruction format. */

			break;

		case 0x1c:	/* FCMPD */

			/* Move src1 into env->src1_data, and set up src2_data */

//			printf("src1_reg_index: %d, src2_reg_index: %d\n", src1_reg_index, src2_reg_index);

			gen_helper_src1_64_direct(cpu_env, tcg_const_i32(src1_reg_index));
			gen_helper_src2_64_direct(cpu_env, tcg_const_i32(src2_reg_index));

			/* Generate a call to the appropriate helper */

//			gen_helper_debug(cpu_env);
			gen_helper_fcmpd(cpu_env);

			/* No auto-increment addressing mode supported for this instruction format. */

			break;

		default:

			/* This an illegal instruction. Generate the illegal instruction trap.  */
			generate_trap(env, 1 << dc->env->cpu_index, 1 << TRAP_CAUSE_ILLEGAL_INST);
			dc->is_jmp = DISAS_UPDATE;
			return;
	}
}

static void decode_insn_mop1e(CPUState *env, DisasContext *dc)
{
	int32_t offset, return_address;
	uint32_t dest_areg_index, indirect_areg_index;

	switch ((dc->opcode >> 11) & 0x1f) {

		case 0:
		case 2:		/* CALLI.B */

			/*
			 * CALLI instruction.
			 * Encoding: 5 bit Maj opc: 3 bits 13-15 of ofst: 3 bit ret addr reg: 5 bit 8-12 of ofst: 5 bits of 0: 3 bits 5-7 of ofst: 3 bit indir reg: 5 bit 0-4 of ofst
			 */

			dest_areg_index = ((dc->opcode >> 21) & 0x7) + 0x20;
			indirect_areg_index = ((dc->opcode >> 5) & 0x7) + 0x20;
			offset = dc->opcode & 0x1f; /* Extract 0-4 bits of the offset. */
			return_address = dc->pc + 4;

			/* Extract bits 5-7 of the offset.  */

			offset |= (dc->opcode >> 3) & (0x7 << 5);

			/* Extract bits 8-12 of the offset.  */

			offset |= (dc->opcode >> 8) & (0x1f << 8);

			/* Extract bits 12 - 15 of the offset.  */

			offset |= (dc->opcode >> 11) & (0x7 << 13);

			/* Convert to 18 bit word address.  */

			offset <<= 2;

			/* Sign extend.  */

			if (offset & 0x20000)
				offset |= 0xfffc0000;

			/* dest_address = offset + indirect register.  */

			tcg_gen_add_i32(dest_address, tcg_const_i32(offset), ubicom32_regs.thread_regs[indirect_areg_index]);

			/* Destination register gets the return address.  */

			tcg_gen_mov_i32(ubicom32_regs.thread_regs[dest_areg_index], tcg_const_i32(return_address));

			/* Set the PC to newly computed address and exit this block.  */

			tcg_gen_movi_i32(ubicom32_regs.thread_regs[0x38], dc->pc);
			tcg_gen_mov_i32(ubicom32_regs.thread_regs[0x34], dest_address);
			dc->is_jmp = DISAS_JUMP;
			break;

		case 0x1:	/* LEAI */

			dest_areg_index = ((dc->opcode >> 21) & 7) + 32;
			offset = dc->opcode & 0x1fffff;

			tcg_gen_add_i32(ubicom32_regs.thread_regs[dest_areg_index],
				ubicom32_regs.thread_regs[dest_areg_index], tcg_const_i32(offset));

			break;
	}
}

uint32_t num_insns_decoded;

static void decode_insn (CPUState *env, DisasContext *dc)
{
    uint32_t major_opcode = (dc->opcode >> 27) & 0x1f;
    struct auto_increment s1_inc, dest_inc;
    uint32_t opsize;

    /*
     * Set this for every instruction.
     */
    tcg_gen_movi_i32(current_pc, (dc->pc));

    switch(major_opcode) {
    case 0x8:
    case 0x9:
    case 0xa:
    case 0xb:
    case 0xc:
    case 0xd:
	{
	    /*
	     * Encoding 5 opc, 11 dest, 1 opsise 1/2  selector, 4 s2 d reg, 11 src1.
	     * AND
	     * Major opcode 0x8 is AND.1 or AND.2
	     * Major opcode 0x9 is AND.4
	     *
	     * OR
	     * Major opcode 0xa is OR.1 or OR.2
	     * Major opcode 0xb is OR.4
	     *
	     * XOR
	     * Major opcode 0xc is XOR.1 or XOR.2
	     * Major opcode 0xd is XOR.4
	     *
	     */
	    int src2_reg_index = (dc->opcode >> 11) & 0xf;

	    /*
	     * Figure out the opsize.
	     */
	    if (major_opcode == 0x8 || major_opcode == 0xa ||major_opcode == 0xc) {
		if(dc->opcode & 0x8000) {
		    opsize = 1;
		} else {
		    opsize = 2;
		}
	    } else {
		opsize = 4;
	    }

	    /*
	     * Setup src1 and get it range checked.
	     */
	    gen_src1_address(dc, dc->opcode, opsize, &s1_inc);

	    /*
	     * Setup dest and get it range checked.
	     */
	    gen_dest_address(dc, dc->opcode, opsize, &dest_inc);
	    gen_address_check(dc, opsize);

	    /*
	     * Move src1 into src1_data in env.
	     */
	    gen_src1(dc, dc->opcode, opsize);

	    /*
	     * Move src2 to src2_data in env.
	     */
	    tcg_gen_mov_i32(src2_data, ubicom32_regs.thread_regs[src2_reg_index]);

	    switch(major_opcode) {
	    case 8:
	    case 9:
		/*
		 * Do the AND operation.
		 */
		tcg_gen_and_i32(dest_data, src1_data, src2_data);
		break;
	    case 0xa:
	    case 0xb:
		/*
		 * Do the OR operation.
		 */
		tcg_gen_or_i32(dest_data, src1_data, src2_data);
		break;
	    case 0xc:
	    case 0xd:
		/*
		 * Do the XOR operation.
		 */
		tcg_gen_xor_i32(dest_data, src1_data, src2_data);
		break;
	    }

	    /*
	     * Run the NZ flag settings via a helper.
	     */
	    gen_helper_nzflag(cpu_env, tcg_const_i32(opsize));

	    /*
	     * Move the dest_data to final destination.
	     */
	    gen_dest(dc, dc->opcode, opsize);

	    /*
	     * Generate the post increments.
	     */
	    gen_store_auto_inc(dc, &s1_inc);
	    gen_store_auto_inc(dc, &dest_inc);
	    break;
	}
    case 0xe:
    case 0xf:
    case 0x10:
	{
	    /*
	     * ADD/ADDC
	     * Encoding 5 opc, 11 dest, 1 add.1 add.2 selector, 4 s2 d reg, 11 src1.
	     * Major opcode 0xe is ADD.1 or ADD.2
	     * Major opcode 0xf is ADD.4
	     * Major opcode 0x10 is ADDC
	     */
	    int src2_reg_index = (dc->opcode >> 11) & 0xf;
	    uint32_t operation = (major_opcode == 0x10) ? 1 : 0;
	    /*
	     * Figure out the opsize.
	     */
	    if (major_opcode == 0xe) {
		if(dc->opcode & 0x8000) {
		    opsize = 1;
		} else {
		    opsize = 2;
		}
	    } else {
		opsize = 4;
	    }

	    /*
	     * Setup src1 and get it range checked.
	     */
	    gen_src1_address(dc, dc->opcode, opsize, &s1_inc);

	    /*
	     * Setup dest and get it range checked.
	     */
	    gen_dest_address(dc, dc->opcode, opsize, &dest_inc);
	    gen_address_check(dc, opsize);

	    /*
	     * Move src1 into src1_data in env.
	     */
	    gen_src1(dc, dc->opcode, opsize);

	    /*
	     * Move src2 to src2_data in env.
	     */
	    tcg_gen_mov_i32(src2_data, ubicom32_regs.thread_regs[src2_reg_index]);

	    /*
	     * Run the computation via the helper function.
	     */
	    gen_helper_add(cpu_env, tcg_const_i32(opsize), tcg_const_i32(operation));

	    /*
	     * Move the dest_data to final destination.
	     */
	    gen_dest(dc, dc->opcode, opsize);

	    /*
	     * Generate the post increments.
	     */
	    gen_store_auto_inc(dc, &s1_inc);
	    gen_store_auto_inc(dc, &dest_inc);
	    break;
	}
    case 0x1c:
    case 0x1d:
	{
	    /*
	     * MOVEAI, MOVEAIH cases. The bits are 4 opc, 4 offset, 3 Ar, 21 Offset
	     */
	    uint32_t areg_index = ((dc->opcode >> 21) & 0x7) + 0x20;
	    uint32_t offset = (((dc->opcode >> 3) & (0xf << 21)) | (dc->opcode & 0x1fffff)) << 7;

	    /*
	     * Generate instruction to store it into the address register
	     */
	    tcg_gen_mov_i32(ubicom32_regs.thread_regs[areg_index], tcg_const_i32(offset));
	    break;
	}
    case 0x11:
    case 0x12:
    case 0x13:
	{
	    int src2_reg_index = (dc->opcode >> 11) & 0xf;
	    uint32_t operation = (major_opcode == 0x13) ? 1 : 0;
	    /*
	     * SUB/SUBC The bits are 5 opc, 11 dest, 1 sub.1 sub.2 selector, 4 s2 d reg, 11 src1.
	     */

	    /*
	     * Figure out the opsize.
	     */
	    if (major_opcode == 0x11) {
		if(dc->opcode & 0x8000) {
		    opsize = 1;
		} else {
		    opsize = 2;
		}
	    } else {
		opsize = 4;
	    }

	    /*
	     * Setup src1 and get it range checked.
	     */
	    gen_src1_address(dc, dc->opcode, opsize, &s1_inc);

	    /*
	     * Setup dest and get it range checked.
	     */
	    gen_dest_address(dc, dc->opcode, opsize, &dest_inc);
	    gen_address_check(dc, opsize);

	    /*
	     * Move src1 into src1_data in env.
	     */
	    gen_src1(dc, dc->opcode, opsize);

	    /*
	     * Move src2 to src2_data in env.
	     */
	    tcg_gen_mov_i32(src2_data, ubicom32_regs.thread_regs[src2_reg_index]);

	    /*
	     * Run the computation via the helper function.
	     */
	    gen_helper_sub(cpu_env, tcg_const_i32(opsize), tcg_const_i32(operation));

	    /*
	     * Move the dest_data to final destination.
	     */
	    gen_dest(dc, dc->opcode, opsize);

	    /*
	     * Generate the post increments.
	     */
	    gen_store_auto_inc(dc, &s1_inc);
	    gen_store_auto_inc(dc, &dest_inc);
	    break;
	}
    case 0x18:
	{
	    /*
	     * cmpi: 5 bit major opcode: 16bits immediate value.:11 bits src1
	     */
	    uint32_t immediate_data = (dc->opcode >>11) & 0xffff;
	    opsize = 2;

	    /*
	     * Sign extend it to 32 bits.
	     */
	    if (immediate_data & 0x8000) {
		immediate_data |= 0xffff0000;
	    }

	    /*
	     * Setup src1 and get it range checked.
	     */
	    gen_src1_address(dc, dc->opcode, opsize, &s1_inc);
	    gen_address_check(dc, opsize);

	    /*
	     * Move src1 into src1_data in env.
	     */
	    gen_src1(dc, dc->opcode, opsize);

	    /*
	     * Move immediate_data  to src2_data in env.
	     */
	    tcg_gen_movi_i32(src2_data, immediate_data);

	    /*
	     * Call the sub helper to set condition codes and forget the result.
	     * The operation field is set 2 for cmpi.
	     */
	    gen_helper_sub(cpu_env, tcg_const_i32(opsize), tcg_const_i32(2));

	    /*
	     * Generate the post increments.
	     */
	    gen_store_auto_inc(dc, &s1_inc);
	    break;
	}
    case 0x0:
	{
	    decode_insn_mop0 (env, dc);
	    return;
	    break;
	}
    case 0x2:
	{
	    decode_insn_mop2 (env, dc);
	    return;
	    break;
	}
    case 0x6:
	{
	    decode_insn_mop6 (env, dc);
	    return;
	    break;
	}
    case 0x4:
    case 0x5:
	{
	    /*
	     * BSET = 4
	     * BCLR = 5
	     * Coding:  5 bits opcode: 11 bit dest: 5 bit immediate bit number: 11 bit s1
	     */

	    uint32_t bit_num;

	    /*
	     * Setup src1 and get it range checked.
	     */
	    opsize = 4;
	    gen_src1_address(dc, dc->opcode, opsize, &s1_inc);

	    /*
	     * Setup dest and get it range checked.
	     */
	    gen_dest_address(dc, dc->opcode, opsize, &dest_inc);
	    gen_address_check(dc, opsize);

	    /*
	     * Move src1 into src1_data in env.
	     */
	    gen_src1(dc, dc->opcode, opsize);

	    /*
	     * Extract the immediate bit number field from bits 11 - 15
	     */
	    bit_num = (dc->opcode >> 11) & 0x1f;

	    /*
	     * Bit # is an immediate value. Mask = 1 << immediate value.
	     */
	    tcg_gen_shli_i32(bset_clr_tmp, tcg_const_i32(1), bit_num);

	    if (major_opcode == 5) {
		/*
		 * BCLR
		 */
		gen_helper_btst_clr_set(cpu_env, tcg_const_i32(1), tcg_const_i32(dc->opcode));
	    } else {
		/*
		 * BSET
		 */
		gen_helper_btst_clr_set(cpu_env, tcg_const_i32(2), tcg_const_i32(dc->opcode));
	    }


	    /*
	     * Write back stage: Move the dest_data to final destination.
	     */
	    gen_dest(dc, dc->opcode, opsize);

	    /*
	     * Generate the post increments.
	     */
	    gen_store_auto_inc(dc, &s1_inc);
	    gen_store_auto_inc(dc, &dest_inc);
	    break;
	}
    case 0x19:
	{
	    /*
	     * MOVEI: 5 bit major opcode:11 bits dest: 16bits immediate value.
	     */
	    uint32_t immediate_data = dc->opcode & 0xffff;
	    opsize = 2;

	    /*
	     * Sign extend it to 32 bits.
	     */
	    if (immediate_data & 0x8000) {
		immediate_data |= 0xffff0000;
	    }

	    /*
	     * Setup dest and get it range checked.
	     */
	    gen_dest_address(dc, dc->opcode, opsize, &dest_inc);
	    gen_address_check(dc, opsize);

	    /*
	     * Move the immediate_data to dest_data.
	     */
	    tcg_gen_mov_i32(dest_data, tcg_const_i32(immediate_data));

	    /*
	     * Move the dest_data to final destination.
	     */
	    gen_dest(dc, dc->opcode, opsize);

	    /*
	     * Generate the post increments.
	     */
	    gen_store_auto_inc(dc, &dest_inc);
	    break;
	}
    case 0x1a:
	{
	    /*
	     * JMP conditional cases. Encodeing 5 Major opcode: 4 Condition code encoding: 1 bit prediction: 1 bit CC flag selector: 21 bits of signed offset.
	     */
	    uint32_t cc = (dc->opcode >> 23) & 0xf;
	    uint32_t flag_32 = (dc->opcode >> 21) & 0x1;
	    int32_t offset = (dc->opcode & 0x1fffff) << 2;
	    int32_t base = (int32_t)dc->pc;
	    int l1;

	    l1 = gen_new_label();

	    /*
	     * The offset now a signed byte offset. We have to ultimately end up with a sign extended byte offset.
	     * Check bit 22. If it is set we have to sign extend.
	     */
	    if (offset & 0x400000) {
		/*
		 * Sign extend by oring in 0xff800000
		 */
		offset |= 0xff800000;
	    }

	    /*
	     * Ultimate jump address is base + offset
	     */
	    base += offset;

	    gen_helper_jmpcc(cpu_env, tcg_const_i32(cc), tcg_const_i32(flag_32));

	    /*
	     * The helper will set dest_data to 1 if the jump is taken. O means PC = pc+4
	     * previous_pc = current_pc (Instructin has finished execution.)
	     */
	    tcg_gen_movi_i32(ubicom32_regs.thread_regs[0x38], dc->pc);
	    tcg_gen_brcondi_i32(TCG_COND_NE, dest_data, 0, l1);
	    gen_jmp_tb(dc, 0, dc->pc+4);
	    gen_set_label(l1);
	    gen_jmp_tb(dc, 1, base);
	    break;
	}
    case 0x1b:
	{
	    /*
	     * CALL instruction.
	     * Coding: 5 bit major opcode: 3 bits 21 - 23 of offset: 3 bit return Address reg: 21 bits 0 - 20 of offset
	     */
	    uint32_t areg_index = ((dc->opcode >> 21) & 0x7) + 0x20;
	    int32_t offset = (dc->opcode >> 3) & ( 0x7 << 21); /* Extract bit 21 - 23 bits of the offset. */
	    int32_t base = dc->pc;
	    int32_t return_address = dc->pc + 4;

	    /*
	     * Extract the remaining bits of the offset and then convert to a 26 bit word offset.
	     */
	    offset = (offset | (dc->opcode & 0x1fffff)) << 2;

	    /*
	     * Sign extend the address.
	     */
	    if (offset & 0x02000000) {
		offset |= 0xfc000000;
	    }

	    /*
	     * Generate the final call address.
	     */
	    base += offset;

	    /*
	     * Execution:
	     *    An = PC + 4
	     *    PC = new address.
	     */
	    tcg_gen_mov_i32(ubicom32_regs.thread_regs[areg_index], tcg_const_i32(return_address));

	    /*
	     * Set PC to new address and exit this block.
	     */
	    tcg_gen_movi_i32(ubicom32_regs.thread_regs[0x38], dc->pc);
	    gen_jmp_tb(dc, 1, base);
	    break;
	}
    case 0x1e:
	decode_insn_mop1e(env, dc);
	break;

    case 0x16:	/* PXADDS */
	{
		int bit_15 = (dc->opcode >> 15) & 1;

//		printf("PXADD instruction\n");

		/* Setup src1 and get it range checked */

		gen_src1_address(dc, dc->opcode, 4, &s1_inc);

		/* Setup dest and get it range checked */

		gen_dest_address(dc, dc->opcode, 2, &dest_inc);
		gen_address_check(dc, 2);

		/* Move src1 into env->src1_data, and set up src2_data */

		gen_src1(dc, dc->opcode, 4);
		tcg_gen_mov_i32(src2_data, ubicom32_regs.thread_regs[(dc->opcode >> 11) & 0xf]);

		/* Generate a call to the pxadds helper */

		gen_helper_pxadds(cpu_env, tcg_const_i32(bit_15));

		/* Move the dest_data to final destination */

		gen_dest(dc, dc->opcode, 2);

		/* Generate the post increments */

		gen_store_auto_inc(dc, &s1_inc);
		gen_store_auto_inc(dc, &dest_inc);
		break;
	}

    case 0x14:	/* PXBLEND */
	{
		int bit_15 = (dc->opcode >> 15) & 1;

//		printf("PXBLEND instruction\n");

		/* Setup src1 and get it range checked */

		gen_src1_address(dc, dc->opcode, 4, &s1_inc);

		/* Setup dest and get it range checked */

		gen_dest_address(dc, dc->opcode, 4, &dest_inc);
		gen_address_check(dc, 4);

		/* Move src1 into env->src1_data, and set up src2_data */

		gen_src1(dc, dc->opcode, 4);
		tcg_gen_mov_i32(src2_data, ubicom32_regs.thread_regs[(dc->opcode >> 11) & 0xf]);

		/* Generate a call to the pxblend helper */

		gen_helper_pxblend(cpu_env, tcg_const_i32(bit_15));

		/* Move the dest_data to final destination */

		gen_dest(dc, dc->opcode, 4);

		/* Generate the post increments */

		gen_store_auto_inc(dc, &s1_inc);
		gen_store_auto_inc(dc, &dest_inc);
		break;
	}

    case 0x15:	/* PXVI */
	{
		int bit_15 = (dc->opcode >> 15) & 1;

//		printf("PXVI instruction\n");

		/* Setup src1 and get it range checked */

		gen_src1_address(dc, dc->opcode, 4, &s1_inc);

		/* Setup dest and get it range checked */

		gen_dest_address(dc, dc->opcode, 4, &dest_inc);
		gen_address_check(dc, 4);

		/* Move src1 into env->src1_data, and set up src2_data */

		gen_src1(dc, dc->opcode, 4);
		tcg_gen_mov_i32(src2_data, ubicom32_regs.thread_regs[(dc->opcode >> 11) & 0xf]);

		/* Generate a call to the pxvi helper */

		gen_helper_pxvi(cpu_env, tcg_const_i32(bit_15));

		/* Move the dest_data to final destination */

		gen_dest(dc, dc->opcode, 4);

		/* Generate the post increments */

		gen_store_auto_inc(dc, &s1_inc);
		gen_store_auto_inc(dc, &dest_inc);
		break;
	}

    case 0x03:

	decode_insn_mop3(env, dc);
	return;

    default:
	/*
	 * This an illegal instruction. Generate the illegal instruction trap.
	 */
	generate_trap(env, 1 << dc->env->cpu_index, 1 << TRAP_CAUSE_ILLEGAL_INST);
	dc->is_jmp = DISAS_UPDATE;
    }

    if (dc->is_jmp == DISAS_NEXT) {
	/*
	 * Move previous PC to Previous PC and set PC to PC + 4.
	 */
	tcg_gen_movi_i32(ubicom32_regs.thread_regs[0x38], dc->pc);
    }

}

/* generate intermediate code for basic block 'tb'.  */
static inline void
gen_intermediate_code_internal(CPUState *env, TranslationBlock *tb,
                               int search_pc)
{
    DisasContext dc1, *dc = &dc1;
    uint16_t *gen_opc_end;
    CPUBreakpoint *bp;
    int j, lj;
    target_ulong pc_start;
    int num_insns;
    int max_insns;

    /* generate intermediate code */
    pc_start = tb->pc;

    dc->tb = tb;

    gen_opc_end = gen_opc_buf + OPC_MAX_SIZE;

    dc->env = env;
    dc->is_jmp = DISAS_NEXT;
    dc->pc = pc_start;
    //dc->cc_op = CC_OP_DYNAMIC;
    dc->singlestep_enabled = env->singlestep_enabled;
    /*
     * This needs to change for the 8k XXXXXXXXX
     */
    dc->user = 0;

    num_insns = 0;
    max_insns = tb->cflags & CF_COUNT_MASK;
    if (max_insns == 0)
        max_insns = CF_COUNT_MASK;

    lj = -1;
    dc->is_mem = 0;

    /*
     * Range check the PC.
     */
    if (env->global_regs[0] == UBICOM32_7K_CHIP_ID) {
	uint32_t thread_mask = 1 << env->cpu_index;
	if (ip7k_instruction_range_check(env, dc->pc) == 0) {
	    /*
	     * Not in any valid range. Triger I_RANGE_ERR trap
	     */
	    env->thread_regs[0x34] = env->current_pc;
	    generate_trap(env, thread_mask, 1 << TRAP_CAUSE_I_RANGE_ERR);
	    dc->is_jmp = DISAS_JUMP;
	    gen_helper_raise_exception(tcg_const_i32(EXCP_INTERRUPT));
	    goto done_generating;
	    return;
	}

	/*
	 * The range check passes. Make sure the access isn't happening to a hole
	 * in the address Map.
	 */
	if (ip7k_hole_check(env, dc->pc) == 0) {
	    /*
	     * Access is in a hole. Generate I_DECODE_ERR
	     */
	    env->thread_regs[0x34] = env->current_pc;
	    generate_trap(env, thread_mask, 1 << TRAP_CAUSE_I_DECODE_ERR);
	    dc->is_jmp = DISAS_JUMP;
	    gen_helper_raise_exception(tcg_const_i32(EXCP_INTERRUPT));
	    goto done_generating;
	    return;
	}

	if (env->def->has_mmu) {
	    /*
	     * Check the mapped range.
	     */
	    if(ubicom32_range_check(env->def->mapped_end, env->def->mapped_base, dc->pc)) {
		/*
		 * The instruction is in the mapped range. Call inst_mapping_check to get it resolved.
		 * This may not return if there is a fault.
		 */
		inst_mapping_check(env, dc->pc);
	    }
	}
    } else {
	uint32_t thread_mask = 1 << env->cpu_index;
	if (ip8k_instruction_range_check(env, dc->pc) == 0) {
	    /*
	     * Not in any valid range. Triger I_RANGE_ERR trap
	     */
	    env->thread_regs[0x34] = env->current_pc;
	    generate_trap(env, thread_mask, 1 << TRAP_CAUSE_I_RANGE_ERR);
	    dc->is_jmp = DISAS_JUMP;
	    gen_helper_raise_exception(tcg_const_i32(EXCP_INTERRUPT));
	    goto done_generating;
	    //return;
	}

	/*
	 * The range check passes. Make sure the access isn't happening to a hole
	 * in the address Map.
	 */
	if (ip8k_hole_check(env, dc->pc) == 0) {
	    /*
	     * Access is in a hole. Generate I_DECODE_ERR
	     */
	    env->thread_regs[0x34] = env->current_pc;
	    generate_trap(env, thread_mask, 1 << TRAP_CAUSE_I_DECODE_ERR);
	    dc->is_jmp = DISAS_JUMP;
	    gen_helper_raise_exception(tcg_const_i32(EXCP_INTERRUPT));
	    goto done_generating;
	    //return;
	}

	if (env->def->has_mmu) {
	    /*
	     * Check the mapped range.
	     */
	    if(ubicom32_range_check(env->def->mapped_end, env->def->mapped_base, dc->pc)) {
		/*
		 * The instruction is in the mapped range. Call inst_mapping_check to get it resolved.
		 * This may not return if there is a fault.
		 */
		inst_mapping_check(env, dc->pc);
	    }
	}
    }

    gen_icount_start();
    while(dc->is_jmp == DISAS_NEXT) {
        if (unlikely(!TAILQ_EMPTY(&env->breakpoints))) {
	    TAILQ_FOREACH(bp, &env->breakpoints, entry) {
		if (bp->pc == dc->pc) {
		    /*
		     * Force PC to current PC.
		     */
		    tcg_gen_mov_i32(ubicom32_regs.thread_regs[0x34], tcg_const_i32(dc->pc));
		    gen_helper_raise_exception(tcg_const_i32(EXCP_DEBUG));
		    dc->is_jmp = DISAS_JUMP;
		    dc->pc += 4;
		    goto done_generating;
		}
	    }
	    if (dc->is_jmp) {
		break;
	    }
	}
        if (search_pc) {
            j = gen_opc_ptr - gen_opc_buf;
            if (lj < j) {
                lj++;
                while (lj < j)
                    gen_opc_instr_start[lj++] = 0;
            }
            gen_opc_pc[lj] = dc->pc;
            gen_opc_instr_start[lj] = 1;
            gen_opc_icount[lj] = num_insns;
        }
        if (num_insns + 1 == max_insns && (tb->cflags & CF_LAST_IO)) {
            gen_io_start();
	}

	/*
	 * Read instruction.
	 */
	dc->opcode = ldl_code(dc->pc);

	/*
	 * Decode instruction.
	 */
	dc->is_lea_pdec = dc->need_src1_checking = dc->need_dest_checking = 0;
	decode_insn(env, dc);
        dc->pc += 4;
        num_insns++;
	num_insns_decoded++;

        if (env->singlestep_enabled)
            break;

        if ((dc->pc & (TARGET_PAGE_SIZE - 1)) == 0)
            break;

        if (gen_opc_ptr >= gen_opc_end)
            break;

        if (num_insns >= max_insns)
            break;
    }

    if (tb->cflags & CF_LAST_IO) {
        gen_io_end();
    }

    if (env->singlestep_enabled) {
	if (!dc->is_jmp) {
	    tcg_gen_mov_i32(ubicom32_regs.thread_regs[0x34], tcg_const_i32(dc->pc));
	}
	gen_helper_raise_exception(tcg_const_i32(EXCP_DEBUG));
    } else {
        switch(dc->is_jmp) {
        case DISAS_NEXT:
            gen_jmp_tb(dc, 0, dc->pc);
            break;
        default:
        case DISAS_JUMP:
        case DISAS_UPDATE:
            /* indicate that the hash table must be used to find the next TB */
            tcg_gen_exit_tb(0);
            break;
        case DISAS_TB_JUMP:
            /* nothing more to generate */
            break;
        }
    }
 done_generating:
    gen_icount_end(tb, num_insns);
    *gen_opc_ptr = INDEX_op_end;
    if (search_pc) {
        j = gen_opc_ptr - gen_opc_buf;
        lj++;
        while (lj <= j)
            gen_opc_instr_start[lj++] = 0;
    } else {
        tb->size = dc->pc - pc_start;
        tb->icount = num_insns;
    }
}

void gen_intermediate_code(CPUState *env, TranslationBlock *tb)
{
    gen_intermediate_code_internal(env, tb, 0);
}

void gen_intermediate_code_pc(CPUState *env, TranslationBlock *tb)
{
    gen_intermediate_code_internal(env, tb, 1);
}

