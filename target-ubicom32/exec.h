/*-----------------------------------------------------------------------
 *  ubicom32 execution defines
 *
 * Copyright (c) 2017, The Linux Foundation. All rights reserved.
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 * -----------------------------------------------------------------------
 */
#include "dyngen-exec.h"

register struct CPUUBICOM32State *env asm(AREG0);
/* This is only used for tb lookup.  */
register uint32_t T0 asm(AREG1);
/* ??? We don't use T1, but common code expects it to exist  */
#define T1 env->t1

#include "cpu.h"
#include "exec-all.h"
#include <string.h>

static inline void env_to_regs(void)
{
    /*
     * Copy global state into env global state.
    memcpy(env->global_regs, ubicom32_global_registers, 64*sizeof(uint32_t));
     */
}

static inline void regs_to_env(void)
{
    /*
     * Copy env global state into global state.
    memcpy(ubicom32_global_registers, env->global_regs, 64*sizeof(uint32_t));
     */
}

#if !defined(CONFIG_USER_ONLY)
#include "softmmu_exec.h"
#endif

static inline int cpu_has_work(CPUState *env)
{
    uint32_t int_state = 0;
    uint32_t num_ints = env->def->num_interrupts/32;
    uint32_t i;
    /*
     * int_state = (int_stat0 & int_mask0) | (int_stat1 & int_mask1) | (int_stat2 & int_mask2)
     */
    for (i=0; i< num_ints; i++) {
	int_state |= (ubicom32_global_registers[0x41 + i] & env->thread_regs[0x30 + i]);
    }
    return int_state;
}

static inline int cpu_halted(CPUState *env) {
    uint32_t mask = 1 << env->cpu_index;
    uint32_t int_state = 0;
    uint32_t num_ints = env->def->num_interrupts/32;
    uint32_t i;
    uint32_t m_runnable = 1;

    if(env->def->has_mmu) {
	m_runnable = mmu_runnable(env);
    }

    /*
     * Thread is runnable only if for particular thread the thread bits in mt_en, mt_active and mt_dbg_active are set.
     */
    if ((ubicom32_global_registers[0x53 - 0x40] & mask) &&
	(ubicom32_global_registers[0x4e - 0x40] & mask) &&
	(ubicom32_global_registers[0x51 - 0x40] & mask) &&
	m_runnable) {
	return 0;
    }

    /*
     * If global interrupts are off just return.
     */
    if ((ubicom32_global_registers[0x4d - 0x40] & 0x1) == 0) {
	/*
	 * Thread is not runnable.
	 */
	return EXCP_HALTED;
    }

    /*
     * int_state = (int_stat0 & int_mask0) | (int_stat1 & int_mask1) | (int_stat2 & int_mask2)
     */
    for (i=0; i< num_ints; i++) {
	int_state |= (ubicom32_global_registers[0x41 - 0x40 + i] & env->thread_regs[0x30 + i]);
    }

    if (int_state) {
	/*
	 * This thread should be active.
	 */
	// printf("Thread[%d] kept alive. Int_state = 0x%08x\n", env->cpu_index, int_state);
	ubicom32_global_registers[0x4e - 0x40] |= mask;
	if (m_runnable) {
	    return 0;
	}
    }

#if 0
    /*
     * Thread is runnable only if for particular thread the thread bits in mt_en, mt_active and mt_dbg_active are set.
     */
    if ((ubicom32_global_registers[0x53 - 0x40] & mask) &&
	(ubicom32_global_registers[0x4e - 0x40] & mask) &&
	(ubicom32_global_registers[0x51 - 0x40] & mask)) {
	return 0;
    }
#endif

    /*
     * Thread is not runnable.
     */
    return EXCP_HALTED;
}
